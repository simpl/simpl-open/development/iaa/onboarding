package eu.europa.ec.simpl.onboarding.services.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

import eu.europa.ec.simpl.common.model.dto.onboarding.ParticipantTypeDTO;
import eu.europa.ec.simpl.onboarding.entities.ParticipantType;
import eu.europa.ec.simpl.onboarding.exceptions.ParticipantTypeNotFoundException;
import eu.europa.ec.simpl.onboarding.mappers.ParticipantTypeMapper;
import eu.europa.ec.simpl.onboarding.repositories.ParticipantTypeRepository;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class ParticipantTypeServiceImplTest {

    @InjectMocks
    private ParticipantTypeServiceImpl participantTypeService;

    @Mock
    private ParticipantTypeRepository participantTypeRepository;

    @Mock
    private ParticipantTypeMapper participantTypeMapper;

    @Test
    void findParticipantTypeById_shouldReturnParticipantType_whenExists() {
        // Arrange
        var participantTypeId = UUID.randomUUID();
        var participantType = mock(ParticipantType.class);
        var dto = mock(ParticipantTypeDTO.class);

        given(participantTypeRepository.findById(participantTypeId)).willReturn(Optional.of(participantType));
        given(participantTypeMapper.toDto(participantType)).willReturn(dto);

        // Act
        var result = participantTypeService.findParticipantTypeById(participantTypeId);

        // Assert
        assertThat(result).isEqualTo(dto);
        verify(participantTypeRepository).findById(participantTypeId);
        verify(participantTypeMapper).toDto(participantType);
    }

    @Test
    void findParticipantTypeById_shouldThrowException_whenNotFound() {
        // Arrange
        var participantTypeId = UUID.randomUUID();

        given(participantTypeRepository.findById(participantTypeId)).willReturn(Optional.empty());

        // Act & Assert
        assertThatThrownBy(() -> participantTypeService.findParticipantTypeById(participantTypeId))
                .isInstanceOf(ParticipantTypeNotFoundException.class)
                .hasMessageContaining(participantTypeId.toString());

        verify(participantTypeRepository).findById(participantTypeId);
        verifyNoInteractions(participantTypeMapper);
    }

    @Test
    void findAllParticipantTypes_shouldReturnAllParticipantTypes() {
        // Arrange
        var participantType1 = mock(ParticipantType.class);
        var participantType2 = mock(ParticipantType.class);
        var dto1 = mock(ParticipantTypeDTO.class);
        var dto2 = mock(ParticipantTypeDTO.class);

        given(participantTypeRepository.findAll()).willReturn(List.of(participantType1, participantType2));
        given(participantTypeMapper.toDto(participantType1)).willReturn(dto1);
        given(participantTypeMapper.toDto(participantType2)).willReturn(dto2);

        // Act
        var result = participantTypeService.findAllParticipantTypes();

        // Assert
        assertThat(result).containsExactlyInAnyOrder(dto1, dto2);
        verify(participantTypeRepository).findAll();
        verify(participantTypeMapper).toDto(participantType1);
        verify(participantTypeMapper).toDto(participantType2);
    }

    @Test
    void findAllParticipantTypes_shouldReturnEmptySet_whenNoParticipantTypesExist() {
        // Arrange
        given(participantTypeRepository.findAll()).willReturn(List.of());

        // Act
        var result = participantTypeService.findAllParticipantTypes();

        // Assert
        assertThat(result).isEmpty();
        verify(participantTypeRepository).findAll();
        verifyNoInteractions(participantTypeMapper);
    }
}
