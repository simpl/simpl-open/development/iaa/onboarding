package eu.europa.ec.simpl.onboarding.services.impl;

import static eu.europa.ec.simpl.common.test.TestUtil.a;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.times;

import eu.europa.ec.simpl.common.exchanges.usersroles.UserExchange;
import eu.europa.ec.simpl.common.model.dto.usersroles.KeycloakUserDTO;
import eu.europa.ec.simpl.onboarding.exceptions.UserCreationException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.client.HttpClientErrorException;

@ExtendWith(MockitoExtension.class)
class UserServiceImplTest {

    @Mock
    private UserExchange userExchange;

    @InjectMocks
    private UserServiceImpl userService;

    private KeycloakUserDTO keycloakUserDTO;

    @BeforeEach
    void setUp() {
        keycloakUserDTO = a(KeycloakUserDTO.class);
    }

    @Test
    void createUser_shouldReturnUserId_whenUserIsCreatedSuccessfully() {
        String expectedUserId = "user-id";
        given(userExchange.createUser(keycloakUserDTO)).willReturn(expectedUserId);

        String actualUserId = userService.createUser(keycloakUserDTO);

        assertThat(actualUserId).isEqualTo(expectedUserId);
        then(userExchange).should(times(1)).createUser(keycloakUserDTO);
    }

    @Test
    void createUser_shouldThrowUserCreationException_whenHttpClientErrorExceptionOccurs() throws Exception {
        // Given
        String errorJson = "{\"error\": \"Some error message\"}";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpClientErrorException exception = new HttpClientErrorException(
                HttpStatus.BAD_REQUEST, "Bad Request", headers, errorJson.getBytes(), null);
        given(userExchange.createUser(keycloakUserDTO)).willThrow(exception);

        // When
        Throwable thrown = catchThrowable(() -> userService.createUser(keycloakUserDTO));

        // Then
        assertThat(thrown).isInstanceOf(UserCreationException.class).hasMessageContaining("Unknown error");
        then(userExchange).should(times(1)).createUser(keycloakUserDTO);
    }

    @Test
    void createUser_shouldThrowUserCreationException_whenJsonProcessingExceptionOccurs() throws Exception {
        // Given
        String errorJson = "{\"wrongField\": \"some text\"}";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpClientErrorException exception = new HttpClientErrorException(
                HttpStatus.BAD_REQUEST, "Bad Request", headers, errorJson.getBytes(), null);
        given(userExchange.createUser(keycloakUserDTO)).willThrow(exception);

        // When
        Throwable thrown = catchThrowable(() -> userService.createUser(keycloakUserDTO));

        // Then
        assertThat(thrown).isInstanceOf(UserCreationException.class).hasMessageContaining("Unknown error");
        then(userExchange).should(times(1)).createUser(keycloakUserDTO);
    }
}
