package eu.europa.ec.simpl.onboarding.controllers;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.*;

import eu.europa.ec.simpl.common.model.dto.onboarding.ParticipantTypeDTO;
import eu.europa.ec.simpl.onboarding.services.ParticipantTypeService;
import java.util.Set;
import org.instancio.junit.InstancioSource;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class ParticipantTypeControllerTest {

    @Mock
    ParticipantTypeService participantTypeService;

    @InjectMocks
    ParticipantTypeController participantTypeController;

    @ParameterizedTest
    @InstancioSource
    void getParticipantTypes(Set<ParticipantTypeDTO> expectedParticipantTypes) {

        // Given
        given(participantTypeService.findAllParticipantTypes()).willReturn(expectedParticipantTypes);

        // When
        var actualParticipantTypes = participantTypeController.getParticipantTypes();

        // Then
        then(participantTypeService).should().findAllParticipantTypes();
        assertThat(actualParticipantTypes).isEqualTo(expectedParticipantTypes);
    }
}
