package eu.europa.ec.simpl.onboarding.mappers;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.springframework.context.annotation.Import;

@Import({
    OnboardingTemplateMapperImpl.class,
    ReferenceMapper.class,
    DocumentTemplateMapperImpl.class,
    BlobMapperImpl.class,
    CommentMapperImpl.class,
    DocumentMapperImpl.class,
    DocumentTemplateMapperImpl.class,
    MimeTypeMapperImpl.class,
    OnboardingApplicantMapperImpl.class,
    OnboardingRequestMapperImpl.class,
    OnboardingStatusMapperImpl.class,
    OnboardingTemplateMapperImpl.class,
    ParticipantTypeMapperImpl.class,
    ReferenceMapper.class,
    OnboardingApplicantMapperImpl.class,
    OnboardingApplicantMapperImpl.class,
    OnboardingRequestMapperImpl.class,
})
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface ImportMapper {}
