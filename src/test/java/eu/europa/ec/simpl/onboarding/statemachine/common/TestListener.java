package eu.europa.ec.simpl.onboarding.statemachine.common;

import lombok.Data;

@Data
public class TestListener implements GenericStateMachine.Listener<TestState> {

    private Boolean invoked = false;

    @Override
    public void afterHandle(TestState oldState, TestState newState, Class<?> event, Object payload) {
        invoked = true;
    }
}
