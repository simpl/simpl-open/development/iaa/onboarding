package eu.europa.ec.simpl.onboarding.mappers;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class BlobMapperTest {

    @Spy
    private BlobMapperImpl mapper;

    @Test
    void testNullIsReturnedIfInputIsNull() {
        assertThat(mapper.fromBytes(null)).as("Input parameter is null").isNull();
        assertThat(mapper.toBytes(null)).as("Input parameter is null").isNull();
        assertThat(mapper.fromBase64String(null)).as("Input parameter is null").isNull();
        assertThat(mapper.toBase64String(null)).as("Input parameter is null").isNull();
    }
}
