package eu.europa.ec.simpl.onboarding.repositories;

import static eu.europa.ec.simpl.common.test.TestUtil.anOptional;
import static eu.europa.ec.simpl.common.test.TestUtil.anUUID;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.BDDAssertions.catchException;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

import eu.europa.ec.simpl.onboarding.entities.OnboardingProcedureTemplate;
import eu.europa.ec.simpl.onboarding.exceptions.OnboardingTemplateNotFoundException;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class OnboardingTemplateRepositoryTest {

    @Spy
    OnboardingTemplateRepository onboardingTemplateRepository;

    @Test
    void findByIdOrThrow_whenFindByIdOrThrowReturnsEntity_shouldNotThrowException() {
        given(onboardingTemplateRepository.findByParticipantTypeId(any()))
                .willReturn(anOptional(OnboardingProcedureTemplate.class));
        var exception = catchException(() -> onboardingTemplateRepository.findByParticipantTypeIdOrThrow(anUUID()));
        assertThat(exception).isNull();
    }

    @Test
    void findByIdOrThrow_whenFindByIdOrThrowReturnsEmpty_shouldThrowCertificateNotFoundException() {
        given(onboardingTemplateRepository.findByParticipantTypeId(any())).willReturn(Optional.empty());
        var exception = catchException(() -> onboardingTemplateRepository.findByParticipantTypeIdOrThrow(anUUID()));
        assertThat(exception).isInstanceOf(OnboardingTemplateNotFoundException.class);
    }
}
