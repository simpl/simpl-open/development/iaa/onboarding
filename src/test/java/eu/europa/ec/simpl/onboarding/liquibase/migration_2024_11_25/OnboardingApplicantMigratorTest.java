package eu.europa.ec.simpl.onboarding.liquibase.migration_2024_11_25;

import static eu.europa.ec.simpl.common.test.TestUtil.an;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.when;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.europa.ec.simpl.common.model.dto.usersroles.KeycloakUserDTO;
import eu.europa.ec.simpl.onboarding.liquibase.QueryExecutor;
import eu.europa.ec.simpl.onboarding.liquibase.QueryFactory;
import eu.europa.ec.simpl.onboarding.liquibase.migration_2024_11_25.pojo.CredentialRequest;
import eu.europa.ec.simpl.onboarding.liquibase.migration_2024_11_25.pojo.OnboardingApplicant;
import eu.europa.ec.simpl.onboarding.liquibase.migration_2024_11_25.pojo.OnboardingRequest;
import java.io.IOException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandler;
import java.util.List;
import liquibase.database.Database;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.CustomChangeException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class OnboardingApplicantMigratorTest {

    @Mock
    private Database database;

    @Mock
    private JdbcConnection jdbcConnection;

    @Mock
    private QueryFactory queryFactory;

    @Mock
    private HttpResponse<String> httpResponse;

    @Spy
    private OnboardingApplicantMigrator migrator;

    @Test
    public void executeTest() throws CustomChangeException, IOException, InterruptedException {
        doReturn(jdbcConnection).when(migrator).getConnection(database);
        doReturn(queryFactory).when(migrator).newQueryFactory(jdbcConnection);
        doReturn("http://juni-tests.localhost").when(migrator).getenv("MICROSERVICE_USERS_ROLES_URL");
        var onboardingRequestRepo = mock(QueryExecutor.class);
        var request = an(OnboardingRequest.class);
        var email = "unit-test@email.com";
        request.setApplicantRepresentative(email);
        given(onboardingRequestRepo.queryList(any())).willReturn(List.of(request));
        doReturn(onboardingRequestRepo).when(migrator).newQueryExecutor(queryFactory, OnboardingRequest.class);

        var credentialRequestRepo = mock(QueryExecutor.class);
        var credentialRequest = an(CredentialRequest.class);
        credentialRequest.setApplicantRepresentative(email);
        given(credentialRequestRepo.queryList(any())).willReturn(List.of(credentialRequest));
        doReturn(credentialRequestRepo).when(migrator).newQueryExecutor(queryFactory, CredentialRequest.class);
        var onboardingApplicantRepo = mock(QueryExecutor.class);
        doReturn(onboardingApplicantRepo).when(migrator).newQueryExecutor(queryFactory, OnboardingApplicant.class);
        try (var httpClient = mockStatic(HttpClient.class)) {
            var client = mock(HttpClient.class);
            given(HttpClient.newHttpClient()).willReturn(client);
            var om = new ObjectMapper();
            var userDTO = an(KeycloakUserDTO.class);
            given(httpResponse.body()).willReturn(om.writeValueAsString(List.of(userDTO)));
            when(client.send(any(HttpRequest.class), (BodyHandler<String>) any(BodyHandler.class)))
                    .thenReturn(httpResponse);
            assertDoesNotThrow(() -> migrator.execute(database));
        }
    }

    @Test
    public void methodTestingCoverage() {
        assertDoesNotThrow(() -> migrator.newQueryExecutor(null, null));
        assertDoesNotThrow(() -> migrator.getConnection(database));
        assertDoesNotThrow(() -> migrator.newQueryFactory(jdbcConnection));
        assertDoesNotThrow(() -> migrator.getenv("junit-testing"));
    }
}
