package eu.europa.ec.simpl.onboarding.statemachine;

import static eu.europa.ec.simpl.common.test.TestUtil.an;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.BDDMockito.given;

import eu.europa.ec.simpl.common.constants.Roles;
import eu.europa.ec.simpl.common.model.dto.onboarding.RejectDTO;
import eu.europa.ec.simpl.common.security.JwtService;
import eu.europa.ec.simpl.onboarding.entities.OnboardingRequest;
import eu.europa.ec.simpl.onboarding.exceptions.OperationNotPermittedException;
import eu.europa.ec.simpl.onboarding.mappers.ImportMapper;
import eu.europa.ec.simpl.onboarding.repositories.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.bean.override.mockito.MockitoBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@ImportMapper
@Import(RejectEventHandler.class)
class RejectEventHandlerTest {

    @MockitoBean
    private OnboardingRequestRepository onboardingRequestRepository;

    @MockitoBean
    private OnboardingStatusRepository onboardingStatusRepository;

    @MockitoBean
    private OnboardingTemplateRepository onboardingTemplateRepository;

    @MockitoBean
    private DocumentTemplateRepository documentTemplateRepository;

    @MockitoBean
    private DocumentRepository documentRepository;

    @MockitoBean
    private CommentRepository commentRepository;

    @MockitoBean
    private ParticipantTypeRepository participantTypeRepository;

    @MockitoBean
    private MimeTypeRepository mimeTypeRepository;

    @MockitoBean
    private JwtService jwtService;

    @MockitoBean
    private StatusChangeHandler statusChangeHandler;

    @Autowired
    private RejectEventHandler handler;

    @Test
    void handleTest() {
        var source = OnboardingState.IN_REVIEW;
        var target = OnboardingState.REJECTED;
        var rejectDTO = an(RejectDTO.class);
        var onboardingRequest = an(OnboardingRequest.class);
        var payload = new OnboardingPayload<>(onboardingRequest, rejectDTO);

        handler.handle(source, target, payload);

        assertEquals(payload.getActionPayload().getRejectionCause(), onboardingRequest.getRejectionCause());
    }

    @Test
    void guardTest_isNotaryRole_returnTrue() {
        var source = OnboardingState.IN_REVIEW;
        var target = OnboardingState.REJECTED;
        var rejectDTO = an(RejectDTO.class);
        var onboardingRequest = an(OnboardingRequest.class);
        var payload = new OnboardingPayload<>(onboardingRequest, rejectDTO);

        given(jwtService.hasRole(Roles.NOTARY)).willReturn(true);
        var ret = handler.guard(source, target, payload);
        assertTrue(ret);
    }

    @Test
    void guardTest_withOutNotaryRole_throwOperationNotPermittedException() {
        var source = OnboardingState.IN_REVIEW;
        var target = OnboardingState.REJECTED;
        var rejectDTO = an(RejectDTO.class);
        var onboardingRequest = an(OnboardingRequest.class);
        var payload = new OnboardingPayload<>(onboardingRequest, rejectDTO);

        given(jwtService.hasRole(Roles.NOTARY)).willReturn(false);
        assertThrows(OperationNotPermittedException.class, () -> handler.guard(source, target, payload));
    }
}
