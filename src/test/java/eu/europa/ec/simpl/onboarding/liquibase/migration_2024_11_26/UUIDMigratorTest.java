package eu.europa.ec.simpl.onboarding.liquibase.migration_2024_11_26;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.mock;

import eu.europa.ec.simpl.onboarding.liquibase.QueryExecutor;
import eu.europa.ec.simpl.onboarding.liquibase.QueryFactory;
import eu.europa.ec.simpl.onboarding.liquibase.migration_2024_11_26.pojo.Document;
import eu.europa.ec.simpl.onboarding.liquibase.migration_2024_11_26.pojo.DocumentTemplate;
import eu.europa.ec.simpl.onboarding.liquibase.migration_2024_11_26.pojo.MimeType;
import eu.europa.ec.simpl.onboarding.liquibase.migration_2024_11_26.pojo.OnboardingProcedureTemplate;
import eu.europa.ec.simpl.onboarding.liquibase.migration_2024_11_26.pojo.OnboardingRequest;
import eu.europa.ec.simpl.onboarding.liquibase.migration_2024_11_26.pojo.OnboardingStatus;
import eu.europa.ec.simpl.onboarding.liquibase.migration_2024_11_26.pojo.ParticipantType;
import java.util.Collections;
import liquibase.database.Database;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.CustomChangeException;
import liquibase.exception.SetupException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class UUIDMigratorTest {

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private JdbcConnection connection;

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private QueryFactory queryFactory;

    @Test
    public void executeTest() throws CustomChangeException {
        var database = mock(Database.class);
        var migrator = newUUIDMigrator();
        migrator.execute(database);
    }

    private UUIDMigrator newUUIDMigrator() {
        return new UUIDMigrator() {
            @Override
            protected QueryFactory newQueryFactory(JdbcConnection connection) {
                return queryFactory;
            }

            @Override
            protected JdbcConnection jdbcConnection(Database database) {
                return connection;
            }

            @Override
            protected <T> QueryExecutor<T> newQueryExecutor(QueryFactory factory, Class<T> clazz) {
                var mimeType = mock(QueryExecutor.class, RETURNS_DEEP_STUBS);
                given(mimeType.queryList(any())).willReturn(Collections.emptyList());

                var document = mock(QueryExecutor.class, RETURNS_DEEP_STUBS);
                given(document.queryList(any())).willReturn(Collections.emptyList());
                var documentTemplate = mock(QueryExecutor.class, RETURNS_DEEP_STUBS);
                given(documentTemplate.queryList(any())).willReturn(Collections.emptyList());
                var participantType = mock(QueryExecutor.class, RETURNS_DEEP_STUBS);
                given(participantType.queryList(any())).willReturn(Collections.emptyList());

                var onboardingRequest = mock(QueryExecutor.class, RETURNS_DEEP_STUBS);
                given(onboardingRequest.queryList(any())).willReturn(Collections.emptyList());
                var onboardingProcedureTemplate = mock(QueryExecutor.class, RETURNS_DEEP_STUBS);
                given(onboardingProcedureTemplate.queryList(any())).willReturn(Collections.emptyList());
                var onboardingStatus = mock(QueryExecutor.class, RETURNS_DEEP_STUBS);
                given(onboardingStatus.queryList(any())).willReturn(Collections.emptyList());

                if (clazz.equals(MimeType.class)) {
                    return (QueryExecutor<T>) mimeType;
                } else if (clazz.equals(Document.class)) {
                    return (QueryExecutor<T>) document;
                } else if (clazz.equals(DocumentTemplate.class)) {
                    return (QueryExecutor<T>) documentTemplate;
                } else if (clazz.equals(ParticipantType.class)) {
                    return (QueryExecutor<T>) participantType;
                } else if (clazz.equals(OnboardingRequest.class)) {
                    return (QueryExecutor<T>) onboardingRequest;
                } else if (clazz.equals(OnboardingProcedureTemplate.class)) {
                    return (QueryExecutor<T>) onboardingProcedureTemplate;
                } else if (clazz.equals(OnboardingStatus.class)) {
                    return (QueryExecutor<T>) onboardingStatus;
                }

                throw new RuntimeException("Not implemented");
            }
        };
    }

    @Test
    public void notImplementedFunctions_assertDoesNotThrow() throws SetupException {
        var database = mock(Database.class);
        given(database.getConnection()).willReturn(new JdbcConnection());
        var migrator = new UUIDMigrator();
        migrator.getConfirmationMessage();
        migrator.setUp();
        migrator.setFileOpener(null);
        migrator.validate(null);
        migrator.newQueryFactory(connection);
        migrator.jdbcConnection(database);
    }
}
