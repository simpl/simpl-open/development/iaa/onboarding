package eu.europa.ec.simpl.onboarding.statemachine.listener;

import static eu.europa.ec.simpl.common.test.TestUtil.a;
import static eu.europa.ec.simpl.common.test.TestUtil.an;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.europa.ec.simpl.common.model.dto.onboarding.CommentDTO;
import eu.europa.ec.simpl.onboarding.entities.OnboardingRequest;
import eu.europa.ec.simpl.onboarding.repositories.EventLogRepository;
import eu.europa.ec.simpl.onboarding.statemachine.AddCommentEvent;
import eu.europa.ec.simpl.onboarding.statemachine.OnboardingPayload;
import eu.europa.ec.simpl.onboarding.statemachine.OnboardingState;
import eu.europa.ec.simpl.onboarding.statemachine.OnboardingStateMapperImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.bean.override.mockito.MockitoBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
class OnboardingStateListenerTest {

    @MockitoBean
    private EventLogRepository eventLogRepository;

    @Autowired
    private OnboardingStateListener onboardingStateListener;

    @Test
    void afterHandle_success() {
        var onboardingRequest = an(OnboardingRequest.class);
        var payload = new OnboardingPayload<>(onboardingRequest);
        onboardingStateListener.afterHandle(
                OnboardingState.IN_PROGRESS, OnboardingState.REJECTED, Object.class, payload);
    }

    @Test
    void afterHandle_whenNewStatusInProgessAndAddCommentEvent_success() {
        var onboardingRequest = an(OnboardingRequest.class);
        var dto = a(CommentDTO.class);
        var payload = new OnboardingPayload<>(onboardingRequest, dto);
        onboardingStateListener.afterHandle(
                OnboardingState.IN_REVIEW, OnboardingState.IN_PROGRESS, AddCommentEvent.class, payload);
        verify(eventLogRepository, times(2)).save(any());
    }

    @Test
    void afterHandle_invalidPayloadType_NoOperation() {
        var payload = new Object();
        onboardingStateListener.afterHandle(
                OnboardingState.IN_REVIEW, OnboardingState.IN_PROGRESS, AddCommentEvent.class, payload);
        verify(eventLogRepository, never()).save(any());
    }

    @Configuration
    @Import({OnboardingStateListener.class, OnboardingStateMapperImpl.class})
    public static class Conf {
        @Bean
        public ObjectMapper objectMapper() {
            return new ObjectMapper();
        }
    }
}
