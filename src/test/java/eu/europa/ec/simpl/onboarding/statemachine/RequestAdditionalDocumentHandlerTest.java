package eu.europa.ec.simpl.onboarding.statemachine;

import static eu.europa.ec.simpl.common.test.TestUtil.an;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;

import eu.europa.ec.simpl.common.constants.Roles;
import eu.europa.ec.simpl.common.model.dto.onboarding.DocumentDTO;
import eu.europa.ec.simpl.common.security.JwtService;
import eu.europa.ec.simpl.onboarding.entities.Document;
import eu.europa.ec.simpl.onboarding.entities.OnboardingRequest;
import eu.europa.ec.simpl.onboarding.exceptions.OperationNotPermittedException;
import eu.europa.ec.simpl.onboarding.mappers.ImportMapper;
import eu.europa.ec.simpl.onboarding.repositories.*;
import eu.europa.ec.simpl.onboarding.services.DocumentService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.bean.override.mockito.MockitoBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@Import(RequestAdditionalDocumentHandler.class)
@ImportMapper
class RequestAdditionalDocumentHandlerTest {

    @MockitoBean
    private OnboardingRequestRepository onboardingRequestRepository;

    @MockitoBean
    private OnboardingStatusRepository onboardingStatusRepository;

    @MockitoBean
    private OnboardingTemplateRepository onboardingTemplateRepository;

    @MockitoBean
    private DocumentTemplateRepository documentTemplateRepository;

    @MockitoBean
    private DocumentRepository documentRepository;

    @MockitoBean
    private CommentRepository commentRepository;

    @MockitoBean
    private ParticipantTypeRepository participantTypeRepository;

    @MockitoBean
    private MimeTypeRepository mimeTypeRepository;

    @MockitoBean
    private JwtService jwtService;

    @MockitoBean
    private DocumentService documentService;

    @Autowired
    private RequestAdditionalDocumentHandler handler;

    @Test
    void handleTest() {
        OnboardingState source = OnboardingState.IN_REVIEW;
        OnboardingState target = OnboardingState.IN_REVIEW;
        var documentDTO = an(DocumentDTO.class);
        var id = documentDTO.getId();
        var onboardingRequest = an(OnboardingRequest.class);
        onboardingRequest.setId(id);
        var payload = new OnboardingPayload<>(onboardingRequest, documentDTO);
        var document = an(Document.class);
        document.setId(id);

        when(documentService.createDocument(payload.getActionPayload(), onboardingRequest.getId()))
                .thenReturn(documentDTO);
        given(documentRepository.getReferenceById(onboardingRequest.getId())).willReturn(document);

        assertDoesNotThrow(() -> handler.handle(source, target, payload));

        assertEquals(document, onboardingRequest.getDocuments().getLast());
    }

    @Test
    void guardTest_withNotaryRole_returnTrue() {
        OnboardingState source = OnboardingState.IN_REVIEW;
        OnboardingState target = OnboardingState.IN_REVIEW;
        var documentDTO = an(DocumentDTO.class);
        var id = documentDTO.getId();
        var onboardingRequest = an(OnboardingRequest.class);
        onboardingRequest.setId(id);
        var payload = new OnboardingPayload<>(onboardingRequest, documentDTO);
        var document = an(Document.class);
        document.setId(id);

        given(jwtService.hasRole(Roles.NOTARY)).willReturn(true);

        var ret = handler.guard(source, target, payload);
        assertEquals(true, ret);
    }

    @Test
    void guardTest_withoutNotaryRole_throwOperationNotPermittedException() {
        OnboardingState source = OnboardingState.IN_REVIEW;
        OnboardingState target = OnboardingState.IN_REVIEW;
        var documentDTO = an(DocumentDTO.class);
        var id = documentDTO.getId();
        var onboardingRequest = an(OnboardingRequest.class);
        onboardingRequest.setId(id);
        var payload = new OnboardingPayload<>(onboardingRequest, documentDTO);
        var document = an(Document.class);
        document.setId(id);

        given(jwtService.hasRole(Roles.NOTARY)).willReturn(false);

        assertThrows(OperationNotPermittedException.class, () -> handler.guard(source, target, payload));
    }
}
