package eu.europa.ec.simpl.onboarding.repositories;

import static eu.europa.ec.simpl.common.test.TestUtil.anOptional;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchException;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

import eu.europa.ec.simpl.onboarding.entities.Document;
import eu.europa.ec.simpl.onboarding.exceptions.DocumentNotFoundException;
import java.util.Optional;
import java.util.UUID;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class DocumentRepositoryTest {

    @Spy
    DocumentRepository documentRepository;

    @Test
    void findFullByIdOrThrow_whenFindFullByIdReturnsEntity_shouldNotThrowException() {
        given(documentRepository.findFullById(any())).willReturn(anOptional(Document.class));
        var exception = catchException(() -> documentRepository.findFullByIdOrThrow(any(UUID.class)));
        assertThat(exception).isNull();
    }

    @Test
    void findFullByIdOrThrow_whenFindFullByIdReturnsEmpty_shouldThrowDocumentNotFoundException() {
        given(documentRepository.findFullById(any())).willReturn(Optional.empty());
        var exception = catchException(() -> documentRepository.findFullByIdOrThrow(any(UUID.class)));
        assertThat(exception).isInstanceOf(DocumentNotFoundException.class);
    }

    @Test
    void findByIdOrThrow_whenfindByIdReturnsEntity_shouldNotThrowException() {
        given(documentRepository.findById(any())).willReturn(anOptional(Document.class));
        var exception = catchException(() -> documentRepository.findByIdOrThrow(any(UUID.class)));
        assertThat(exception).isNull();
    }

    @Test
    void findByIdOrThrow_whenfindByIdReturnsEmpty_shouldThrowDocumentNotFoundException() {
        given(documentRepository.findById(any())).willReturn(Optional.empty());
        var exception = catchException(() -> documentRepository.findByIdOrThrow(any(UUID.class)));
        assertThat(exception).isInstanceOf(DocumentNotFoundException.class);
    }
}
