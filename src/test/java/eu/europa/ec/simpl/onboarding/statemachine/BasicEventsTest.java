package eu.europa.ec.simpl.onboarding.statemachine;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import eu.europa.ec.simpl.onboarding.statemachine.common.BasicEvent;
import java.util.List;
import java.util.stream.Stream;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class BasicEventsTest {
    public static Stream<Arguments> getSourceAndTarget_expectNotNull() {
        return List.of(
                        UploadDocument.class,
                        Approve.class,
                        SubmitEvent.class,
                        TimeoutEvent.class,
                        RejectEvent.class,
                        RequestRevision.class,
                        RequestAdditionalDocument.class)
                .stream()
                .map(Arguments::of);
    }

    @ParameterizedTest
    @MethodSource
    public void getSourceAndTarget_expectNotNull(
            Class<? extends BasicEvent<OnboardingState, OnboardingPayload<? extends Object>>> eventObj) {
        var event = mock(eventObj);
        when(event.getSource()).thenCallRealMethod();
        when(event.getTarget()).thenCallRealMethod();
        assertNotNull(event.getSource());
        assertNotNull(event.getTarget());
    }
}
