package eu.europa.ec.simpl.onboarding.liquibase;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

import eu.europa.ec.simpl.common.exceptions.RuntimeWrapperException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import org.apache.commons.dbutils.QueryRunner;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class QueryTest {

    @Mock
    private Connection connection;

    @Mock
    private QueryRunner runner;

    @Test
    public void getResultTest_success() throws Exception {
        var expected = "success";
        when(runner.query(eq(connection), eq("junit-query"), any(), any(Object[].class)))
                .thenReturn("success");
        var query = new Query<>(connection, runner, String.class, "junit-query");
        var result = query.getResult();
        assertThat(result).isEqualTo(expected);
    }

    @Test
    public void getResultTest_throwSQLException_runtimeWrapperException() throws Exception {
        when(runner.query(eq(connection), eq("junit-query"), any(), any(Object[].class)))
                .thenThrow(SQLException.class);
        var query = new Query<>(connection, runner, String.class, "junit-query");
        assertThrows(RuntimeWrapperException.class, () -> query.getResult());
    }

    @Test
    public void getResultListTest_success() throws Exception {
        var expected = "success";
        when(runner.query(eq(connection), eq("junit-query"), any(), any(Object[].class)))
                .thenReturn(List.of("success"));
        var query = new Query<>(connection, runner, String.class, "junit-query");
        var result = query.getResultList();
        assertThat(result.get(0)).isEqualTo(expected);
    }

    @Test
    public void getResultLisTest_throwSQLException_runtimeWrapperException() throws Exception {
        when(runner.query(eq(connection), eq("junit-query"), any(), any(Object[].class)))
                .thenThrow(SQLException.class);
        var query = new Query<>(connection, runner, String.class, "junit-query");
        assertThrows(RuntimeWrapperException.class, () -> query.getResultList());
    }

    @Test
    public void insert_success() {
        var query = new Query<>(connection, runner, String.class, "junit-query");
        query.insert();
    }

    @Test
    public void insert_throwException() throws Exception {
        doThrow(SQLException.class).when(runner).update(eq(connection), eq("junit-query"), any(Object[].class));
        var query = new Query<>(connection, runner, String.class, "junit-query");
        assertThrows(RuntimeWrapperException.class, () -> query.insert());
    }

    @Test
    public void execute_success() {
        var query = new Query<>(connection, runner, String.class, "junit-query");
        query.execute();
    }

    @Test
    public void execute_throwException() throws Exception {
        doThrow(SQLException.class).when(runner).execute(eq(connection), eq("junit-query"), any(Object[].class));
        var query = new Query<>(connection, runner, String.class, "junit-query");
        assertThrows(RuntimeException.class, () -> query.execute());
    }
}
