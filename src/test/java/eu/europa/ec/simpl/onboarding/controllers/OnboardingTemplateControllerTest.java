package eu.europa.ec.simpl.onboarding.controllers;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import eu.europa.ec.simpl.common.model.dto.onboarding.OnboardingTemplateDTO;
import eu.europa.ec.simpl.onboarding.services.OnboardingTemplateService;
import java.util.List;
import java.util.UUID;
import org.instancio.Instancio;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@ExtendWith(MockitoExtension.class)
class OnboardingTemplateControllerTest {

    private MockMvc mockMvc;
    private ObjectMapper objectMapper;

    @Mock
    private OnboardingTemplateService onboardingTemplateService;

    @InjectMocks
    private OnboardingTemplateController onboardingTemplateController;

    private OnboardingTemplateDTO onboardingTemplateDTO;
    private UUID uuid;

    @BeforeEach
    void setUp() {
        // Configure ObjectMapper with JavaTimeModule
        objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());

        // Simple MockMvc setup without custom exception handling
        mockMvc = MockMvcBuilders.standaloneSetup(onboardingTemplateController).build();

        onboardingTemplateDTO = Instancio.create(OnboardingTemplateDTO.class);
        uuid = UUID.randomUUID();
    }

    @Test
    void updateOnboardingTemplate_WithValidInput_ReturnsUpdatedTemplate() throws Exception {
        onboardingTemplateDTO.setId(uuid);
        when(onboardingTemplateService.setOnboardingTemplate(any(UUID.class), any(OnboardingTemplateDTO.class)))
                .thenReturn(onboardingTemplateDTO);

        mockMvc.perform(put("/onboarding-template/{participantType}", UUID.randomUUID())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(onboardingTemplateDTO)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(onboardingTemplateDTO.getId().toString()));
    }

    @Test
    void getOnboardingTemplates_ReturnsListOfTemplates() throws Exception {
        List<OnboardingTemplateDTO> templates = List.of(onboardingTemplateDTO);
        given(onboardingTemplateService.getOnboardingTemplates()).willReturn(templates);

        mockMvc.perform(get("/onboarding-template"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json(objectMapper.writeValueAsString(templates)));
    }

    @Test
    void getOnboardingTemplate_WithValidParticipantType_ReturnsTemplate() throws Exception {
        given(onboardingTemplateService.findOnboardingTemplate(any(UUID.class))).willReturn(onboardingTemplateDTO);

        mockMvc.perform(get("/onboarding-template/{participantType}", UUID.randomUUID()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json(objectMapper.writeValueAsString(onboardingTemplateDTO)));
    }

    @Test
    void updateOnboardingTemplate_WithValidInput_ReturnsNoContent() throws Exception {
        List<UUID> uuids = List.of(uuid);
        doNothing().when(onboardingTemplateService).updateOnboardingTemplate(any(UUID.class), any());

        mockMvc.perform(patch("/onboarding-template/{participantType}", UUID.randomUUID())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(uuids)))
                .andExpect(status().isOk());
    }

    @Test
    void updateOnboardingTemplate_WithInvalidUUIDs_ReturnsBadRequest() throws Exception {
        List<String> invalidUuids = List.of("not-a-uuid");

        mockMvc.perform(patch("/onboarding-template/{participantType}", UUID.randomUUID())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(invalidUuids)))
                .andExpect(status().isBadRequest());
    }

    @Test
    void deleteOnboardingTemplate_WithValidParticipantType_ReturnsNoContent() throws Exception {
        doNothing().when(onboardingTemplateService).deleteOnboardingTemplate((any(UUID.class)));

        mockMvc.perform(delete("/onboarding-template/{participantType}", UUID.randomUUID()))
                .andExpect(status().isNoContent());
    }
}
