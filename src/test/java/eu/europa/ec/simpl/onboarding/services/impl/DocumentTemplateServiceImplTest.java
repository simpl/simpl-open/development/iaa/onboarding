package eu.europa.ec.simpl.onboarding.services.impl;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

import eu.europa.ec.simpl.common.model.dto.onboarding.DocumentTemplateDTO;
import eu.europa.ec.simpl.common.model.dto.onboarding.MimeTypeDTO;
import eu.europa.ec.simpl.common.test.TestUtil;
import eu.europa.ec.simpl.onboarding.entities.DocumentTemplate;
import eu.europa.ec.simpl.onboarding.entities.MimeType;
import eu.europa.ec.simpl.onboarding.exceptions.DocumentTemplateNotFoundException;
import eu.europa.ec.simpl.onboarding.mappers.DocumentTemplateMapper;
import eu.europa.ec.simpl.onboarding.mappers.ReferenceMapper;
import eu.europa.ec.simpl.onboarding.repositories.DocumentTemplateRepository;
import java.util.UUID;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class DocumentTemplateServiceImplTest {

    @InjectMocks
    private DocumentTemplateServiceImpl documentTemplateService;

    @Mock
    private DocumentTemplateRepository documentTemplateRepository;

    @Mock
    private ReferenceMapper referenceMapper;

    @Mock
    private DocumentTemplateMapper documentTemplateMapper;

    @Test
    void createDocumentTemplate_shouldSaveAndReturnUUID() {
        // Arrange
        var documentTemplateDTO = mock(DocumentTemplateDTO.class);
        var mimeType = mock(MimeType.class);
        var mimeTypeDTO = mock(MimeTypeDTO.class);
        var documentTemplate = mock(DocumentTemplate.class);
        var savedDocumentTemplate = mock(DocumentTemplate.class);
        var documentId = TestUtil.anUUID();

        given(documentTemplateDTO.getMimeType()).willReturn(mimeTypeDTO);
        when(referenceMapper.toMimeType(documentTemplateDTO.getMimeType().getId()))
                .thenReturn(mimeType);
        given(documentTemplateMapper.toEntity(documentTemplateDTO)).willReturn(documentTemplate);
        given(documentTemplate.setMimeType(mimeType)).willReturn(documentTemplate);
        given(documentTemplateRepository.saveAndFlush(documentTemplate)).willReturn(savedDocumentTemplate);
        given(savedDocumentTemplate.getId()).willReturn(documentId);

        // Act
        var result = documentTemplateService.createDocumentTemplate(documentTemplateDTO);

        // Assert
        assertThat(result).isEqualTo(documentId);
        verify(referenceMapper).toMimeType(documentTemplateDTO.getMimeType().getId());
        verify(documentTemplateMapper).toEntity(documentTemplateDTO);
        verify(documentTemplate).setMimeType(mimeType);
        verify(documentTemplateRepository).saveAndFlush(documentTemplate);
    }

    @Test
    void findDocumentTemplateById_shouldReturnDocumentTemplateDTO_whenExists() {
        // Arrange
        var uuid = UUID.randomUUID();
        var documentTemplate = mock(DocumentTemplate.class);
        var documentTemplateDTO = mock(DocumentTemplateDTO.class);

        given(documentTemplateRepository.findByIdOrThrow(uuid)).willReturn(documentTemplate);
        given(documentTemplateMapper.toDto(documentTemplate)).willReturn(documentTemplateDTO);

        // Act
        var result = documentTemplateService.findDocumentTemplateById(uuid);

        // Assert
        assertThat(result).isEqualTo(documentTemplateDTO);
        verify(documentTemplateRepository).findByIdOrThrow(uuid);
        verify(documentTemplateMapper).toDto(documentTemplate);
    }

    @Test
    void findDocumentTemplateById_shouldThrowException_whenNotFound() {
        // Arrange
        var uuid = UUID.randomUUID();

        when(documentTemplateRepository.findByIdOrThrow(uuid)).thenThrow(new DocumentTemplateNotFoundException(uuid));

        // Act & Assert
        assertThatThrownBy(() -> documentTemplateService.findDocumentTemplateById(uuid))
                .isInstanceOf(DocumentTemplateNotFoundException.class)
                .hasMessageContaining(uuid.toString());

        verify(documentTemplateRepository).findByIdOrThrow(uuid);
        verifyNoInteractions(documentTemplateMapper);
    }

    @Test
    void checkExistsByIdOrThrow_shouldPass_whenDocumentTemplateExists() {
        // Arrange
        var uuid = UUID.randomUUID();

        given(documentTemplateRepository.findByIdOrThrow(uuid)).willReturn(mock(DocumentTemplate.class));

        // Act & Assert (no exception)
        assertThatCode(() -> documentTemplateService.checkExistsByIdOrThrow(uuid))
                .doesNotThrowAnyException();

        verify(documentTemplateRepository).findByIdOrThrow(uuid);
    }

    @Test
    void checkExistsByIdOrThrow_shouldThrowException_whenDocumentTemplateDoesNotExist() {
        // Arrange
        var uuid = UUID.randomUUID();

        when(documentTemplateRepository.findByIdOrThrow(uuid)).thenThrow(new DocumentTemplateNotFoundException(uuid));

        // Act & Assert
        assertThatThrownBy(() -> documentTemplateService.checkExistsByIdOrThrow(uuid))
                .isInstanceOf(DocumentTemplateNotFoundException.class)
                .hasMessageContaining(uuid.toString());

        verify(documentTemplateRepository).findByIdOrThrow(uuid);
    }
}
