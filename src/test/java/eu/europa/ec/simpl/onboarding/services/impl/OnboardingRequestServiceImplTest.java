package eu.europa.ec.simpl.onboarding.services.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

import eu.europa.ec.simpl.common.filters.OnboardingRequestFilter;
import eu.europa.ec.simpl.common.model.dto.onboarding.*;
import eu.europa.ec.simpl.common.model.dto.onboarding.OnboardingStatusValue;
import eu.europa.ec.simpl.common.model.dto.onboarding.OnboardingTemplateDTO;
import eu.europa.ec.simpl.common.model.dto.onboarding.RejectDTO;
import eu.europa.ec.simpl.common.security.JwtService;
import eu.europa.ec.simpl.onboarding.entities.Document;
import eu.europa.ec.simpl.onboarding.entities.OnboardingApplicant;
import eu.europa.ec.simpl.onboarding.entities.OnboardingRequest;
import eu.europa.ec.simpl.onboarding.exceptions.DocumentNotFoundException;
import eu.europa.ec.simpl.onboarding.mappers.OnboardingRequestMapper;
import eu.europa.ec.simpl.onboarding.mappers.ReferenceMapper;
import eu.europa.ec.simpl.onboarding.repositories.CommentRepository;
import eu.europa.ec.simpl.onboarding.repositories.OnboardingRequestRepository;
import eu.europa.ec.simpl.onboarding.services.DocumentService;
import eu.europa.ec.simpl.onboarding.services.OnboardingApplicantService;
import eu.europa.ec.simpl.onboarding.services.OnboardingStatusService;
import eu.europa.ec.simpl.onboarding.services.OnboardingTemplateService;
import eu.europa.ec.simpl.onboarding.statemachine.*;
import eu.europa.ec.simpl.onboarding.statemachine.executor.OnboardingStateMachine;
import java.util.List;
import java.util.UUID;
import org.instancio.Instancio;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

@ExtendWith(MockitoExtension.class)
class OnboardingRequestServiceImplTest {
    @Mock
    private JwtService jwtService;

    @Mock
    private OnboardingTemplateService onboardingTemplateService;

    @Mock
    private OnboardingStatusService onboardingStatusService;

    @Mock
    private DocumentService documentService;

    @Mock
    private OnboardingRequestRepository onboardingRequestRepository;

    @Mock
    private OnboardingRequestMapper onboardingRequestMapper;

    @Mock
    private ReferenceMapper referenceMapper;

    @Mock
    private OnboardingApplicantService onboardingApplicantService;

    @Mock
    private CommentRepository commentRepository;

    @Mock
    private OnboardingStateMachine stateMachine;

    @InjectMocks
    private OnboardingRequestServiceImpl onboardingRequestService;

    @Test
    void createOnboardingRequest_shouldCreateAndSaveRequest() {
        var requestDTO = mockOnboardingRequestDTO();
        var template = mockOnboardingTemplate();
        var initialStatus = mockOnboardingStatus();
        var savedEntity = mockOnboardingRequest();

        given(onboardingTemplateService.findOnboardingTemplate(any(UUID.class))).willReturn(template);
        given(onboardingStatusService.getOnboardingStatus()).willReturn(List.of(initialStatus));
        given(onboardingApplicantService.create(any(), any(), any())).willReturn(new OnboardingApplicant());
        given(onboardingRequestMapper.create(any(), any(), any(), any())).willReturn(savedEntity);
        given(onboardingRequestRepository.saveAndFlush(any())).willReturn(savedEntity);
        given(documentService.createDocuments(any(), any())).willReturn(List.of());
        given(onboardingRequestMapper.toOnboardingRequestDto(any())).willReturn(requestDTO);

        var result = onboardingRequestService.create(requestDTO);

        assertThat(result).isEqualTo(requestDTO);
        verify(onboardingRequestRepository).saveAndFlush(savedEntity);
    }

    @Test
    void submitOnboardingRequest_shouldExecuteStateMachine() {
        var requestId = UUID.randomUUID();
        var initiator = "testUser";
        var expectedDTO = mockOnboardingRequestDTO();

        given(stateMachine.execute(requestId, SubmitEvent.class, initiator)).willReturn(expectedDTO);

        var result = onboardingRequestService.submit(requestId, initiator);

        assertThat(result).isEqualTo(expectedDTO);
        verify(stateMachine).execute(requestId, SubmitEvent.class, initiator);
    }

    @Test
    void searchOnboardingRequests_shouldFilterBasedOnUserRole() {
        var filter = new OnboardingRequestFilter();
        var pageable = PageRequest.of(0, 10);
        var mockRequest = mockOnboardingRequest();
        var mockPage = new PageImpl<>(List.of(mockRequest));
        var mockDTO = mockOnboardingRequestDTO();

        given(jwtService.hasRole(any())).willReturn(false);
        given(jwtService.getEmail()).willReturn("user@example.com");
        when(onboardingRequestRepository.findAll(any(Specification.class), any(Pageable.class)))
                .thenReturn(mockPage);
        given(onboardingRequestMapper.toOnboardingRequestDto(mockRequest)).willReturn(mockDTO);
        given(commentRepository.findByOnboardingRequestIdIn(any())).willReturn(List.of());

        var result = onboardingRequestService.search(filter, pageable);

        assertThat(result.getContent()).hasSize(1);
        verify(jwtService).getEmail();
    }

    private OnboardingRequestDTO mockOnboardingRequestDTO() {
        return Instancio.of(OnboardingRequestDTO.class).create();
    }

    private OnboardingTemplateDTO mockOnboardingTemplate() {
        return Instancio.of(OnboardingTemplateDTO.class).create();
    }

    private OnboardingStatusDTO mockOnboardingStatus() {
        var status = new OnboardingStatusDTO();
        status.setValue(OnboardingStatusValue.IN_PROGRESS);
        return status;
    }

    private OnboardingRequest mockOnboardingRequest() {
        return Instancio.of(OnboardingRequest.class).create();
    }

    @Test
    void approveOnboardingRequest_shouldExecuteStateMachine() {
        var requestId = UUID.randomUUID();
        var approveDTO = new ApproveDTO();
        var initiator = "testUser";
        var expectedDTO = mockOnboardingRequestDTO();

        when(stateMachine.execute(requestId, Approve.class, approveDTO, initiator))
                .thenReturn(expectedDTO);

        var result = onboardingRequestService.approve(requestId, approveDTO, initiator);

        assertThat(result).isEqualTo(expectedDTO);
        verify(stateMachine).execute(requestId, Approve.class, approveDTO, initiator);
    }

    @Test
    void rejectOnboardingRequest_shouldExecuteStateMachine() {
        var requestId = UUID.randomUUID();
        var rejectDTO = new RejectDTO();
        var initiator = "testUser";
        var expectedDTO = mockOnboardingRequestDTO();

        when(stateMachine.execute(requestId, RejectEvent.class, rejectDTO, initiator))
                .thenReturn(expectedDTO);

        var result = onboardingRequestService.reject(requestId, rejectDTO, initiator);

        assertThat(result).isEqualTo(expectedDTO);
        verify(stateMachine).execute(requestId, RejectEvent.class, rejectDTO, initiator);
    }

    @Test
    void uploadDocument_shouldExecuteStateMachine() {
        var requestId = UUID.randomUUID();
        var documentDTO = new DocumentDTO();
        var initiator = "testUser";
        var expectedDTO = mockOnboardingRequestDTO();

        when(stateMachine.execute(requestId, UploadDocument.class, documentDTO, initiator))
                .thenReturn(expectedDTO);

        var result = onboardingRequestService.uploadDocument(requestId, documentDTO, initiator);

        assertThat(result).isEqualTo(expectedDTO);
        verify(stateMachine).execute(requestId, UploadDocument.class, documentDTO, initiator);
    }

    @Test
    void requestAdditionalDocument_shouldExecuteStateMachine() {
        var requestId = UUID.randomUUID();
        var documentDTO = new DocumentDTO();
        var initiator = "testUser";
        var expectedDTO = mockOnboardingRequestDTO();

        when(stateMachine.execute(requestId, RequestAdditionalDocument.class, documentDTO, initiator))
                .thenReturn(expectedDTO);

        var result = onboardingRequestService.requestAdditionalDocument(requestId, documentDTO, initiator);

        assertThat(result).isEqualTo(expectedDTO);
        verify(stateMachine).execute(requestId, RequestAdditionalDocument.class, documentDTO, initiator);
    }

    @Test
    void requestRevision_shouldExecuteStateMachine() {
        var requestId = UUID.randomUUID();
        var initiator = "testUser";
        var expectedDTO = mockOnboardingRequestDTO();

        given(stateMachine.execute(requestId, RequestRevision.class, initiator)).willReturn(expectedDTO);

        var result = onboardingRequestService.requestRevision(requestId, initiator);

        assertThat(result).isEqualTo(expectedDTO);
        verify(stateMachine).execute(requestId, RequestRevision.class, initiator);
    }

    @Test
    void addComment_shouldExecuteStateMachine() {
        var requestId = UUID.randomUUID();
        var commentDTO = new CommentDTO();
        var initiator = "testUser";
        var expectedDTO = mockOnboardingRequestDTO();

        when(stateMachine.execute(requestId, AddCommentEvent.class, commentDTO, initiator))
                .thenReturn(expectedDTO);

        var result = onboardingRequestService.addComment(requestId, commentDTO, initiator);

        assertThat(result).isEqualTo(expectedDTO);
        verify(stateMachine).execute(requestId, AddCommentEvent.class, commentDTO, initiator);
    }

    @Test
    void getDocument_shouldReturnDocumentAfterValidatingRequest() {
        var requestId = UUID.randomUUID();
        var documentId = UUID.randomUUID();
        var expectedDocument = new DocumentDTO();

        given(onboardingRequestRepository.findByIdOrThrow(requestId)).willReturn(mockOnboardingRequest());
        given(documentService.getDocument(documentId)).willReturn(expectedDocument);

        var result = onboardingRequestService.getDocument(requestId, documentId);

        assertThat(result).isEqualTo(expectedDocument);
        verify(onboardingRequestRepository).findByIdOrThrow(requestId);
        verify(documentService).getDocument(documentId);
    }

    @Test
    void setExpirationTimeframe_shouldUpdateOnboardingRequest() {
        var requestId = UUID.randomUUID();
        var expirationTimeframe = 30L;
        var mockRequest = mockOnboardingRequest();

        given(onboardingRequestRepository.findByIdOrThrow(requestId)).willReturn(mockRequest);

        onboardingRequestService.setExpirationTimeframe(requestId, expirationTimeframe);

        verify(onboardingRequestRepository).findByIdOrThrow(requestId);
        verify(onboardingRequestRepository).save(mockRequest);
        assertThat(mockRequest.getExpirationTimeframe()).isEqualTo(expirationTimeframe);
    }

    @Test
    void deleteDocument_shouldClearDocumentContentWhenDocumentExists() {
        var requestId = UUID.randomUUID();
        var documentId = UUID.randomUUID();
        var mockDocument = new Document();
        mockDocument.setId(documentId);
        OnboardingRequestDTO mockRequestDTO = mockOnboardingRequestDTO();
        mockRequestDTO.setDocuments(List.of(new DocumentDTO().setId(documentId)));

        given(onboardingRequestService.getOnboardingRequest(requestId)).willReturn(mockRequestDTO);

        onboardingRequestService.deleteDocument(requestId, documentId);

        verify(documentService).clearDocumentContent(documentId);
    }

    @Test
    void deleteDocument_shouldThrowExceptionWhenDocumentNotFound() {
        var requestId = UUID.randomUUID();
        var documentId = UUID.randomUUID();
        var mockRequestDTO = mockOnboardingRequestDTO();
        mockRequestDTO.setDocuments(List.of());

        given(onboardingRequestService.getOnboardingRequest(requestId)).willReturn(mockRequestDTO);

        assertThatThrownBy(() -> onboardingRequestService.deleteDocument(requestId, documentId))
                .isInstanceOf(DocumentNotFoundException.class)
                .hasMessageContaining(documentId.toString());
    }
}
