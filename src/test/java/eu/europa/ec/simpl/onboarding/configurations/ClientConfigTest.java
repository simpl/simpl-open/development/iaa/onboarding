package eu.europa.ec.simpl.onboarding.configurations;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@EnableAutoConfiguration(
        exclude = {
            DataSourceAutoConfiguration.class,
        })
@EnableConfigurationProperties({
    MicroserviceProperties.class,
})
@TestPropertySource(
        properties = {
            "microservice.usersRoles.url=http://localhost:8080",
            "microservice.identityProvider.url=http://localhost:8080",
        })
public class ClientConfigTest {

    @Autowired
    private ApplicationContext ac;

    @Test
    public void loadConfigurationTest() {
        try (var tcn = new AnnotationConfigApplicationContext()) {
            tcn.setParent(ac);
            tcn.register(ClientConfig.class);
            tcn.refresh();
        }
    }
}
