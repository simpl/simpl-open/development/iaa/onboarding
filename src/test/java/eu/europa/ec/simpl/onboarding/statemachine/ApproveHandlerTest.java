package eu.europa.ec.simpl.onboarding.statemachine;

import static eu.europa.ec.simpl.common.test.TestUtil.an;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.given;

import eu.europa.ec.simpl.common.constants.Roles;
import eu.europa.ec.simpl.common.exchanges.identityprovider.IdentityProviderParticipantExchange;
import eu.europa.ec.simpl.common.model.dto.onboarding.ApproveDTO;
import eu.europa.ec.simpl.common.security.JwtService;
import eu.europa.ec.simpl.onboarding.entities.OnboardingRequest;
import eu.europa.ec.simpl.onboarding.exceptions.OperationNotPermittedException;
import eu.europa.ec.simpl.onboarding.mappers.ImportMapper;
import eu.europa.ec.simpl.onboarding.repositories.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.bean.override.mockito.MockitoBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ImportMapper
@Import(ApproveHandler.class)
@ExtendWith(SpringExtension.class)
class ApproveHandlerTest {

    @MockitoBean
    private OnboardingRequestRepository onboardingRequestRepository;

    @MockitoBean
    private OnboardingStatusRepository onboardingStatusRepository;

    @MockitoBean
    private OnboardingTemplateRepository onboardingTemplateRepository;

    @MockitoBean
    private DocumentTemplateRepository documentTemplateRepository;

    @MockitoBean
    private DocumentRepository documentRepository;

    @MockitoBean
    private CommentRepository commentRepository;

    @MockitoBean
    private ParticipantTypeRepository participantTypeRepository;

    @MockitoBean
    private MimeTypeRepository mimeTypeRepository;

    @MockitoBean
    private JwtService jwtService;

    @MockitoBean
    private StatusChangeHandler statusChangeHandler;

    @MockitoBean
    private IdentityProviderParticipantExchange participantExchange;

    @Autowired
    private ApproveHandler handler;

    @Test
    void handleTest_success() {
        var source = an(OnboardingState.class);
        var target = an(OnboardingState.class);
        var onboardingRequest = an(OnboardingRequest.class);
        var approveDTO = an(ApproveDTO.class);
        var payload = new OnboardingPayload<ApproveDTO>(onboardingRequest, approveDTO);

        handler.handle(source, target, payload);
    }

    @Test
    void guardTest_isNotary_success() {
        var source = an(OnboardingState.class);
        var target = an(OnboardingState.class);
        var onboardingRequest = an(OnboardingRequest.class);
        var approveDTO = an(ApproveDTO.class);
        var payload = new OnboardingPayload<ApproveDTO>(onboardingRequest, approveDTO);

        given(jwtService.hasRole(Roles.NOTARY)).willReturn(true);

        handler.guard(source, target, payload);
    }

    @Test
    void guardTest_isNotNoray_throwOperationNotPermittedException() {
        var source = an(OnboardingState.class);
        var target = an(OnboardingState.class);
        var onboardingRequest = an(OnboardingRequest.class);
        var approveDTO = an(ApproveDTO.class);
        var payload = new OnboardingPayload<ApproveDTO>(onboardingRequest, approveDTO);

        given(jwtService.hasRole(Roles.NOTARY)).willReturn(false);

        assertThrows(OperationNotPermittedException.class, () -> handler.guard(source, target, payload));
    }
}
