package eu.europa.ec.simpl.onboarding.exceptions;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

import org.junit.jupiter.api.Test;

/**
 * OnboardingStatusNotFoundExceptionTest
 */
public class OnboardingStatusNotFoundExceptionTest {

    @Test
    public void constructorWitoutArgumentTest() {
        assertDoesNotThrow(() -> new OnboardingStatusNotFoundException());
    }

    @Test
    public void constructorWithArgumentTest() {
        assertDoesNotThrow(() -> new OnboardingStatusNotFoundException("junit-test"));
    }
}
