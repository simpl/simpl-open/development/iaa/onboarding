package eu.europa.ec.simpl.onboarding.statemachine;

import static eu.europa.ec.simpl.common.test.TestUtil.a;
import static eu.europa.ec.simpl.common.test.TestUtil.an;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchException;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

import eu.europa.ec.simpl.common.model.dto.onboarding.OnboardingStatusDTO;
import eu.europa.ec.simpl.onboarding.entities.OnboardingRequest;
import eu.europa.ec.simpl.onboarding.mappers.ReferenceMapper;
import eu.europa.ec.simpl.onboarding.services.OnboardingStatusService;
import java.time.Clock;
import java.time.Instant;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class TimeoutEventHandlerTest {

    @InjectMocks
    TimeoutEventHandler timeoutEventHandler;

    @Mock
    ReferenceMapper referenceMapper;

    @Mock
    OnboardingStateMapper onboardingStateMapper;

    @Mock
    OnboardingStatusService onboardingStatusService;

    @Mock
    Clock clock;

    @Test
    void handle_success() {
        given(onboardingStatusService.findOnboardingStatusByStatus(any())).willReturn(an(OnboardingStatusDTO.class));
        var payload = onboardingPayload();
        var exception = catchException(
                () -> timeoutEventHandler.handle(OnboardingState.IN_REVIEW, OnboardingState.REJECTED, payload));
        assertThat(exception).isNull();
    }

    @Test
    void guard_validCriteria_success() {
        var mockNow = Instant.parse("2024-11-22T00:00:20+00:00");
        given(clock.instant()).willReturn(mockNow);
        var payload = onboardingPayload();
        assertTrue(timeoutEventHandler.guard(OnboardingState.IN_REVIEW, OnboardingState.REJECTED, payload));
    }

    @Test
    void guard_InvalidCriteria_returnFalse() {
        var payload = onboardingPayload();
        var mockNow = Instant.parse("2024-11-22T00:00:05+00:00");
        given(clock.instant()).willReturn(mockNow);
        assertFalse(timeoutEventHandler.guard(OnboardingState.IN_REVIEW, OnboardingState.REJECTED, payload));
    }

    private OnboardingRequest request(String update, long expirationTimeframe) {
        var request = a(OnboardingRequest.class);
        request.setUpdateTimestamp(Instant.parse(update));
        request.setExpirationTimeframe(expirationTimeframe);
        return request;
    }

    private OnboardingPayload<Void> onboardingPayload() {
        return new OnboardingPayload<>(request("2024-11-22T00:00:00+00:00", 10));
    }
}
