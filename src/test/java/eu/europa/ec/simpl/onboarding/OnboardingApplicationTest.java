package eu.europa.ec.simpl.onboarding;

import static org.assertj.core.api.Assertions.assertThat;

import eu.europa.ec.simpl.common.exchanges.identityprovider.IdentityProviderParticipantExchange;
import eu.europa.ec.simpl.common.exchanges.usersroles.UserExchange;
import eu.europa.ec.simpl.onboarding.configurations.MicroserviceProperties;
import eu.europa.ec.simpl.onboarding.configurations.SchedulingConfig;
import eu.europa.ec.simpl.onboarding.controllers.OnboardingRequestController;
import eu.europa.ec.simpl.onboarding.repositories.*;
import eu.europa.ec.simpl.onboarding.scheduled.RejectionOfStaleOnboardingRequestsTask;
import eu.europa.ec.simpl.onboarding.services.OnboardingApplicantService;
import eu.europa.ec.simpl.onboarding.services.OnboardingRequestService;
import eu.europa.ec.simpl.onboarding.services.UserService;
import eu.europa.ec.simpl.onboarding.statemachine.ApproveHandler;
import net.javacrumbs.shedlock.core.LockProvider;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.ssl.SslAutoConfiguration;
import org.springframework.boot.ssl.SslBundles;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.bean.override.mockito.MockitoBean;

@SpringBootTest
@EnableAutoConfiguration(exclude = {DataSourceAutoConfiguration.class, SslAutoConfiguration.class})
class OnboardingApplicationTest {
    @MockitoBean
    private OnboardingApplicantRepository onboardingApplicantRepository;

    @MockitoBean
    private DocumentTemplateRepository documentTemplateRepository;

    @MockitoBean
    private MimeTypeRepository mimeTypeRepository;

    @MockitoBean
    private OnboardingRequestRepository onboardingRequestRepository;

    @MockitoBean
    private OnboardingTemplateRepository onboardingTemplateRepository;

    @MockitoBean
    private OnboardingStatusRepository onboardingStatusRepository;

    @MockitoBean
    private ParticipantTypeRepository participantTypeRepository;

    @MockitoBean
    private DocumentRepository documentRepository;

    @MockitoBean
    private CommentRepository commentRepository;

    @MockitoBean
    private EventLogRepository eventLogRepository;

    @MockitoBean
    private LockProvider lockProvider;

    @MockitoBean
    private MicroserviceProperties microserviceProperties;

    @MockitoBean
    private SslBundles sslBundles;

    @MockitoBean
    private SchedulingConfig schedulingConfig;

    @MockitoBean
    private OnboardingRequestController onboardingRequestController;

    @MockitoBean
    private RejectionOfStaleOnboardingRequestsTask rejectionOfStaleOnboardingRequestsTask;

    @MockitoBean
    private OnboardingApplicantService onboardingApplicantService;

    @MockitoBean
    private OnboardingRequestService onboardingRequestService;

    @MockitoBean
    private ApproveHandler approveHandler;

    @MockitoBean
    private UserService userService;

    @MockitoBean
    private UserExchange userExchange;

    @MockitoBean
    private IdentityProviderParticipantExchange participantExchange;

    @Autowired
    ApplicationContext applicationContext;

    @Test
    void contextLoads() {
        assertThat(applicationContext).isNotNull();
    }
}
