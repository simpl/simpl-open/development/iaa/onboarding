package eu.europa.ec.simpl.onboarding.services.impl;

import static eu.europa.ec.simpl.common.test.TestUtil.an;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

import eu.europa.ec.simpl.common.model.dto.onboarding.OnboardingApplicantDTO;
import eu.europa.ec.simpl.common.model.dto.onboarding.OnboardingTemplateDTO;
import eu.europa.ec.simpl.common.model.dto.onboarding.ParticipantTypeDTO;
import eu.europa.ec.simpl.onboarding.entities.OnboardingApplicant;
import eu.europa.ec.simpl.onboarding.mappers.ImportMapper;
import eu.europa.ec.simpl.onboarding.repositories.*;
import eu.europa.ec.simpl.onboarding.services.OnboardingTemplateService;
import eu.europa.ec.simpl.onboarding.services.ParticipantTypeService;
import eu.europa.ec.simpl.onboarding.services.UserService;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.bean.override.mockito.MockitoBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@ImportMapper
@Import(OnboardingApplicantServiceImpl.class)
class OnboardingApplicantServiceImplTest {
    @MockitoBean
    private OnboardingRequestRepository onboardingRequestRepository;

    @MockitoBean
    private OnboardingStatusRepository onboardingStatusRepository;

    @MockitoBean
    private OnboardingTemplateRepository onboardingTemplateRepository;

    @MockitoBean
    private DocumentTemplateRepository documentTemplateRepository;

    @MockitoBean
    private DocumentRepository documentRepository;

    @MockitoBean
    private CommentRepository commentRepository;

    @MockitoBean
    private ParticipantTypeRepository participantTypeRepository;

    @MockitoBean
    private MimeTypeRepository mimeTypeRepository;

    @MockitoBean
    private UserService userService;

    @MockitoBean
    private ParticipantTypeService participantTypeService;

    @MockitoBean
    private OnboardingTemplateService onboardingTemplateService;

    @MockitoBean
    private OnboardingApplicantRepository onboardingApplicantRepository;

    @Autowired
    private OnboardingApplicantServiceImpl service;

    @Test
    void createTest() {
        var applicantDTO = an(OnboardingApplicantDTO.class);
        var pType = an(ParticipantTypeDTO.class);
        var organization = "JUnitOrg";

        given(participantTypeService.findParticipantTypeById(pType.getId())).willReturn(pType);
        var email = "junit@email.com";
        var onboardingApplicant = an(OnboardingApplicant.class);
        given(onboardingApplicantRepository.findByEmail(email)).willReturn(List.of(onboardingApplicant));

        var onboardingTemplateDTO = an(OnboardingTemplateDTO.class);
        given(onboardingTemplateService.findOnboardingTemplate(pType.getId())).willReturn(onboardingTemplateDTO);

        given(userService.createUser(any())).willReturn("mockcreateUser");

        assertDoesNotThrow(() -> service.create(applicantDTO, pType, organization));
        verify(onboardingApplicantRepository).saveAndFlush(argThat(arg -> {
            assertNotNull(arg);
            return true;
        }));
    }
}
