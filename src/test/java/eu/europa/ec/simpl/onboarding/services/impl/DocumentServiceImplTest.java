package eu.europa.ec.simpl.onboarding.services.impl;

import static eu.europa.ec.simpl.common.test.TestUtil.a;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willAnswer;
import static org.mockito.Mockito.*;

import eu.europa.ec.simpl.common.model.dto.onboarding.DocumentDTO;
import eu.europa.ec.simpl.onboarding.entities.Document;
import eu.europa.ec.simpl.onboarding.exceptions.DocumentNotFoundException;
import eu.europa.ec.simpl.onboarding.mappers.BlobMapper;
import eu.europa.ec.simpl.onboarding.mappers.DocumentMapper;
import eu.europa.ec.simpl.onboarding.repositories.DocumentRepository;
import java.util.List;
import java.util.UUID;
import javax.sql.rowset.serial.SerialBlob;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;

@ExtendWith(MockitoExtension.class)
class DocumentServiceImplTest {

    @Mock
    private DocumentMapper documentMapper;

    @Mock
    private DocumentRepository documentRepository;

    @Mock
    private BlobMapper blobMapper;

    @InjectMocks
    private DocumentServiceImpl documentService;

    private UUID documentId;
    private UUID onboardingRequestId;
    private DocumentDTO documentDTO;
    private Document document;

    @BeforeEach
    void setUp() {
        documentId = UUID.randomUUID();
        onboardingRequestId = UUID.randomUUID();
        documentDTO = a(DocumentDTO.class).setId(documentId).setContent("sample content");
        document = a(Document.class).setId(documentId);
    }

    @Test
    void createDocument_success() {
        // Given
        given(documentMapper.toEntity(documentDTO, onboardingRequestId)).willReturn(document);
        given(documentMapper.toDTO(document)).willReturn(documentDTO);
        given(documentRepository.saveAndFlush(any())).willReturn(document);

        // When
        var result = documentService.createDocument(documentDTO, onboardingRequestId);

        // Then
        assertThat(result).isEqualTo(documentDTO);
    }

    @Test
    void createDocuments_shouldReturnListOfUUIDs_whenDocumentsAreSaved() {
        // Given
        var documentDTOList = List.of(documentDTO);
        var savedDocuments = List.of(document);
        given(documentMapper.toEntity(documentDTO, onboardingRequestId)).willReturn(document);
        given(documentMapper.toDTO(document)).willReturn(documentDTO);
        given(documentRepository.saveAllAndFlush(anyList())).willReturn(savedDocuments);

        // When
        var result = documentService.createDocuments(documentDTOList, onboardingRequestId);

        // Then
        assertThat(result).containsExactlyElementsOf(documentDTOList);
        verify(documentMapper, times(1)).toEntity(documentDTO, onboardingRequestId);
        verify(documentRepository, times(1)).saveAllAndFlush(anyList());
    }

    @Test
    void uploadDocument_shouldSaveContent() throws Exception {
        // Given
        var blob = new SerialBlob("sample content".getBytes());
        given(documentRepository.findByIdOrThrow(documentId)).willReturn(document);
        willAnswer(setDocumentContent(blob)).given(documentMapper).updateDocument(any(), any());

        // When
        documentService.uploadDocument(documentDTO);

        // Then
        verify(documentRepository, times(1)).findByIdOrThrow(documentId);
        assertThat(document.getContent()).isEqualTo(blob);
    }

    private static Answer<Object> setDocumentContent(SerialBlob blob) {
        return invocation -> {
            var document = (Document) invocation.getArgument(0);
            document.setContent(blob);
            return null;
        };
    }

    @Test
    void uploadDocument_shouldThrowDocumentNotFoundException_whenDocumentDoesNotExist() {
        // Given
        given(documentRepository.findByIdOrThrow(documentId))
                .willThrow(new DocumentNotFoundException(UUID.randomUUID()));

        // When / Then
        assertThrows(DocumentNotFoundException.class, () -> documentService.uploadDocument(documentDTO));
        verify(documentRepository, times(1)).findByIdOrThrow(documentId);
        verify(documentRepository, never()).save(any());
    }

    @Test
    void getDocument_shouldReturnDocumentDTO_whenDocumentExists() {
        // Given
        given(documentRepository.findFullByIdOrThrow(documentId)).willReturn(document);
        given(documentMapper.toFullDTO(document)).willReturn(documentDTO);

        // When
        var result = documentService.getDocument(documentId);

        // Then
        assertThat(result).isEqualTo(documentDTO);
        verify(documentRepository, times(1)).findFullByIdOrThrow(documentId);
        verify(documentMapper, times(1)).toFullDTO(document);
    }

    @Test
    void getDocument_shouldThrowEntityNotFoundException_whenDocumentDoesNotExist() {
        // Given
        given(documentRepository.findFullByIdOrThrow(documentId))
                .willThrow(new DocumentNotFoundException(UUID.randomUUID()));

        // When / Then
        assertThrows(DocumentNotFoundException.class, () -> documentService.getDocument(documentId));
        verify(documentRepository, times(1)).findFullByIdOrThrow(documentId);
        verify(documentMapper, never()).toFullDTO(any());
    }

    @Test
    void clearDocumentTest_success() {
        // Given
        var testingDocument = a(Document.class);
        given(documentRepository.findByIdOrThrow(documentId)).willReturn(testingDocument);

        // When
        documentService.clearDocumentContent(documentId);

        // Then
        assertThat(testingDocument.getContent()).isNull();
        assertThat(testingDocument.getFilename()).isNull();
        assertThat(testingDocument.getFilesize()).isNull();
    }
}
