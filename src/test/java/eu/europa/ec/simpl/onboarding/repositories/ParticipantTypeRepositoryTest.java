package eu.europa.ec.simpl.onboarding.repositories;

import static eu.europa.ec.simpl.common.test.TestUtil.anOptional;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.BDDAssertions.catchException;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

import eu.europa.ec.simpl.onboarding.entities.ParticipantType;
import eu.europa.ec.simpl.onboarding.exceptions.ParticipantTypeNotFoundException;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class ParticipantTypeRepositoryTest {
    @Spy
    ParticipantTypeRepository participantTypeRepository;

    @Test
    void findByIdOrThrow_whenFindByIdOrThrowReturnsEntity_shouldNotThrowException() {
        given(participantTypeRepository.findByValueIgnoreCase(any())).willReturn(anOptional(ParticipantType.class));
        var exception = catchException(() -> participantTypeRepository.findByValueIgnoreCaseOrThrow("test"));
        assertThat(exception).isNull();
    }

    @Test
    void findByIdOrThrow_whenFindByIdOrThrowReturnsEmpty_shouldThrowCertificateNotFoundException() {
        given(participantTypeRepository.findByValueIgnoreCase(any())).willReturn(Optional.empty());
        var exception = catchException(() -> participantTypeRepository.findByValueIgnoreCaseOrThrow("test"));
        assertThat(exception).isInstanceOf(ParticipantTypeNotFoundException.class);
    }
}
