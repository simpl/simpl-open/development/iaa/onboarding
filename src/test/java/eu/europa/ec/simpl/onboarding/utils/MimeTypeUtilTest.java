package eu.europa.ec.simpl.onboarding.utils;

import static org.junit.jupiter.api.Assertions.assertThrows;

import eu.europa.ec.simpl.common.exceptions.RuntimeWrapperException;
import org.apache.tika.mime.MimeTypes;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

class MimeTypeUtilTest {

    @Test
    void getAllAvailableMimeTypes_whenSearchElement_returnFilteredElements() {
        var types = MimeTypeUtil.getAllAvailableMimeTypes("xml");
        Assertions.assertThat(types).as("All mime types available").isNotEmpty();
    }

    @Test
    void matches_whenXml_thenFalse() {
        Assertions.assertThat(MimeTypeUtil.hasMatchingMimeType(MimeTypes.XML, "SGVsbG8sIFdvcmxkXCE="))
                .as("A non XML content doesn't match")
                .isFalse();
    }

    @Test
    void matches_whenInvalid_thenThrowException() {
        assertThrows(
                RuntimeWrapperException.class,
                () -> MimeTypeUtil.hasMatchingMimeType("invalid", "SGVsbG8sIFdvcmxkXCE="));
    }
}
