package eu.europa.ec.simpl.onboarding.statemachine.common;

import lombok.extern.log4j.Log4j2;

@Log4j2
public abstract class AbstractTestEvent implements BasicEvent<TestState, TestPayload> {

    private final TestState source;
    private final TestState target;

    public AbstractTestEvent(TestState source, TestState target) {
        this.source = source;
        this.target = target;
    }

    @Override
    public void handle(TestState source, TestState target, TestPayload payload) {
        payload.setHandlerExecuted(true);
        if (payload.getHandlerException() != null) {
            throw payload.getHandlerException();
        }
        log.info("{} {}", getClass().getSimpleName(), payload.getMessage());
    }

    @Override
    public boolean guard(TestState source, TestState target, TestPayload payload) {
        payload.setGuardExecuted(true);
        if (payload.getGuardException() != null) {
            throw payload.getGuardException();
        }
        return payload.getGuardResult();
    }

    @Override
    public TestState getSource() {
        return source;
    }

    @Override
    public TestState getTarget() {
        return target;
    }
}
