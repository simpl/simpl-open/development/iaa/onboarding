package eu.europa.ec.simpl.onboarding.statemachine;

import static eu.europa.ec.simpl.common.test.TestUtil.an;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

import eu.europa.ec.simpl.common.constants.Roles;
import eu.europa.ec.simpl.common.security.JwtService;
import eu.europa.ec.simpl.onboarding.entities.OnboardingRequest;
import eu.europa.ec.simpl.onboarding.exceptions.MissingMandatoryDocumentsException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.bean.override.mockito.MockitoBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@Import(SubmitEventHandler.class)
class SubmitEventHandlerTest {

    @MockitoBean
    private JwtService jwtService;

    @MockitoBean
    private StatusChangeHandler statusChangeHandler;

    @Autowired
    private SubmitEventHandler handler;

    @Test
    void handleTest_success() {
        var source = an(OnboardingState.class);
        var target = an(OnboardingState.class);
        var onboardingRequest = an(OnboardingRequest.class);
        var payload = new OnboardingPayload<Void>(onboardingRequest);

        handler.handle(source, target, payload);

        verify(statusChangeHandler).changeStatus(payload.getOnboardingRequest(), target);
    }

    @Test
    void guard_hasRoleApplicant_success() {
        var email = "junit@email.com";
        var source = an(OnboardingState.class);
        var target = an(OnboardingState.class);
        var onboardingRequest = an(OnboardingRequest.class);
        var payload = new OnboardingPayload<Void>(onboardingRequest);

        onboardingRequest.getOnboardingApplicant().setEmail(email);
        given(jwtService.getEmail()).willReturn(email);

        given(jwtService.hasRole(Roles.APPLICANT)).willReturn(true);
        var ret = handler.guard(source, target, payload);
        assertTrue(ret);
    }

    @Test
    void guard_missingMandatoryDocument_throwMissingMandatoryDocumentsException() {
        var email = "junit@email.com";
        var source = an(OnboardingState.class);
        var target = an(OnboardingState.class);
        var onboardingRequest = an(OnboardingRequest.class);
        var payload = new OnboardingPayload<Void>(onboardingRequest);
        var mandatoryDocument = spy(onboardingRequest.getDocuments().remove(0));
        onboardingRequest.getDocuments().add(mandatoryDocument);
        doReturn(true).when(mandatoryDocument).isMandatory();
        doReturn(false).when(mandatoryDocument).getHasContent();

        onboardingRequest.getOnboardingApplicant().setEmail(email);
        given(jwtService.getEmail()).willReturn(email);

        given(jwtService.hasRole(Roles.APPLICANT)).willReturn(true);
        assertThrows(MissingMandatoryDocumentsException.class, () -> handler.guard(source, target, payload));
    }
}
