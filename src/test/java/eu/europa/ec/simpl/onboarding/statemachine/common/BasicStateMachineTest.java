package eu.europa.ec.simpl.onboarding.statemachine.common;

import static eu.europa.ec.simpl.onboarding.statemachine.OnboardingState.*;
import static org.assertj.core.api.Assertions.catchException;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

import eu.europa.ec.simpl.onboarding.statemachine.OnboardingState;
import java.util.Map;
import java.util.function.Consumer;
import org.junit.jupiter.api.Test;

class BasicStateMachineTest {

    enum Event {
        SUBMIT,
        REQUEST_INFORMATION,
        REJECT,
        APPROVE,
        ABORT,
        TIMEOUT,
        ADD_DOCUMENT
    }

    private final Map<Map.Entry<OnboardingState, Event>, Map.Entry<OnboardingState, Consumer<Object>>> stateMachine =
            Map.of(
                    from(IN_PROGRESS, Event.SUBMIT), to(IN_REVIEW, this::submit),
                    from(IN_PROGRESS, Event.ABORT), to(REJECTED, this::abort),
                    from(IN_PROGRESS, Event.TIMEOUT), to(REJECTED, this::timeout),
                    from(IN_PROGRESS, Event.ADD_DOCUMENT), to(IN_PROGRESS, this::addDocument),
                    from(IN_REVIEW, Event.REQUEST_INFORMATION), to(IN_PROGRESS, this::requestInformation),
                    from(IN_REVIEW, Event.APPROVE), to(APPROVED, this::approve),
                    from(IN_REVIEW, Event.REJECT), to(REJECTED, this::reject));

    private OnboardingState exec(OnboardingState state, Event event) {
        var to = stateMachine.get(Map.entry(state, event));
        to.getValue().accept(new Object());
        return to.getKey();
    }

    private void submit(Object payload) {
        System.out.println("submit");
    }

    private void requestInformation(Object payload) {
        System.out.println("requestInformation");
    }

    private void abort(Object payload) {
        System.out.println("abort");
    }

    private void timeout(Object payload) {
        System.out.println("timeout");
    }

    private void approve(Object payload) {
        System.out.println("approve");
    }

    private void reject(Object payload) {
        System.out.println("reject");
    }

    private void addDocument(Object payload) {
        System.out.println("addDocument");
    }

    private static Map.Entry<OnboardingState, Event> from(OnboardingState state, Event event) {
        return Map.entry(state, event);
    }

    private static Map.Entry<OnboardingState, Consumer<Object>> to(OnboardingState state, Consumer<Object> handler) {
        return Map.entry(state, handler);
    }

    @Test
    void test() {
        assertThat(exec(IN_PROGRESS, Event.SUBMIT)).isEqualTo(IN_REVIEW);
        assertThat(exec(IN_PROGRESS, Event.ABORT)).isEqualTo(REJECTED);
        assertThat(exec(IN_PROGRESS, Event.TIMEOUT)).isEqualTo(REJECTED);
        assertThat(exec(IN_REVIEW, Event.REQUEST_INFORMATION)).isEqualTo(IN_PROGRESS);
        assertThat(exec(IN_REVIEW, Event.REJECT)).isEqualTo(REJECTED);
        assertThat(exec(IN_REVIEW, Event.APPROVE)).isEqualTo(APPROVED);
        assertThat(exec(IN_PROGRESS, Event.ADD_DOCUMENT)).isEqualTo(IN_PROGRESS);

        assertThat(catchException(() -> exec(IN_PROGRESS, Event.REJECT))).isNotNull();
        assertThat(catchException(() -> exec(IN_PROGRESS, Event.APPROVE))).isNotNull();

        assertThat(catchException(() -> exec(APPROVED, Event.SUBMIT))).isNotNull();
        assertThat(catchException(() -> exec(REJECTED, Event.SUBMIT))).isNotNull();

        assertThat(catchException(() -> exec(APPROVED, Event.APPROVE))).isNotNull();
        assertThat(catchException(() -> exec(REJECTED, Event.REJECT))).isNotNull();

        // ...
    }
}
