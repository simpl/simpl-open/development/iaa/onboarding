package eu.europa.ec.simpl.onboarding.controllers;

import static eu.europa.ec.simpl.common.test.TestUtil.a;
import static eu.europa.ec.simpl.common.test.TestUtil.aListOf;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import eu.europa.ec.simpl.common.model.dto.onboarding.MimeTypeDTO;
import eu.europa.ec.simpl.onboarding.services.MimeTypeService;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@ExtendWith(MockitoExtension.class)
class MimeTypeControllerTest {

    private MockMvc mockMvc;

    @Mock
    private MimeTypeService mimeTypeService;

    @InjectMocks
    private MimeTypeController mimeTypeController;

    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(mimeTypeController)
                .setCustomArgumentResolvers(new PageableHandlerMethodArgumentResolver())
                .build();
    }

    @Test
    void createMimeType_shouldReturnCreatedStatus() throws Exception {
        MimeTypeDTO mimeTypeDTO = new MimeTypeDTO();
        mimeTypeDTO.setValue("application/json");
        var uuid = UUID.randomUUID();
        given(mimeTypeService.createMimeType(any(MimeTypeDTO.class))).willReturn(uuid);

        mockMvc.perform(post("/mime-type")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"value\": \"application/json\"}"))
                .andExpect(status().isCreated())
                .andExpect(content().string("\"" + uuid + "\""));
    }

    @Test
    void getAllMimeTypes_shouldReturnPageOfMimeTypes() throws Exception {
        var result = aListOf(MimeTypeDTO.class);
        PageRequest pageable = PageRequest.of(0, 10);
        var page = new PageImpl<>(result, pageable, 1);
        given(mimeTypeService.search(any(Pageable.class))).willReturn(page);

        mockMvc.perform(get("/mime-type").param("page", "0").param("size", "10"))
                .andExpect(status().isOk());
    }

    @Test
    void getMimeType_shouldReturnMimeType() throws Exception {
        var uuid = UUID.randomUUID();
        MimeTypeDTO mimeTypeDTO = a(MimeTypeDTO.class).setValue("application/json");
        given(mimeTypeService.findById(uuid)).willReturn(mimeTypeDTO);

        mockMvc.perform(get("/mime-type/" + uuid))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.value").value("application/json"));
    }

    @Test
    void updateMimeType_shouldReturnNoContentStatus() throws Exception {
        mockMvc.perform(put("/mime-type/0193057f-096b-75ac-bfec-27ad89c73ba3")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"value\": \"application/json\"}"))
                .andExpect(status().isOk());
    }

    @Test
    void deleteMimeType_shouldReturnNoContentStatus() throws Exception {
        var uuid = UUID.randomUUID();
        mockMvc.perform(delete("/mime-type/" + uuid)).andExpect(status().isNoContent());
    }
}
