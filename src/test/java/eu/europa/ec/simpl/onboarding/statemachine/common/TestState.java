package eu.europa.ec.simpl.onboarding.statemachine.common;

public enum TestState implements State {
    START(Type.INITIAL),
    STATE_1(Type.STATE),
    STATE_2(Type.STATE),
    END(Type.FINAL);

    private final Type type;

    TestState(Type type) {
        this.type = type;
    }

    @Override
    public Type getType() {
        return type;
    }
}
