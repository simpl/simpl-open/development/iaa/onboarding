package eu.europa.ec.simpl.onboarding.statemachine.common;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@NoArgsConstructor
public class TestPayload {
    private String message = "";
    private Boolean guardResult = true;
    private RuntimeException handlerException;
    private RuntimeException guardException;
    private Boolean handlerExecuted = false;
    private Boolean guardExecuted = false;

    public TestPayload(String message) {
        this.message = message;
    }
}
