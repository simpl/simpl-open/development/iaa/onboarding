package eu.europa.ec.simpl.onboarding.repositories.specifications;

import static org.mockito.Mockito.*;

import eu.europa.ec.simpl.common.filters.OnboardingRequestFilter;
import eu.europa.ec.simpl.onboarding.entities.OnboardingApplicant;
import eu.europa.ec.simpl.onboarding.entities.OnboardingRequest;
import eu.europa.ec.simpl.onboarding.entities.OnboardingRequest_;
import eu.europa.ec.simpl.onboarding.entities.OnboardingStatus;
import jakarta.persistence.criteria.*;
import java.time.Instant;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

class OnboardingRequestSpecificationTest {

    @Mock
    private Root<OnboardingRequest> root;

    @Mock
    private CriteriaQuery<?> query;

    @Mock
    private CriteriaBuilder criteriaBuilder;

    @Mock
    private Path<String> applicantEmailPath;

    @Mock
    private Path<OnboardingStatus> onboardingStatusPath;

    @Mock
    private Path<OnboardingApplicant> onboardingApplicantPath;

    @Mock
    private Path<Instant> updateTimestampPath;

    private OnboardingRequestSpecification specification;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        when(root.get(OnboardingRequest_.onboardingApplicant)).thenReturn(onboardingApplicantPath);
        when(root.get(OnboardingRequest_.onboardingStatus)).thenReturn(onboardingStatusPath);
        when(root.get(OnboardingRequest_.updateTimestamp)).thenReturn(updateTimestampPath);
    }

    @Test
    void toPredicate_noFiltersProvided_shouldNotCreatePredicates() {
        var emptyFilter = new OnboardingRequestFilter();
        specification = new OnboardingRequestSpecification(emptyFilter);

        specification.toPredicate(root, query, criteriaBuilder);

        verify(criteriaBuilder, never()).equal(any(Expression.class), any());
        verify(criteriaBuilder, never()).like(any(Expression.class), anyString());
        verify(criteriaBuilder, never()).greaterThanOrEqualTo(any(Expression.class), any(Instant.class));
        verify(criteriaBuilder, never()).lessThanOrEqualTo(any(Expression.class), any(Instant.class));
    }
}
