package eu.europa.ec.simpl.onboarding.exceptions;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

import java.util.UUID;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

public class CannotEditOnboardingRequestExceptionTest {

    @Test
    public void constructorWithUserIdTest() {
        assertDoesNotThrow(() -> new CannotEditOnboardingRequestException("junit-users-id", UUID.randomUUID()));
    }

    @Test
    public void constructorWithHttpStatusTest() {
        assertDoesNotThrow(() -> new CannotEditOnboardingRequestException(HttpStatus.CONFLICT, "junit-test-message"));
    }
}
