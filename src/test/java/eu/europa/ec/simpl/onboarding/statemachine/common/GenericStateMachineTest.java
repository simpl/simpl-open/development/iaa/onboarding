package eu.europa.ec.simpl.onboarding.statemachine.common;

import static eu.europa.ec.simpl.onboarding.statemachine.common.TestState.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchException;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class GenericStateMachineTest {

    GenericStateMachine<TestState> sm;

    @BeforeEach
    void setupStateMachine() {
        this.sm = new GenericStateMachine<>();

        sm.addTransition(new Event1(START, STATE_1));
        sm.addTransition(new Event3(START, STATE_2));
        sm.addTransition(new Event1(STATE_1, STATE_2));
        sm.addTransition(new Event3(STATE_2, STATE_2));
        sm.addTransition(new Event2(STATE_2, STATE_1));
        sm.addTransition(new Event2(STATE_1, END));

        sm.init();
        sm.start();
    }

    @Test
    void stop_stateMachineNotInitialized_shoudThrowIllegalStateException() {
        var genericStateMachine = new GenericStateMachine<>();
        assertThrows(IllegalStateException.class, () -> genericStateMachine.stop());
    }

    @Test
    void sendEvent_stateMachineNotInitialized_shoudThrowIllegalStateException() {
        var genericStateMachine = new GenericStateMachine<>();
        assertThrows(IllegalStateException.class, () -> genericStateMachine.sendEvent(null, null));
    }

    @Test
    void reset_stateMachineNotInitialized_shoudThrowIllegalStateException() {
        var genericStateMachine = new GenericStateMachine<>();
        assertThrows(IllegalStateException.class, () -> genericStateMachine.reset(null));
    }

    @Test
    void start_stateMachineNotInitialized_shoudThrowIllegalStateException() {
        var genericStateMachine = new GenericStateMachine<>();
        assertThrows(IllegalStateException.class, () -> genericStateMachine.start());
    }

    @Test
    void init_stateMachineAlreadyInitialized_shoudThrowIllegalStateException() {
        assertThrows(IllegalStateException.class, () -> sm.init());
    }

    @Test
    void addTransition_stateMachineAlreadyInitialized_shoudThrowIllegalStateException() {
        assertThrows(IllegalStateException.class, () -> sm.addTransition(null));
    }

    @Test
    void stateMachine_whenTransitionsAreDefined_shouldExecuteTransitions() {

        assertThat(sm.getState()).isEqualTo(START);
        assertThat(sm.isComplete()).isFalse();

        sm.sendEvent(TestEvent1.class, new TestPayload("START -> STATE_1"));
        assertThat(sm.getState()).isEqualTo(STATE_1);

        sm.sendEvent(TestEvent1.class, new TestPayload("STATE_1 -> STATE_2"));
        assertThat(sm.getState()).isEqualTo(STATE_2);

        sm.sendEvent(TestEvent2.class, new TestPayload("STATE_2 -> STATE_1"));
        assertThat(sm.getState()).isEqualTo(STATE_1);

        sm.sendEvent(TestEvent2.class, new TestPayload("STATE_1 -> END"));
        assertThat(sm.getState()).isEqualTo(END);

        assertThat(sm.isComplete()).isTrue();
    }

    @Test
    void stateMachine_whenTransitionIsNotDefined_shouldThrowIllegalTransitionException() {
        var state = sm.getState();
        var payload = new TestPayload();
        var exception = catchException(() -> sm.sendEvent(TestEvent2.class, payload));
        assertThat(exception).isInstanceOf(IllegalTransitionException.class);
        assertThat(sm.getState()).isEqualTo(state);
        assertThat(payload.getGuardExecuted()).isFalse();
        assertThat(payload.getHandlerExecuted()).isFalse();
    }

    @Test
    void stateMachine_whenGuardReturnsFalse_shouldThrowGuardFailedException() {
        var state = sm.getState();
        var payload = new TestPayload().setGuardResult(false);
        var exception = catchException(() -> sm.sendEvent(TestEvent1.class, payload));
        assertThat(exception).isInstanceOf(GuardFailedException.class);
        assertThat(sm.getState()).isEqualTo(state);
        assertThat(payload.getGuardExecuted()).isTrue();
        assertThat(payload.getHandlerExecuted()).isFalse();
    }

    @Test
    void stateMachine_whenGuardThrowsException_shouldThrowTheException() {
        var state = sm.getState();
        var payload = new TestPayload().setGuardException(new TestException());
        var exception = catchException(() -> sm.sendEvent(TestEvent1.class, payload));
        assertThat(exception).isInstanceOf(TestException.class);
        assertThat(sm.getState()).isEqualTo(state);
        assertThat(payload.getGuardExecuted()).isTrue();
        assertThat(payload.getHandlerExecuted()).isFalse();
    }

    @Test
    void stateMachine_whenHandlerThrowsException_shouldThrowTheException() {
        var state = sm.getState();
        var payload = new TestPayload().setHandlerException(new TestException());
        var exception = catchException(() -> sm.sendEvent(TestEvent1.class, payload));
        assertThat(exception).isInstanceOf(TestException.class);
        assertThat(sm.getState()).isEqualTo(state);
        assertThat(payload.getGuardExecuted()).isTrue();
        assertThat(payload.getHandlerExecuted()).isTrue();
    }

    @Test
    void stateMachine_withListener_whenTransitionHappens_listenerShouldBeInvoked() {

        var listener = new TestListener();

        sm.reset(STATE_2);
        sm.listeners().add(listener);
        sm.sendEvent(TestEvent3.class, new TestPayload());
        assertThat(listener.getInvoked()).isTrue();
    }

    @Test
    void stateMachine_reset_shouldResetTheStateMachineToTheGivenState() {
        sm.reset(STATE_2);

        assertThat(sm.getState()).isEqualTo(STATE_2);
        assertThat(sm.isComplete()).isFalse();

        sm.sendEvent(TestEvent2.class, new TestPayload("STATE_2 -> STATE_1"));
        assertThat(sm.getState()).isEqualTo(STATE_1);

        sm.sendEvent(TestEvent2.class, new TestPayload("STATE_1 -> END"));
        assertThat(sm.getState()).isEqualTo(END);

        assertThat(sm.isComplete()).isTrue();
    }

    @Test
    void stateMachine_withListener_whenHandlerThrowsException_listenerShouldNotBeInvoked() {

        var listener = new TestListener();

        sm.reset(STATE_2);
        sm.listeners().add(listener);
        var exception = catchException(
                () -> sm.sendEvent(TestEvent3.class, new TestPayload().setHandlerException(new TestException())));
        assertThat(listener.getInvoked()).isFalse();
        assertThat(exception).isNotNull();
    }

    @Test
    void stateMachine_withListener_whenGuardThrowsException_listenerShouldNotBeInvoked() {

        var listener = new TestListener();

        sm.reset(STATE_2);
        sm.listeners().add(listener);
        var exception = catchException(
                () -> sm.sendEvent(TestEvent3.class, new TestPayload().setGuardException(new TestException())));
        assertThat(listener.getInvoked()).isFalse();
        assertThat(exception).isNotNull();
    }

    @Test
    void stateMachine_withListener_whenGuardReturnsFalse_listenerShouldNotBeInvoked() {

        var listener = new TestListener();

        sm.reset(STATE_2);
        sm.listeners().add(listener);
        var exception = catchException(() -> sm.sendEvent(TestEvent3.class, new TestPayload().setGuardResult(false)));
        assertThat(listener.getInvoked()).isFalse();
        assertThat(exception).isNotNull();
    }
}
