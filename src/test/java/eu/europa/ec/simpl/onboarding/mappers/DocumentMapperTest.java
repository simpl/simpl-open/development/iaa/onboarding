package eu.europa.ec.simpl.onboarding.mappers;

import com.nimbusds.jose.util.Base64;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class DocumentMapperTest {

    @Spy
    private DocumentMapperImpl documentMapper;

    @Test
    void testToFilesize() {
        String base64 = Base64.encode("dq3uihri3hr192eh").toString();
        Assertions.assertThat(documentMapper.toFilesize(base64))
                .as("The size of a base64 string")
                .isPositive();
    }
}
