package eu.europa.ec.simpl.onboarding.exceptions;

import eu.europa.ec.simpl.common.exceptions.StatusException;
import org.springframework.http.HttpStatus;

public class UserCreationException extends StatusException {
    public UserCreationException(HttpStatus status, String message) {
        super(status, message);
    }
}
