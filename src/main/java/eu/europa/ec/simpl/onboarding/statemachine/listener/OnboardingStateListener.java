package eu.europa.ec.simpl.onboarding.statemachine.listener;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.europa.ec.simpl.common.exceptions.RuntimeWrapperException;
import eu.europa.ec.simpl.common.model.dto.onboarding.CommentDTO;
import eu.europa.ec.simpl.common.model.dto.onboarding.OnboardingStatusValue;
import eu.europa.ec.simpl.onboarding.entities.EventLog;
import eu.europa.ec.simpl.onboarding.repositories.EventLogRepository;
import eu.europa.ec.simpl.onboarding.statemachine.AddCommentEvent;
import eu.europa.ec.simpl.onboarding.statemachine.OnboardingPayload;
import eu.europa.ec.simpl.onboarding.statemachine.OnboardingState;
import eu.europa.ec.simpl.onboarding.statemachine.OnboardingStateMapper;
import eu.europa.ec.simpl.onboarding.statemachine.common.GenericStateMachine;
import java.time.Instant;
import java.util.Map;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

@Log4j2
@Component
public class OnboardingStateListener implements GenericStateMachine.Listener<OnboardingState> {

    public static final String COMMENT_INSERTED = "COMMENT_INSERTED";
    public static final String STATUS_CHANGED = "STATUS_CHANGED";

    private final ObjectMapper objectMapper;
    private final OnboardingStateMapper onboardingStateMapper;
    private final EventLogRepository eventLogRepository;

    public OnboardingStateListener(
            ObjectMapper objectMapper,
            OnboardingStateMapper onboardingStateMapper,
            EventLogRepository eventLogRepository) {
        this.objectMapper = objectMapper;
        this.onboardingStateMapper = onboardingStateMapper;
        this.eventLogRepository = eventLogRepository;
    }

    @Override
    public void afterHandle(OnboardingState oldState, OnboardingState newState, Class<?> event, Object payload) {
        var oldStatus = onboardingStateMapper.toOnboardingStatus(oldState);
        var newStatus = onboardingStateMapper.toOnboardingStatus(newState);

        if (!(payload instanceof OnboardingPayload<?> onboardingPayload)) {
            log.warn("Unexpected payload type {}", payload.getClass());
            return;
        }

        if (oldStatus != newStatus) {
            onboardingPayload.getOnboardingRequest().setLastStatusUpdateTimestamp(Instant.now());

            var eventLog = createEventLog(onboardingPayload);
            eventLog.setEventType(STATUS_CHANGED);
            var eventDetails = Map.of("previousStatus", oldStatus, "newStatus", newStatus);
            eventLog.setEventDetails(toJson(eventDetails));
            eventLogRepository.save(eventLog);
        }

        if (event.equals(AddCommentEvent.class)) {
            var eventLog = createEventLog(onboardingPayload);
            var comment = (CommentDTO) onboardingPayload.getActionPayload();
            eventLog.setEventType(COMMENT_INSERTED);
            eventLog.setEntityId(comment.getId());
            eventLogRepository.save(eventLog);
        }
    }

    private String toJson(Map<String, OnboardingStatusValue> eventDetails) {
        try {
            return objectMapper.writeValueAsString(eventDetails);
        } catch (JsonProcessingException e) {
            throw new RuntimeWrapperException(e);
        }
    }

    private static EventLog createEventLog(OnboardingPayload<?> onboardingPayload) {
        return new EventLog()
                .setOnboardingRequestId(onboardingPayload.getOnboardingRequest().getId())
                .setInitiatorUserId(onboardingPayload.getInitiatorUserId())
                .setInitiatorService(onboardingPayload.getInitiatorService());
    }
}
