package eu.europa.ec.simpl.onboarding.statemachine;

import eu.europa.ec.simpl.common.model.dto.onboarding.OnboardingStatusValue;
import eu.europa.ec.simpl.onboarding.entities.OnboardingStatus;
import eu.europa.ec.simpl.onboarding.mappers.ReferenceMapper;
import eu.europa.ec.simpl.onboarding.services.OnboardingStatusService;
import java.time.Clock;
import java.util.Objects;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class TimeoutEventHandler implements TimeoutEvent {

    private final Clock clock;
    private final OnboardingStateMapper onboardingStateMapper;
    private final ReferenceMapper referenceMapper;
    private final OnboardingStatusService onboardingStatusService;

    public TimeoutEventHandler(
            @Autowired(required = false) Clock clock,
            OnboardingStateMapper onboardingStateMapper,
            ReferenceMapper referenceMapper,
            OnboardingStatusService onboardingStatusService) {
        this.clock = Objects.requireNonNullElse(clock, Clock.systemUTC());
        this.onboardingStateMapper = onboardingStateMapper;
        this.referenceMapper = referenceMapper;
        this.onboardingStatusService = onboardingStatusService;
    }

    @Override
    public void handle(OnboardingState source, OnboardingState target, OnboardingPayload<Void> payload) {
        var request = payload.getOnboardingRequest();
        var targetStatus = onboardingStateMapper.toOnboardingStatus(target);
        var onboardingStatus = getOnboardingStatusReference(targetStatus);
        request.setRejectionCause("Expired time. Id: %s, Update timestamp: %s, Current timestamp: %s"
                .formatted(request.getId(), request.getUpdateTimestamp(), clock.instant()));
        request.setOnboardingStatus(onboardingStatus);
    }

    @Override
    public boolean guard(OnboardingState source, OnboardingState target, OnboardingPayload<Void> payload) {
        var request = payload.getOnboardingRequest();
        var updateTimestamp = request.getUpdateTimestamp();
        var expirationTimeframe = request.getExpirationTimeframe();
        if (clock.instant().compareTo(updateTimestamp.plusSeconds(expirationTimeframe)) < 0) {
            log.error(
                    "Invalid criteria for timeout. Id {}, Update timestamp {}, Expiration time frame: {}",
                    request.getId(),
                    updateTimestamp,
                    expirationTimeframe);
            return false;
        }
        return true;
    }

    private OnboardingStatus getOnboardingStatusReference(OnboardingStatusValue status) {
        return referenceMapper.toOnboardingStatus(
                onboardingStatusService.findOnboardingStatusByStatus(status).getId());
    }
}
