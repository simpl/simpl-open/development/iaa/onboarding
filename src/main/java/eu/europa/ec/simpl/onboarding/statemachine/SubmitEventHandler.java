package eu.europa.ec.simpl.onboarding.statemachine;

import eu.europa.ec.simpl.common.security.JwtService;
import eu.europa.ec.simpl.onboarding.entities.OnboardingRequest;
import eu.europa.ec.simpl.onboarding.exceptions.MissingMandatoryDocumentsException;
import java.util.List;
import org.springframework.stereotype.Component;

@Component
public class SubmitEventHandler implements SubmitEvent, ApplicantEvent<Void> {

    private final JwtService jwtService;
    private final StatusChangeHandler statusChangeHandler;

    public SubmitEventHandler(JwtService jwtService, StatusChangeHandler statusChangeHandler) {
        this.jwtService = jwtService;
        this.statusChangeHandler = statusChangeHandler;
    }

    @Override
    public void handle(OnboardingState source, OnboardingState target, OnboardingPayload<Void> payload) {
        statusChangeHandler.changeStatus(payload.getOnboardingRequest(), target);
    }

    @Override
    public boolean guard(OnboardingState source, OnboardingState target, OnboardingPayload<Void> payload) {
        return ApplicantEvent.super.guard(source, target, payload) && checkMissingMandatoryDocumentsOrThrow(payload);
    }

    private boolean checkMissingMandatoryDocumentsOrThrow(OnboardingPayload<Void> payload) {
        if (hasMissingMandatoryDocuments(payload.getOnboardingRequest())) {
            throw new MissingMandatoryDocumentsException(
                    List.of()); // TODO Pass missing documents via state machine context
        }
        return true;
    }

    @Override
    public JwtService getJwtService() {
        return jwtService;
    }

    private boolean hasMissingMandatoryDocuments(OnboardingRequest onboardingRequest) {
        return onboardingRequest.getDocuments().stream()
                .anyMatch(document -> document.isMandatory() && !document.getHasContent());
    }
}
