package eu.europa.ec.simpl.onboarding.mappers;

import eu.europa.ec.simpl.api.onboarding.v1.model.OnboardingTemplateDTO;
import java.util.List;
import lombok.Generated;
import org.mapstruct.AnnotateWith;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(unmappedSourcePolicy = ReportingPolicy.ERROR, unmappedTargetPolicy = ReportingPolicy.ERROR)
@AnnotateWith(Generated.class)
public interface OnboardingTemplateMapperV1 {
    OnboardingTemplateDTO toV1(eu.europa.ec.simpl.common.model.dto.onboarding.OnboardingTemplateDTO onboardingTemplate);

    List<OnboardingTemplateDTO> toV1(
            List<eu.europa.ec.simpl.common.model.dto.onboarding.OnboardingTemplateDTO> onboardingTemplates);

    eu.europa.ec.simpl.common.model.dto.onboarding.OnboardingTemplateDTO toV0(
            OnboardingTemplateDTO onboardingTemplateDTO);
}
