package eu.europa.ec.simpl.onboarding.exceptions;

import eu.europa.ec.simpl.common.exceptions.StatusException;
import java.util.UUID;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class OnboardingTemplateNotFoundException extends StatusException {

    public OnboardingTemplateNotFoundException(UUID participantTypeId) {
        this(HttpStatus.NOT_FOUND, participantTypeId.toString());
    }

    public OnboardingTemplateNotFoundException(String pType) {
        this(HttpStatus.NOT_FOUND, pType);
    }

    public OnboardingTemplateNotFoundException(HttpStatus status, String pType) {
        super(status, "Onboarding template not found for participant type [ %s ]".formatted(pType));
    }
}
