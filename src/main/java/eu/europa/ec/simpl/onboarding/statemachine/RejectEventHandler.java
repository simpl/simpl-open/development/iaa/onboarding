package eu.europa.ec.simpl.onboarding.statemachine;

import eu.europa.ec.simpl.common.model.dto.onboarding.RejectDTO;
import eu.europa.ec.simpl.common.security.JwtService;
import org.springframework.stereotype.Component;

@Component
public class RejectEventHandler implements RejectEvent, NotaryEvent<RejectDTO> {

    private final JwtService jwtService;
    private final StatusChangeHandler statusChangeHandler;

    public RejectEventHandler(JwtService jwtService, StatusChangeHandler statusChangeHandler) {
        this.jwtService = jwtService;
        this.statusChangeHandler = statusChangeHandler;
    }

    @Override
    public void handle(OnboardingState source, OnboardingState target, OnboardingPayload<RejectDTO> payload) {
        var onboardingRequest = payload.getOnboardingRequest();
        statusChangeHandler.changeStatus(onboardingRequest, target);
        onboardingRequest.setRejectionCause(payload.getActionPayload().getRejectionCause());
    }

    @Override
    public boolean guard(OnboardingState source, OnboardingState target, OnboardingPayload<RejectDTO> payload) {
        return NotaryEvent.super.guard(source, target, payload);
    }

    @Override
    public JwtService getJwtService() {
        return jwtService;
    }
}
