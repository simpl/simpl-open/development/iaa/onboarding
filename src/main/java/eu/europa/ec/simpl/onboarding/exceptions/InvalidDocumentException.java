package eu.europa.ec.simpl.onboarding.exceptions;

import eu.europa.ec.simpl.common.exceptions.StatusException;
import eu.europa.ec.simpl.onboarding.enums.ViolationType;
import java.util.UUID;
import org.springframework.http.HttpStatus;

public class InvalidDocumentException extends StatusException {

    public InvalidDocumentException(UUID documentId, ViolationType violationType) {
        super(
                HttpStatus.BAD_REQUEST,
                "Provided document do not match those required for onboarding request: [%s : %s]"
                        .formatted(documentId, violationType.name()));
    }

    public InvalidDocumentException(String message) {
        super(HttpStatus.BAD_REQUEST, message);
    }
}
