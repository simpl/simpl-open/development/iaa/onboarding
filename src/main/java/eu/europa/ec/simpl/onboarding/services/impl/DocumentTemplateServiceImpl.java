package eu.europa.ec.simpl.onboarding.services.impl;

import eu.europa.ec.simpl.common.model.dto.onboarding.DocumentTemplateDTO;
import eu.europa.ec.simpl.onboarding.mappers.DocumentTemplateMapper;
import eu.europa.ec.simpl.onboarding.mappers.ReferenceMapper;
import eu.europa.ec.simpl.onboarding.repositories.DocumentTemplateRepository;
import eu.europa.ec.simpl.onboarding.services.DocumentTemplateService;
import java.util.UUID;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

@Log4j2
@Service
@Validated
public class DocumentTemplateServiceImpl implements DocumentTemplateService {

    private final DocumentTemplateRepository documentTemplateRepository;
    private final ReferenceMapper referenceMapper;
    private final DocumentTemplateMapper mapper;

    public DocumentTemplateServiceImpl(
            DocumentTemplateRepository documentTemplateRepository,
            ReferenceMapper referenceMapper,
            DocumentTemplateMapper mapper) {
        this.documentTemplateRepository = documentTemplateRepository;
        this.referenceMapper = referenceMapper;
        this.mapper = mapper;
    }

    @Override
    @Transactional
    public UUID createDocumentTemplate(DocumentTemplateDTO documentTemplate) {
        var mimeType = referenceMapper.toMimeType(documentTemplate.getMimeType().getId());
        var entity = mapper.toEntity(documentTemplate).setMimeType(mimeType);
        return documentTemplateRepository.saveAndFlush(entity).getId();
    }

    @Override
    public DocumentTemplateDTO findDocumentTemplateById(UUID uuid) {
        return mapper.toDto(documentTemplateRepository.findByIdOrThrow(uuid));
    }

    @Override
    public void checkExistsByIdOrThrow(UUID id) {
        documentTemplateRepository.findByIdOrThrow(id);
    }
}
