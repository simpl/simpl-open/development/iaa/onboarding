package eu.europa.ec.simpl.onboarding.exceptions;

import eu.europa.ec.simpl.common.exceptions.StatusException;
import org.springframework.http.HttpStatus;

public class OnboardingStatusNotFoundException extends StatusException {
    public OnboardingStatusNotFoundException() {
        super(HttpStatus.NOT_FOUND, "No onboarding status found with isInitial = true");
    }

    public OnboardingStatusNotFoundException(String value) {
        super(HttpStatus.NOT_FOUND, "No onboarding status found with value = %s".formatted(value));
    }
}
