package eu.europa.ec.simpl.onboarding.mappers;

import eu.europa.ec.simpl.onboarding.entities.Comment;
import eu.europa.ec.simpl.onboarding.entities.Document;
import eu.europa.ec.simpl.onboarding.entities.DocumentTemplate;
import eu.europa.ec.simpl.onboarding.entities.MimeType;
import eu.europa.ec.simpl.onboarding.entities.OnboardingProcedureTemplate;
import eu.europa.ec.simpl.onboarding.entities.OnboardingRequest;
import eu.europa.ec.simpl.onboarding.entities.OnboardingStatus;
import eu.europa.ec.simpl.onboarding.entities.ParticipantType;
import eu.europa.ec.simpl.onboarding.repositories.CommentRepository;
import eu.europa.ec.simpl.onboarding.repositories.DocumentRepository;
import eu.europa.ec.simpl.onboarding.repositories.DocumentTemplateRepository;
import eu.europa.ec.simpl.onboarding.repositories.MimeTypeRepository;
import eu.europa.ec.simpl.onboarding.repositories.OnboardingRequestRepository;
import eu.europa.ec.simpl.onboarding.repositories.OnboardingStatusRepository;
import eu.europa.ec.simpl.onboarding.repositories.OnboardingTemplateRepository;
import eu.europa.ec.simpl.onboarding.repositories.ParticipantTypeRepository;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.springframework.stereotype.Component;

@Component
public class ReferenceMapper {

    private final OnboardingRequestRepository onboardingRequestRepository;
    private final OnboardingStatusRepository onboardingStatusRepository;
    private final OnboardingTemplateRepository onboardingTemplateRepository;
    private final DocumentTemplateRepository documentTemplateRepository;
    private final DocumentRepository documentRepository;
    private final CommentRepository commentRepository;
    private final ParticipantTypeRepository participantTypeRepository;
    private final MimeTypeRepository mimeTypeRepository;

    public ReferenceMapper(
            OnboardingRequestRepository onboardingRequestRepository,
            OnboardingStatusRepository onboardingStatusRepository,
            OnboardingTemplateRepository onboardingTemplateRepository,
            DocumentTemplateRepository documentTemplateRepository,
            DocumentRepository documentRepository,
            CommentRepository commentRepository,
            ParticipantTypeRepository participantTypeRepository,
            MimeTypeRepository mimeTypeRepository) {
        this.onboardingRequestRepository = onboardingRequestRepository;
        this.onboardingStatusRepository = onboardingStatusRepository;
        this.onboardingTemplateRepository = onboardingTemplateRepository;
        this.documentTemplateRepository = documentTemplateRepository;
        this.documentRepository = documentRepository;
        this.commentRepository = commentRepository;
        this.participantTypeRepository = participantTypeRepository;
        this.mimeTypeRepository = mimeTypeRepository;
    }

    public OnboardingStatus toOnboardingStatus(UUID id) {
        return Optional.ofNullable(id)
                .map(onboardingStatusRepository::getReferenceById)
                .orElse(null);
    }

    public OnboardingProcedureTemplate toOnboardingProcedureTemplate(UUID id) {
        return Optional.ofNullable(id)
                .map(onboardingTemplateRepository::getReferenceById)
                .orElse(null);
    }

    public OnboardingRequest toOnboardingRequest(UUID id) {
        return Optional.ofNullable(id)
                .map(onboardingRequestRepository::getReferenceById)
                .orElse(null);
    }

    public DocumentTemplate toDocumentTemplate(UUID id) {
        return Optional.ofNullable(id)
                .map(documentTemplateRepository::getReferenceById)
                .orElse(null);
    }

    public Document toDocument(UUID id) {
        return Optional.ofNullable(id).map(documentRepository::getReferenceById).orElse(null);
    }

    public Comment toComment(UUID id) {
        return Optional.ofNullable(id).map(commentRepository::getReferenceById).orElse(null);
    }

    public MimeType toMimeType(UUID id) {
        return Optional.ofNullable(id).map(mimeTypeRepository::getReferenceById).orElse(null);
    }

    public List<ParticipantType> findAllParticipantTypes() {
        return participantTypeRepository.findAll();
    }
}
