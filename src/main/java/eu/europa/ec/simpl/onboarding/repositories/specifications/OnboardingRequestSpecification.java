package eu.europa.ec.simpl.onboarding.repositories.specifications;

import eu.europa.ec.simpl.common.filters.OnboardingRequestFilter;
import eu.europa.ec.simpl.onboarding.entities.OnboardingApplicant_;
import eu.europa.ec.simpl.onboarding.entities.OnboardingRequest;
import eu.europa.ec.simpl.onboarding.entities.OnboardingRequest_;
import eu.europa.ec.simpl.onboarding.entities.OnboardingStatus_;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import java.time.Instant;
import org.springframework.data.jpa.domain.Specification;

public class OnboardingRequestSpecification implements Specification<OnboardingRequest> {
    private static final String LIKE_TEMPLATE = "%%%s%%";

    private final transient OnboardingRequestFilter filter;

    public OnboardingRequestSpecification(OnboardingRequestFilter filter) {
        this.filter = filter;
    }

    private static Specification<OnboardingRequest> applicantEmailLike(String applicantEmail) {
        return (root, query, criteriaBuilder) -> applicantEmail != null
                ? criteriaBuilder.like(
                        criteriaBuilder.lower(
                                root.get(OnboardingRequest_.onboardingApplicant).get(OnboardingApplicant_.email)),
                        LIKE_TEMPLATE.formatted(applicantEmail.toLowerCase()))
                : null;
    }

    private static Specification<OnboardingRequest> applicantEmailEqual(String applicantEmail) {
        return (root, query, criteriaBuilder) -> applicantEmail != null
                ? criteriaBuilder.equal(
                        root.get(OnboardingRequest_.onboardingApplicant).get(OnboardingApplicant_.email),
                        applicantEmail)
                : null;
    }

    private static Specification<OnboardingRequest> onboardingStatusEqual(String onboardingStatus) {
        return (root, query, criteriaBuilder) -> onboardingStatus != null
                ? criteriaBuilder.equal(
                        root.get(OnboardingRequest_.onboardingStatus).get(OnboardingStatus_.value), onboardingStatus)
                : null;
    }

    private static Specification<OnboardingRequest> updateTimestampGreaterThanOrEqualTo(Instant updateTimestampMin) {
        return (root, query, criteriaBuilder) -> updateTimestampMin != null
                ? criteriaBuilder.greaterThanOrEqualTo(root.get(OnboardingRequest_.updateTimestamp), updateTimestampMin)
                : null;
    }

    private static Specification<OnboardingRequest> updateTimestampLessThanOrEqualTo(Instant updateTimestampMax) {

        return (root, query, criteriaBuilder) -> updateTimestampMax != null
                ? criteriaBuilder.lessThanOrEqualTo(root.get(OnboardingRequest_.updateTimestamp), updateTimestampMax)
                : null;
    }

    private static Specification<OnboardingRequest> creationTimestampGreaterThanOrEqualTo(
            Instant creationTimestampMin) {
        return (root, query, criteriaBuilder) -> creationTimestampMin != null
                ? criteriaBuilder.greaterThanOrEqualTo(
                        root.get(OnboardingRequest_.creationTimestamp), creationTimestampMin)
                : null;
    }

    private static Specification<OnboardingRequest> creationTimestampLessThanOrEqualTo(Instant creationTimestampMax) {

        return (root, query, criteriaBuilder) -> creationTimestampMax != null
                ? criteriaBuilder.lessThanOrEqualTo(
                        root.get(OnboardingRequest_.creationTimestamp), creationTimestampMax)
                : null;
    }

    private static Specification<OnboardingRequest> lastStatusUpdateTimestampGreaterThanOrEqualTo(
            Instant lastStatusUpdateTimestampMin) {
        return (root, query, criteriaBuilder) -> lastStatusUpdateTimestampMin != null
                ? criteriaBuilder.greaterThanOrEqualTo(
                        root.get(OnboardingRequest_.lastStatusUpdateTimestamp), lastStatusUpdateTimestampMin)
                : null;
    }

    private static Specification<OnboardingRequest> lastStatusUpdateTimestampTimestampLessThanOrEqualTo(
            Instant lastStatusUpdateTimestampMax) {

        return (root, query, criteriaBuilder) -> lastStatusUpdateTimestampMax != null
                ? criteriaBuilder.lessThanOrEqualTo(
                        root.get(OnboardingRequest_.lastStatusUpdateTimestamp), lastStatusUpdateTimestampMax)
                : null;
    }

    @Override
    public Predicate toPredicate(
            Root<OnboardingRequest> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        return Specification.allOf(
                        applicantEmailLike(filter.getEmail()),
                        applicantEmailEqual(filter.getEmail()),
                        onboardingStatusEqual(filter.getStatus()),
                        updateTimestampGreaterThanOrEqualTo(filter.getUpdateTimestampFrom()),
                        updateTimestampLessThanOrEqualTo(filter.getUpdateTimestampTo()),
                        creationTimestampGreaterThanOrEqualTo(filter.getCreationTimestampFrom()),
                        creationTimestampLessThanOrEqualTo(filter.getCreationTimestampTo()),
                        lastStatusUpdateTimestampGreaterThanOrEqualTo(filter.getLastStatusUpdateTimestampFrom()),
                        lastStatusUpdateTimestampTimestampLessThanOrEqualTo(filter.getLastStatusUpdateTimestampTo()))
                .toPredicate(root, query, criteriaBuilder);
    }
}
