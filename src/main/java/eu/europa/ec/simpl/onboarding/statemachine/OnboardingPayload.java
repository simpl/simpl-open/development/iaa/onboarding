package eu.europa.ec.simpl.onboarding.statemachine;

import eu.europa.ec.simpl.onboarding.entities.OnboardingRequest;
import java.util.UUID;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OnboardingPayload<T> {

    private final OnboardingRequest onboardingRequest;
    private final T actionPayload;
    private UUID initiatorUserId;
    private String initiatorService;

    public OnboardingPayload(OnboardingRequest onboardingRequest, T actionPayload) {
        this.onboardingRequest = onboardingRequest;
        this.actionPayload = actionPayload;
    }

    public OnboardingPayload(OnboardingRequest onboardingRequest) {
        this(onboardingRequest, null);
    }
}
