package eu.europa.ec.simpl.onboarding.exceptions;

import eu.europa.ec.simpl.common.exceptions.StatusException;
import org.springframework.http.HttpStatus;

public class OperationNotPermittedException extends StatusException {

    public OperationNotPermittedException(Exception cause) {
        super(HttpStatus.BAD_REQUEST, "Operation not permitted", cause);
    }

    public OperationNotPermittedException() {
        super(HttpStatus.BAD_REQUEST, "Operation not permitted");
    }
}
