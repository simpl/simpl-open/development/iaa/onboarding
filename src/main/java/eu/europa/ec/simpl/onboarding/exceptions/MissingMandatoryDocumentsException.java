package eu.europa.ec.simpl.onboarding.exceptions;

import eu.europa.ec.simpl.common.exceptions.StatusException;
import java.util.List;
import java.util.UUID;
import org.springframework.http.HttpStatus;

public class MissingMandatoryDocumentsException extends StatusException {
    public MissingMandatoryDocumentsException(List<UUID> documentIds) {
        super(HttpStatus.BAD_REQUEST, "Missing mandatory document(s)");
        // TODO Pass missing documents via state machine context
        //                        .formatted(documentIds.stream().map(UUID::toString).collect(Collectors.joining(",
        // "))));
    }
}
