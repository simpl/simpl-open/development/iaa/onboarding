package eu.europa.ec.simpl.onboarding.mappers;

import eu.europa.ec.simpl.common.exceptions.RuntimeWrapperException;
import java.io.IOException;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.Base64;
import java.util.Optional;
import javax.sql.rowset.serial.SerialBlob;
import lombok.Generated;
import org.mapstruct.AnnotateWith;
import org.mapstruct.Mapper;

@Mapper
@AnnotateWith(Generated.class)
public interface BlobMapper {

    default Blob fromBytes(byte[] bytes) {

        if (bytes == null) {
            return null;
        }

        try {
            return new SerialBlob(bytes);
        } catch (SQLException e) {
            throw new RuntimeWrapperException(e);
        }
    }

    default Blob fromBase64String(String base64) {
        var decoder = Base64.getDecoder();
        return Optional.ofNullable(base64)
                .map(decoder::decode)
                .map(this::fromBytes)
                .orElse(null);
    }

    default byte[] toBytes(Blob blob) {

        if (blob == null) {
            return null;
        }

        try (var stream = blob.getBinaryStream()) {
            return stream.readAllBytes();
        } catch (IOException | SQLException e) {
            throw new RuntimeWrapperException(e);
        }
    }

    default String toBase64String(Blob blob) {
        var encoder = Base64.getEncoder();
        return Optional.ofNullable(blob)
                .map(this::toBytes)
                .map(encoder::encodeToString)
                .orElse(null);
    }
}
