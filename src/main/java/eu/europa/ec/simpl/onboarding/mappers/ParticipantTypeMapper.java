package eu.europa.ec.simpl.onboarding.mappers;

import eu.europa.ec.simpl.common.model.dto.onboarding.ParticipantTypeDTO;
import eu.europa.ec.simpl.onboarding.entities.ParticipantType;
import lombok.Generated;
import org.mapstruct.AnnotateWith;
import org.mapstruct.Mapper;

@Mapper
@AnnotateWith(Generated.class)
public interface ParticipantTypeMapper {

    ParticipantTypeDTO toDto(ParticipantType participantType);
}
