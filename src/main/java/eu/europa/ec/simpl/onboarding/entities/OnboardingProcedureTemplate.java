package eu.europa.ec.simpl.onboarding.entities;

import eu.europa.ec.simpl.common.entity.annotations.UUIDv7Generator;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import java.time.Instant;
import java.util.Set;
import java.util.UUID;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@Accessors(chain = true)
@ToString
@Getter
@Setter
@Entity
@Table(name = "onboarding_procedure_template")
@NoArgsConstructor
public class OnboardingProcedureTemplate {
    @Id
    @UUIDv7Generator
    private UUID id;

    @ToString.Exclude
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "participant_type_id", nullable = false)
    private ParticipantType participantType;

    @Column(name = "description", nullable = false)
    String description;

    @CreationTimestamp
    @Column(name = "creation_timestamp", nullable = false)
    private Instant creationTimestamp;

    @UpdateTimestamp
    @Column(name = "update_timestamp")
    private Instant updateTimestamp;

    @Column(name = "expiration_timeframe", nullable = false)
    private long expirationTimeframe;

    @ToString.Exclude
    @ManyToMany()
    @JoinTable(
            name = "onboarding_procedure_template_document_template",
            joinColumns = @JoinColumn(name = "onboarding_procedure_template_id"),
            inverseJoinColumns = @JoinColumn(name = "document_template_id"))
    private Set<DocumentTemplate> documentTemplates;
}
