package eu.europa.ec.simpl.onboarding.statemachine;

import eu.europa.ec.simpl.common.model.dto.onboarding.OnboardingStatusValue;
import eu.europa.ec.simpl.onboarding.entities.OnboardingRequest;
import eu.europa.ec.simpl.onboarding.entities.OnboardingStatus;
import eu.europa.ec.simpl.onboarding.mappers.ReferenceMapper;
import eu.europa.ec.simpl.onboarding.services.OnboardingStatusService;
import org.springframework.stereotype.Component;

@Component
public class StatusChangeHandler {

    private final ReferenceMapper referenceMapper;
    private final OnboardingStateMapper onboardingStateMapper;
    private final OnboardingStatusService onboardingStatusService;

    public StatusChangeHandler(
            ReferenceMapper referenceMapper,
            OnboardingStateMapper onboardingStateMapper,
            OnboardingStatusService onboardingStatusService) {
        this.referenceMapper = referenceMapper;
        this.onboardingStateMapper = onboardingStateMapper;
        this.onboardingStatusService = onboardingStatusService;
    }

    void changeStatus(OnboardingRequest onboardingRequest, OnboardingState target) {
        var targetStatus = onboardingStateMapper.toOnboardingStatus(target);
        var onboardingStatus = getOnboardingStatusReference(targetStatus);
        onboardingRequest.setOnboardingStatus(onboardingStatus);
    }

    private OnboardingStatus getOnboardingStatusReference(OnboardingStatusValue status) {
        return referenceMapper.toOnboardingStatus(
                onboardingStatusService.findOnboardingStatusByStatus(status).getId());
    }
}
