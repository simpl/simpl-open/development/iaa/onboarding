package eu.europa.ec.simpl.onboarding.liquibase.migration_2024_11_25.pojo;

import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OnboardingApplicant {

    private UUID id;
    private String username;
    private String firstName;
    private String lastName;
    private String email;
}
