package eu.europa.ec.simpl.onboarding.repositories;

import eu.europa.ec.simpl.onboarding.entities.DocumentTemplate;
import eu.europa.ec.simpl.onboarding.exceptions.DocumentTemplateNotFoundException;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DocumentTemplateRepository extends JpaRepository<DocumentTemplate, UUID> {
    default DocumentTemplate findByIdOrThrow(UUID id) {
        return findById(id).orElseThrow(() -> new DocumentTemplateNotFoundException(id));
    }
}
