package eu.europa.ec.simpl.onboarding.mappers;

import eu.europa.ec.simpl.api.onboarding.v1.model.ParticipantTypeDTO;
import java.util.List;
import java.util.Set;
import lombok.Generated;
import org.mapstruct.AnnotateWith;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(unmappedSourcePolicy = ReportingPolicy.ERROR, unmappedTargetPolicy = ReportingPolicy.ERROR)
@AnnotateWith(Generated.class)
public interface ParticipantTypeMapperV1 {
    List<ParticipantTypeDTO> toV1(
            Set<eu.europa.ec.simpl.common.model.dto.onboarding.ParticipantTypeDTO> participantTypes);
}
