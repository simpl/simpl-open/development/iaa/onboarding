package eu.europa.ec.simpl.onboarding.statemachine;

import static eu.europa.ec.simpl.onboarding.statemachine.OnboardingState.APPROVED;
import static eu.europa.ec.simpl.onboarding.statemachine.OnboardingState.IN_REVIEW;

import eu.europa.ec.simpl.common.model.dto.onboarding.ApproveDTO;
import eu.europa.ec.simpl.onboarding.statemachine.common.BasicEvent;

public interface Approve extends BasicEvent<OnboardingState, OnboardingPayload<ApproveDTO>> {

    @Override
    default OnboardingState getSource() {
        return IN_REVIEW;
    }

    @Override
    default OnboardingState getTarget() {
        return APPROVED;
    }
}
