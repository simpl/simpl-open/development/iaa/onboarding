package eu.europa.ec.simpl.onboarding.repositories;

import eu.europa.ec.simpl.onboarding.entities.DocumentTemplate_;
import eu.europa.ec.simpl.onboarding.entities.OnboardingProcedureTemplate;
import eu.europa.ec.simpl.onboarding.entities.OnboardingProcedureTemplate_;
import eu.europa.ec.simpl.onboarding.entities.ParticipantType;
import eu.europa.ec.simpl.onboarding.exceptions.OnboardingTemplateNotFoundException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OnboardingTemplateRepository extends JpaRepository<OnboardingProcedureTemplate, UUID> {

    @EntityGraph(
            attributePaths = {
                OnboardingProcedureTemplate_.DOCUMENT_TEMPLATES + "." + DocumentTemplate_.MIME_TYPE,
                OnboardingProcedureTemplate_.PARTICIPANT_TYPE
            })
    Optional<OnboardingProcedureTemplate> findByParticipantTypeId(UUID id);

    default OnboardingProcedureTemplate findByParticipantTypeIdOrThrow(UUID id) {
        return findByParticipantTypeId(id).orElseThrow(() -> new OnboardingTemplateNotFoundException(id));
    }

    @EntityGraph(
            attributePaths = {
                OnboardingProcedureTemplate_.DOCUMENT_TEMPLATES + "." + DocumentTemplate_.MIME_TYPE,
                OnboardingProcedureTemplate_.PARTICIPANT_TYPE
            })
    List<OnboardingProcedureTemplate> findAllByParticipantTypeIn(List<ParticipantType> participantTypes);
}
