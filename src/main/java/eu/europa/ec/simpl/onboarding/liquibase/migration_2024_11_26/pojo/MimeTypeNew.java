package eu.europa.ec.simpl.onboarding.liquibase.migration_2024_11_26.pojo;

import java.util.UUID;

public record MimeTypeNew(UUID id, String value) {}
