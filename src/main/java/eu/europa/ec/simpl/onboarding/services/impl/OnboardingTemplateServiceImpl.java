package eu.europa.ec.simpl.onboarding.services.impl;

import eu.europa.ec.simpl.common.model.dto.onboarding.DocumentTemplateDTO;
import eu.europa.ec.simpl.common.model.dto.onboarding.OnboardingTemplateDTO;
import eu.europa.ec.simpl.onboarding.entities.DocumentTemplate;
import eu.europa.ec.simpl.onboarding.entities.OnboardingProcedureTemplate;
import eu.europa.ec.simpl.onboarding.entities.ParticipantType;
import eu.europa.ec.simpl.onboarding.mappers.DocumentTemplateMapper;
import eu.europa.ec.simpl.onboarding.mappers.OnboardingTemplateMapper;
import eu.europa.ec.simpl.onboarding.mappers.ReferenceMapper;
import eu.europa.ec.simpl.onboarding.repositories.DocumentTemplateRepository;
import eu.europa.ec.simpl.onboarding.repositories.OnboardingTemplateRepository;
import eu.europa.ec.simpl.onboarding.services.DocumentTemplateService;
import eu.europa.ec.simpl.onboarding.services.OnboardingTemplateService;
import eu.europa.ec.simpl.onboarding.services.ParticipantTypeService;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

@Log4j2
@Service
@Validated
public class OnboardingTemplateServiceImpl implements OnboardingTemplateService {

    private final OnboardingTemplateRepository templateRepository;
    private final ParticipantTypeService participantTypeService;
    private final DocumentTemplateService documentTemplateService;
    private final DocumentTemplateRepository documentTemplateRepository;
    private final DocumentTemplateMapper documentTemplateMapper;
    private final OnboardingTemplateRepository onboardingTemplateRepository;
    private final OnboardingTemplateMapper onboardingTemplateMapper;
    private final ReferenceMapper referenceMapper;

    public OnboardingTemplateServiceImpl(
            OnboardingTemplateRepository templateRepository,
            ParticipantTypeService participantTypeService,
            DocumentTemplateService documentTemplateService,
            DocumentTemplateRepository documentTemplateRepository,
            DocumentTemplateMapper documentTemplateMapper,
            OnboardingTemplateRepository onboardingTemplateRepository,
            OnboardingTemplateMapper onboardingTemplateMapper,
            ReferenceMapper referenceMapper) {
        this.templateRepository = templateRepository;
        this.participantTypeService = participantTypeService;
        this.documentTemplateService = documentTemplateService;
        this.documentTemplateRepository = documentTemplateRepository;
        this.documentTemplateMapper = documentTemplateMapper;
        this.onboardingTemplateRepository = onboardingTemplateRepository;
        this.onboardingTemplateMapper = onboardingTemplateMapper;
        this.referenceMapper = referenceMapper;
    }

    @Override
    @Transactional
    public OnboardingTemplateDTO setOnboardingTemplate(UUID participantTypeId, OnboardingTemplateDTO dto) {
        var participantType = participantTypeService.findParticipantTypeById(participantTypeId);

        var onboardingTemplate = templateRepository
                .findByParticipantTypeId(participantType.getId())
                .orElse(new OnboardingProcedureTemplate());

        var documentTemplates = dto.getDocumentTemplates().stream()
                .map(documentTemplateService::createDocumentTemplate)
                .map(documentTemplateRepository::getReferenceById)
                .collect(Collectors.toSet());

        onboardingTemplateMapper.updateEntity(onboardingTemplate, dto, documentTemplates, participantType);
        return onboardingTemplateMapper.toDTO(onboardingTemplateRepository.save(onboardingTemplate));
    }

    @Override
    public OnboardingTemplateDTO findOnboardingTemplate(UUID participantTypeId) {
        var template = onboardingTemplateRepository.findByParticipantTypeIdOrThrow(participantTypeId);
        return onboardingTemplateMapper.toDTO(template);
    }

    @Override
    public List<OnboardingTemplateDTO> getOnboardingTemplates() {
        var participantTypes = referenceMapper.findAllParticipantTypes();
        return findAllOnboardingProcedureTemplate(participantTypes).stream()
                .map(onboardingTemplateMapper::toDTO)
                .toList();
    }

    @Override
    public void deleteOnboardingTemplate(UUID participantTypeId) {
        var template = templateRepository.findByParticipantTypeIdOrThrow(participantTypeId);
        templateRepository.delete(template);
    }

    @Transactional
    @Override
    public void updateOnboardingTemplate(UUID participantTypeId, List<UUID> uuids) {
        var template = templateRepository.findByParticipantTypeIdOrThrow(participantTypeId);
        var templatesSet = getNewDocumentTemplatesSet(uuids, template);
        template.setDocumentTemplates(templatesSet);
    }

    private Set<DocumentTemplate> getNewDocumentTemplatesSet(List<UUID> uuids, OnboardingProcedureTemplate template) {
        var remainingDocumentTemplates = template.getDocumentTemplates().stream()
                .dropWhile(doc -> uuids.contains(doc.getId()))
                .toList();
        var newDocumentTemplates = uuids.stream()
                .distinct()
                .filter(uuid -> !template.getDocumentTemplates().stream()
                        .map(DocumentTemplate::getId)
                        .toList()
                        .contains(uuid))
                .map(referenceMapper::toDocumentTemplate)
                .map(documentTemplateMapper::toDto)
                .toList();
        return Stream.of(
                        remainingDocumentTemplates,
                        newDocumentTemplates.stream()
                                .map(DocumentTemplateDTO::getId)
                                .map(documentTemplateRepository::getReferenceById)
                                .toList())
                .flatMap(Collection::stream)
                .collect(Collectors.toSet());
    }

    private List<OnboardingProcedureTemplate> findAllOnboardingProcedureTemplate(
            List<ParticipantType> participantTypes) {
        return templateRepository.findAllByParticipantTypeIn(participantTypes);
    }
}
