package eu.europa.ec.simpl.onboarding.services;

import eu.europa.ec.simpl.common.model.dto.onboarding.ParticipantTypeDTO;
import java.util.Set;
import java.util.UUID;

public interface ParticipantTypeService {

    ParticipantTypeDTO findParticipantTypeById(UUID participantTypeId);

    Set<ParticipantTypeDTO> findAllParticipantTypes();
}
