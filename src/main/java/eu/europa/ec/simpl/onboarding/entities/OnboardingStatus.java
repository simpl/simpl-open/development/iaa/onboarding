package eu.europa.ec.simpl.onboarding.entities;

import eu.europa.ec.simpl.common.entity.annotations.UUIDv7Generator;
import eu.europa.ec.simpl.common.model.dto.onboarding.OnboardingStatusValue;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import java.util.UUID;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

@Accessors(chain = true)
@Getter
@Setter
@Entity
@Table(name = "onboarding_status")
@NoArgsConstructor
public class OnboardingStatus {

    @Id
    @UUIDv7Generator
    private UUID id;

    @Enumerated(EnumType.STRING)
    @Column(name = "value", nullable = false)
    private OnboardingStatusValue value;

    @Column(name = "label")
    private String label;
}
