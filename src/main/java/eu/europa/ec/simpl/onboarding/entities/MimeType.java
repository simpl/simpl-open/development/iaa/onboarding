package eu.europa.ec.simpl.onboarding.entities;

import eu.europa.ec.simpl.common.entity.annotations.UUIDv7Generator;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;
import java.util.UUID;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Accessors(chain = true)
@ToString
@Getter
@Setter
@Entity
@Table(name = "mime_type")
@NoArgsConstructor
public class MimeType {
    @Id
    @UUIDv7Generator
    private UUID id;

    @Column(name = "value", unique = true)
    private String value;

    @NotNull @Column(name = "description", nullable = false)
    private String description;
}
