package eu.europa.ec.simpl.onboarding.statemachine;

import eu.europa.ec.simpl.common.model.dto.onboarding.DocumentDTO;
import eu.europa.ec.simpl.common.security.JwtService;
import eu.europa.ec.simpl.onboarding.entities.Document;
import eu.europa.ec.simpl.onboarding.mappers.ReferenceMapper;
import eu.europa.ec.simpl.onboarding.services.DocumentService;
import java.util.UUID;
import org.springframework.stereotype.Component;

@Component
public class RequestAdditionalDocumentHandler implements RequestAdditionalDocument, NotaryEvent<DocumentDTO> {

    private final JwtService jwtService;
    private final DocumentService documentService;
    private final ReferenceMapper referenceMapper;

    public RequestAdditionalDocumentHandler(
            JwtService jwtService, DocumentService documentService, ReferenceMapper referenceMapper) {
        this.jwtService = jwtService;
        this.documentService = documentService;
        this.referenceMapper = referenceMapper;
    }

    @Override
    public void handle(OnboardingState source, OnboardingState target, OnboardingPayload<DocumentDTO> payload) {
        var onboardingRequest = payload.getOnboardingRequest();
        // TODO Should check if template exists?
        var document = createDocument(payload.getActionPayload(), onboardingRequest.getId());
        onboardingRequest.addDocument(document);
    }

    @Override
    public boolean guard(OnboardingState source, OnboardingState target, OnboardingPayload<DocumentDTO> payload) {
        return NotaryEvent.super.guard(source, target, payload);
    }

    private Document createDocument(DocumentDTO payload, UUID onboardingRequestId) {
        return referenceMapper.toDocument(
                documentService.createDocument(payload, onboardingRequestId).getId());
    }

    @Override
    public JwtService getJwtService() {
        return jwtService;
    }
}
