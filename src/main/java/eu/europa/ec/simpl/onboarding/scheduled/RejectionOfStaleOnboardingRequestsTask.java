package eu.europa.ec.simpl.onboarding.scheduled;

import eu.europa.ec.simpl.onboarding.repositories.OnboardingRequestRepository;
import eu.europa.ec.simpl.onboarding.statemachine.TimeoutEvent;
import eu.europa.ec.simpl.onboarding.statemachine.executor.OnboardingStateMachine;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
public class RejectionOfStaleOnboardingRequestsTask {

    private final OnboardingRequestRepository onboardingRequestRepository;
    private final OnboardingStateMachine onboardingStateMachine;

    public RejectionOfStaleOnboardingRequestsTask(
            OnboardingRequestRepository onboardingRequestRepository, OnboardingStateMachine onboardingStateMachine) {
        this.onboardingRequestRepository = onboardingRequestRepository;
        this.onboardingStateMachine = onboardingStateMachine;
    }

    @Transactional
    public void rejectionOfStaleOnboardingRequestsTask() {
        log.info("Start rejection of stale onboarding requests");
        onboardingRequestRepository.findInProgressWithExpiredTimeframe().forEach(request -> {
            log.info(
                    "Expired onboarding request id: {}, Last status update timestamp: {}, Expiration timeframe {}",
                    request.getId(),
                    request.getLastStatusUpdateTimestamp(),
                    request.getExpirationTimeframe());
            onboardingStateMachine.execute(
                    request.getId(), TimeoutEvent.class, getClass().getName());
        });
        log.info("End rejection of stale onboarding requests");
    }
}
