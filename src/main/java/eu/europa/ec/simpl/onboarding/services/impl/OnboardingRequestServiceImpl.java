package eu.europa.ec.simpl.onboarding.services.impl;

import eu.europa.ec.simpl.common.constants.Roles;
import eu.europa.ec.simpl.common.filters.OnboardingRequestFilter;
import eu.europa.ec.simpl.common.model.dto.onboarding.ApproveDTO;
import eu.europa.ec.simpl.common.model.dto.onboarding.CommentDTO;
import eu.europa.ec.simpl.common.model.dto.onboarding.DocumentDTO;
import eu.europa.ec.simpl.common.model.dto.onboarding.OnboardingRequestDTO;
import eu.europa.ec.simpl.common.model.dto.onboarding.OnboardingStatusDTO;
import eu.europa.ec.simpl.common.model.dto.onboarding.OnboardingStatusValue;
import eu.europa.ec.simpl.common.model.dto.onboarding.OnboardingTemplateDTO;
import eu.europa.ec.simpl.common.model.dto.onboarding.RejectDTO;
import eu.europa.ec.simpl.common.responses.PageResponse;
import eu.europa.ec.simpl.common.security.JwtService;
import eu.europa.ec.simpl.onboarding.entities.Document;
import eu.europa.ec.simpl.onboarding.entities.OnboardingApplicant;
import eu.europa.ec.simpl.onboarding.entities.OnboardingRequest;
import eu.europa.ec.simpl.onboarding.exceptions.DocumentNotFoundException;
import eu.europa.ec.simpl.onboarding.mappers.OnboardingRequestMapper;
import eu.europa.ec.simpl.onboarding.mappers.ReferenceMapper;
import eu.europa.ec.simpl.onboarding.repositories.CommentRepository;
import eu.europa.ec.simpl.onboarding.repositories.OnboardingRequestRepository;
import eu.europa.ec.simpl.onboarding.repositories.specifications.OnboardingRequestSpecification;
import eu.europa.ec.simpl.onboarding.services.DocumentService;
import eu.europa.ec.simpl.onboarding.services.OnboardingApplicantService;
import eu.europa.ec.simpl.onboarding.services.OnboardingRequestService;
import eu.europa.ec.simpl.onboarding.services.OnboardingStatusService;
import eu.europa.ec.simpl.onboarding.services.OnboardingTemplateService;
import eu.europa.ec.simpl.onboarding.statemachine.AddCommentEvent;
import eu.europa.ec.simpl.onboarding.statemachine.Approve;
import eu.europa.ec.simpl.onboarding.statemachine.RejectEvent;
import eu.europa.ec.simpl.onboarding.statemachine.RequestAdditionalDocument;
import eu.europa.ec.simpl.onboarding.statemachine.RequestRevision;
import eu.europa.ec.simpl.onboarding.statemachine.SubmitEvent;
import eu.europa.ec.simpl.onboarding.statemachine.UploadDocument;
import eu.europa.ec.simpl.onboarding.statemachine.executor.OnboardingStateMachine;
import eu.europa.ec.simpl.onboarding.validators.CreateOnboardingRequestOperation;
import jakarta.validation.Valid;
import jakarta.validation.constraints.PositiveOrZero;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

@Log4j2
@Service
@Validated
@Transactional(readOnly = true)
public class OnboardingRequestServiceImpl implements OnboardingRequestService {

    private final JwtService jwtService;
    private final OnboardingTemplateService onboardingTemplateService;
    private final OnboardingStatusService onboardingStatusService;
    private final DocumentService documentService;
    private final OnboardingRequestRepository onboardingRequestRepository;
    private final OnboardingRequestMapper onboardingRequestMapper;
    private final ReferenceMapper referenceMapper;
    private final OnboardingApplicantService onboardingApplicantService;
    private final CommentRepository commentRepository;
    private final OnboardingStateMachine stateMachine;

    public OnboardingRequestServiceImpl(
            JwtService jwtService,
            OnboardingTemplateService onboardingTemplateService,
            OnboardingStatusService onboardingStatusService,
            DocumentService documentService,
            OnboardingRequestRepository onboardingRequestRepository,
            OnboardingRequestMapper onboardingRequestMapper,
            ReferenceMapper referenceMapper,
            OnboardingApplicantService onboardingApplicantService,
            CommentRepository commentRepository,
            OnboardingStateMachine stateMachine) {
        this.jwtService = jwtService;
        this.onboardingTemplateService = onboardingTemplateService;
        this.onboardingStatusService = onboardingStatusService;
        this.documentService = documentService;
        this.onboardingRequestRepository = onboardingRequestRepository;
        this.onboardingRequestMapper = onboardingRequestMapper;
        this.referenceMapper = referenceMapper;
        this.onboardingApplicantService = onboardingApplicantService;
        this.commentRepository = commentRepository;
        this.stateMachine = stateMachine;
    }

    @Transactional
    @Override
    @Validated(CreateOnboardingRequestOperation.class)
    public OnboardingRequestDTO create(@Valid OnboardingRequestDTO onboardingRequestDTO) {
        var onboardingTemplate = onboardingTemplateService.findOnboardingTemplate(
                onboardingRequestDTO.getParticipantType().getId());

        var entity = onboardingRequestMapper.create(
                createKeycloakUser(onboardingRequestDTO),
                onboardingTemplate,
                getInitialOnboardingStatus(),
                onboardingRequestDTO.getOrganization());

        var savedEntity = onboardingRequestRepository.saveAndFlush(entity);
        var documentReferences = createDocumentsForEachDocumentTemplate(savedEntity, onboardingTemplate);

        savedEntity.setDocuments(documentReferences);
        return onboardingRequestMapper.toOnboardingRequestDto(savedEntity);
    }

    private List<Document> createDocumentsForEachDocumentTemplate(
            OnboardingRequest entity, OnboardingTemplateDTO template) {
        var documents = template.getDocumentTemplates().stream()
                .map(documentTemplate -> new DocumentDTO().setDocumentTemplate(documentTemplate))
                .toList();

        return documentService.createDocuments(documents, entity.getId()).stream()
                .map(DocumentDTO::getId)
                .map(referenceMapper::toDocument)
                .toList();
    }

    private OnboardingStatusDTO getInitialOnboardingStatus() {
        var initialStatusValue = OnboardingStatusValue.IN_PROGRESS;
        return onboardingStatusService.getOnboardingStatus().stream()
                .filter(o -> o.getValue().equals(initialStatusValue))
                .findFirst()
                .orElseThrow(() -> new IllegalStateException(
                        "No initial onboarding status found, expected value: %s".formatted(initialStatusValue)));
    }

    private OnboardingApplicant createKeycloakUser(OnboardingRequestDTO onboardingRequestDTO) {
        return onboardingApplicantService.create(
                onboardingRequestDTO.getApplicant(),
                onboardingRequestDTO.getParticipantType(),
                onboardingRequestDTO.getOrganization());
    }

    @Override
    @Transactional
    public OnboardingRequestDTO submit(UUID onboardingRequestId, String initiator) {
        return stateMachine.execute(onboardingRequestId, SubmitEvent.class, initiator);
    }

    @Override
    @Transactional
    public OnboardingRequestDTO approve(UUID onboardingRequestId, ApproveDTO approveDTO, String initiator) {
        return stateMachine.execute(onboardingRequestId, Approve.class, approveDTO, initiator);
    }

    @Override
    @Transactional
    public OnboardingRequestDTO reject(UUID onboardingRequestId, RejectDTO rejectDTO, String initiator) {
        return stateMachine.execute(onboardingRequestId, RejectEvent.class, rejectDTO, initiator);
    }

    @Override
    @Transactional
    public OnboardingRequestDTO uploadDocument(UUID onboardingRequestId, DocumentDTO documentDTO, String initiator) {
        return stateMachine.execute(onboardingRequestId, UploadDocument.class, documentDTO, initiator);
    }

    @Override
    @Transactional
    public OnboardingRequestDTO requestAdditionalDocument(
            UUID onboardingRequestId, DocumentDTO documentDTO, String initiator) {
        return stateMachine.execute(onboardingRequestId, RequestAdditionalDocument.class, documentDTO, initiator);
    }

    @Override
    @Transactional
    public OnboardingRequestDTO requestRevision(UUID onboardingRequestId, String initiator) {
        return stateMachine.execute(onboardingRequestId, RequestRevision.class, initiator);
    }

    @Override
    @Transactional
    public OnboardingRequestDTO addComment(UUID onboardingRequestId, CommentDTO commentDTO, String initiator) {
        return stateMachine.execute(onboardingRequestId, AddCommentEvent.class, commentDTO, initiator);
    }

    @Override
    public DocumentDTO getDocument(UUID onboardingRequestId, UUID documentId) {
        onboardingRequestRepository.findByIdOrThrow(onboardingRequestId);
        return documentService.getDocument(documentId);
    }

    @Override
    @Transactional
    public void setExpirationTimeframe(UUID id, @PositiveOrZero Long expirationTimeframe) {
        var onboardingRequest = onboardingRequestRepository.findByIdOrThrow(id);
        onboardingRequest.setExpirationTimeframe(expirationTimeframe);
        onboardingRequestRepository.save(onboardingRequest);
    }

    @Override
    @Transactional
    public void deleteDocument(UUID onboardingRequestId, UUID documentId) {
        var onboardingRequest = getOnboardingRequest(onboardingRequestId);
        if (onboardingRequest.getDocuments().stream()
                .noneMatch(document -> document.getId().equals(documentId))) {
            throw new DocumentNotFoundException(
                    "Document %s not found in onboarding request %s".formatted(documentId, onboardingRequest));
        }
        documentService.clearDocumentContent(documentId);
    }

    @Override
    public PageResponse<OnboardingRequestDTO> search(OnboardingRequestFilter filter, Pageable pageable) {
        if (!jwtService.hasRole(Roles.NOTARY)) {
            filter.setEmail(jwtService.getEmail());
        }

        var onboardingRequests =
                onboardingRequestRepository.findAll(new OnboardingRequestSpecification(filter), pageable);

        var onboardingRequestIdToComments = commentRepository
                .findByOnboardingRequestIdIn(onboardingRequests.stream()
                        .map(OnboardingRequest::getId)
                        .collect(Collectors.toSet()))
                .stream()
                .collect(Collectors.groupingBy(
                        comment -> comment.getOnboardingRequest().getId()));

        onboardingRequests.forEach(onboardingRequest ->
                onboardingRequest.setComments(onboardingRequestIdToComments.get(onboardingRequest.getId())));

        var page = onboardingRequests.map(onboardingRequestMapper::toOnboardingRequestDto);

        return new PageResponse<>(page);
    }

    @Override
    public OnboardingRequestDTO getOnboardingRequest(UUID onboardingRequestId) {
        var request = onboardingRequestRepository.findByIdOrThrow(onboardingRequestId);
        return onboardingRequestMapper.toOnboardingRequestDto(request);
    }
}
