package eu.europa.ec.simpl.onboarding.entities;

import eu.europa.ec.simpl.common.entity.annotations.UUIDv7Generator;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@Getter
@Setter
@Entity
@Table(name = "onboarding_request")
public class OnboardingRequest {
    @Id
    @UUIDv7Generator
    private UUID id;

    @ToString.Exclude
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "onboarding_procedure_template_id", nullable = false)
    private OnboardingProcedureTemplate onboardingProcedureTemplate;

    @ToString.Exclude
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "onboarding_status_id", nullable = false)
    private OnboardingStatus onboardingStatus;

    @ToString.Exclude
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "onboarding_applicant_id", nullable = false)
    private OnboardingApplicant onboardingApplicant;

    @CreationTimestamp
    @Column(name = "creation_timestamp", nullable = false)
    private Instant creationTimestamp;

    @UpdateTimestamp
    @Column(name = "update_timestamp")
    private Instant updateTimestamp;

    @Column(name = "expiration_timeframe", nullable = false)
    private long expirationTimeframe;

    @Column(name = "last_status_update_timestamp")
    private Instant lastStatusUpdateTimestamp;

    @ToString.Exclude
    @OneToMany(mappedBy = "onboardingRequest")
    private List<Comment> comments = new ArrayList<>();

    @ToString.Exclude
    @OneToMany(mappedBy = "onboardingRequest")
    private List<Document> documents = new ArrayList<>();

    @ToString.Exclude
    @NotNull @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "participant_type_id")
    private ParticipantType participantType;

    @Column(name = "organization", nullable = false)
    private String organization;

    @Column(name = "participant_id")
    private UUID participantId;

    @Column(name = "rejection_cause")
    private String rejectionCause;

    public OnboardingRequest addComment(Comment comment) {
        comments.add(comment);
        comment.setOnboardingRequest(this);
        return this;
    }

    public OnboardingRequest addDocument(Document document) {
        documents.add(document);
        document.setOnboardingRequest(this);
        return this;
    }

    public Optional<Document> getDocumentById(UUID documentId) {
        return documents.stream()
                .filter(document -> document.getId().equals(documentId))
                .findFirst();
    }
}
