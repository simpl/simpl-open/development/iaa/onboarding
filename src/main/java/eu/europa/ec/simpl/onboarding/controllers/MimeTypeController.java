package eu.europa.ec.simpl.onboarding.controllers;

import eu.europa.ec.simpl.common.model.dto.onboarding.MimeTypeDTO;
import eu.europa.ec.simpl.onboarding.services.MimeTypeService;
import eu.europa.ec.simpl.onboarding.utils.MimeTypeUtil;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import java.util.List;
import java.util.UUID;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("mime-type")
public class MimeTypeController {

    private final MimeTypeService mimeTypeService;

    public MimeTypeController(MimeTypeService mimeTypeService) {
        this.mimeTypeService = mimeTypeService;
    }

    @Operation(
            summary = "Create a new MIME type",
            description = "Creates a new MIME type with the provided details.",
            requestBody =
                    @io.swagger.v3.oas.annotations.parameters.RequestBody(
                            description = "MIME type details",
                            required = true,
                            content = @Content(schema = @Schema(implementation = MimeTypeDTO.class))),
            responses = {
                @ApiResponse(
                        responseCode = "201",
                        description = "MIME type created successfully",
                        content =
                                @Content(
                                        mediaType = MediaType.APPLICATION_JSON_VALUE,
                                        schema = @Schema(implementation = UUID.class))),
                @ApiResponse(responseCode = "400", description = "Invalid input data"),
                @ApiResponse(responseCode = "401", description = "Access denied"),
                @ApiResponse(responseCode = "403", description = "Forbidden: User does not have the required role"),
                @ApiResponse(responseCode = "422", description = "Mime type not supported")
            })
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public UUID createMimeType(@RequestBody MimeTypeDTO mimeType) {
        return mimeTypeService.createMimeType(mimeType);
    }

    @Operation(
            summary = "Get all MIME types",
            description = "Retrieves a paginated list of all MIME types.",
            responses = {
                @ApiResponse(responseCode = "200", description = "Successfully retrieved list of MIME types"),
                @ApiResponse(responseCode = "401", description = "Access denied"),
                @ApiResponse(responseCode = "403", description = "Forbidden: User does not have the required role")
            })
    @GetMapping
    public Page<MimeTypeDTO> getAllMimeTypes(@PageableDefault(sort = "id") @ParameterObject Pageable pageable) {
        return mimeTypeService.search(pageable);
    }

    @Operation(
            summary = "Get all supported MIME types",
            description = "Retrieves all supported MIME types.",
            responses = {
                @ApiResponse(responseCode = "200", description = "Successfully retrieved list of MIME types"),
                @ApiResponse(responseCode = "401", description = "Access denied"),
                @ApiResponse(responseCode = "403", description = "Forbidden: User does not have the required role")
            })
    @GetMapping("supported")
    public List<String> getAllAvailableMimeTypes(@RequestParam(defaultValue = "") String search) {
        return MimeTypeUtil.getAllAvailableMimeTypes(search);
    }

    @Operation(
            summary = "Get MIME type by ID",
            description = "Retrieves a MIME type by its unique identifier.",
            parameters = {
                @Parameter(
                        name = "id",
                        description = "Unique identifier of the MIME type",
                        required = true,
                        schema = @Schema(type = "string", format = "uuid"))
            },
            responses = {
                @ApiResponse(
                        responseCode = "200",
                        description = "Successfully retrieved MIME type",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = MimeTypeDTO.class))),
                @ApiResponse(responseCode = "401", description = "Access denied"),
                @ApiResponse(responseCode = "403", description = "Forbidden: User does not have the required role"),
                @ApiResponse(responseCode = "404", description = "MIME type not found")
            })
    @GetMapping("{id}")
    public MimeTypeDTO getMimeType(@PathVariable UUID id) {
        return mimeTypeService.findById(id);
    }

    @Operation(
            summary = "Update MIME type by ID",
            description = "Updates an existing MIME type with the provided details.",
            parameters = {
                @Parameter(
                        name = "id",
                        description = "Unique identifier of the MIME type to be updated",
                        required = true,
                        schema = @Schema(type = "string", format = "uuid"))
            },
            requestBody =
                    @io.swagger.v3.oas.annotations.parameters.RequestBody(
                            description = "Updated MIME type details",
                            required = true,
                            content = @Content(schema = @Schema(implementation = MimeTypeDTO.class))),
            responses = {
                @ApiResponse(responseCode = "200", description = "MIME type updated successfully"),
                @ApiResponse(responseCode = "400", description = "Invalid input data"),
                @ApiResponse(responseCode = "401", description = "Access denied"),
                @ApiResponse(responseCode = "403", description = "Forbidden: User does not have the required role"),
                @ApiResponse(responseCode = "404", description = "MIME type not found")
            })
    @PutMapping("{id}")
    public void updateMimeType(@PathVariable UUID id, @RequestBody MimeTypeDTO mimeType) {
        mimeTypeService.updateById(id, mimeType);
    }

    @Operation(
            summary = "Delete MIME type by ID",
            description = "Deletes a MIME type by its unique identifier.",
            parameters = {
                @Parameter(
                        name = "id",
                        description = "Unique identifier of the MIME type to be deleted",
                        required = true,
                        schema = @Schema(type = "string", format = "uuid"))
            },
            responses = {
                @ApiResponse(responseCode = "204", description = "MIME type deleted successfully"),
                @ApiResponse(responseCode = "401", description = "Access denied"),
                @ApiResponse(responseCode = "403", description = "Forbidden: User does not have the required role")
            })
    @DeleteMapping("{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteMimeType(@PathVariable UUID id) {
        mimeTypeService.deleteById(id);
    }
}
