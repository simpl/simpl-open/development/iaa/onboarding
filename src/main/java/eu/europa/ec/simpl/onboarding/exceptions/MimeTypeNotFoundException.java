package eu.europa.ec.simpl.onboarding.exceptions;

import eu.europa.ec.simpl.common.exceptions.StatusException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class MimeTypeNotFoundException extends StatusException {
    public MimeTypeNotFoundException(String mType) {
        this(HttpStatus.NOT_FOUND, mType);
    }

    public MimeTypeNotFoundException(HttpStatus status, String mType) {
        super(status, "Mime type [ %s ] not found".formatted(mType));
    }
}
