package eu.europa.ec.simpl.onboarding.statemachine;

import eu.europa.ec.simpl.common.model.dto.onboarding.CommentDTO;
import eu.europa.ec.simpl.onboarding.statemachine.common.Event;
import java.util.Set;

public interface AddCommentEvent extends Event<OnboardingState, OnboardingPayload<CommentDTO>> {
    @Override
    default Set<Edge<OnboardingState>> getEdges() {
        return Set.of(
                Edge.of(OnboardingState.IN_PROGRESS, OnboardingState.IN_PROGRESS),
                Edge.of(OnboardingState.IN_REVIEW, OnboardingState.IN_REVIEW));
    }
}
