package eu.europa.ec.simpl.onboarding.repositories;

import eu.europa.ec.simpl.onboarding.entities.Document;
import eu.europa.ec.simpl.onboarding.entities.Document_;
import eu.europa.ec.simpl.onboarding.exceptions.DocumentNotFoundException;
import java.util.Optional;
import java.util.UUID;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DocumentRepository extends JpaRepository<Document, UUID> {

    @EntityGraph(attributePaths = {Document_.CONTENT, Document_.DOCUMENT_TEMPLATE})
    Optional<Document> findFullById(UUID id);

    default Document findFullByIdOrThrow(UUID id) {
        return findFullById(id).orElseThrow(() -> new DocumentNotFoundException(id));
    }

    default Document findByIdOrThrow(UUID id) {
        return findById(id).orElseThrow(() -> new DocumentNotFoundException(id));
    }
}
