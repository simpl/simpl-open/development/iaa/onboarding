package eu.europa.ec.simpl.onboarding.repositories;

import eu.europa.ec.simpl.onboarding.entities.OnboardingApplicant;
import java.util.List;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OnboardingApplicantRepository extends JpaRepository<OnboardingApplicant, UUID> {

    List<OnboardingApplicant> findByEmail(String email);
}
