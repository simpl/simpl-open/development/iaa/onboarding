package eu.europa.ec.simpl.onboarding.exceptions;

import eu.europa.ec.simpl.common.exceptions.StatusException;
import org.springframework.http.HttpStatus;

public class RejectionCauseNotProvidedException extends StatusException {
    public RejectionCauseNotProvidedException() {
        super(HttpStatus.BAD_REQUEST, "Rejection cause not provided");
    }
}
