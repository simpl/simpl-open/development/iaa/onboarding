package eu.europa.ec.simpl.onboarding.liquibase.migration_2024_11_26.pojo;

import lombok.Data;

@Data
public class MimeType {

    private Long id;
    private String value;
}
