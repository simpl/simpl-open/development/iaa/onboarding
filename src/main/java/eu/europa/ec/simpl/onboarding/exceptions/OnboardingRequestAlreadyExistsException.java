package eu.europa.ec.simpl.onboarding.exceptions;

import eu.europa.ec.simpl.common.exceptions.StatusException;
import org.springframework.http.HttpStatus;

public class OnboardingRequestAlreadyExistsException extends StatusException {
    public OnboardingRequestAlreadyExistsException(String applicantEmail) {
        super(HttpStatus.CONFLICT, "Onboarding request already exists for user %s".formatted(applicantEmail));
    }
}
