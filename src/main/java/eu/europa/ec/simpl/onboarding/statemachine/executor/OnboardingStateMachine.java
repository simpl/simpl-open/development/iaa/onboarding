package eu.europa.ec.simpl.onboarding.statemachine.executor;

import eu.europa.ec.simpl.common.model.dto.onboarding.OnboardingRequestDTO;
import eu.europa.ec.simpl.common.security.JwtService;
import eu.europa.ec.simpl.onboarding.exceptions.OperationNotPermittedException;
import eu.europa.ec.simpl.onboarding.mappers.OnboardingRequestMapper;
import eu.europa.ec.simpl.onboarding.repositories.OnboardingRequestRepository;
import eu.europa.ec.simpl.onboarding.statemachine.OnboardingPayload;
import eu.europa.ec.simpl.onboarding.statemachine.OnboardingState;
import eu.europa.ec.simpl.onboarding.statemachine.OnboardingStateMapper;
import eu.europa.ec.simpl.onboarding.statemachine.common.Event;
import eu.europa.ec.simpl.onboarding.statemachine.common.GenericStateMachine;
import eu.europa.ec.simpl.onboarding.statemachine.common.GuardFailedException;
import eu.europa.ec.simpl.onboarding.statemachine.common.IllegalTransitionException;
import eu.europa.ec.simpl.onboarding.statemachine.listener.OnboardingStateListener;
import java.util.List;
import java.util.UUID;
import org.springframework.stereotype.Component;

@Component
public class OnboardingStateMachine {

    private final List<Event<OnboardingState, OnboardingPayload<?>>> eventHandlers;
    private final OnboardingStateListener stateListener;
    private final OnboardingRequestRepository onboardingRequestRepository;
    private final OnboardingRequestMapper onboardingRequestMapper;
    private final OnboardingStateMapper onboardingStateMapper;
    private final JwtService jwtService;

    public OnboardingStateMachine(
            List<Event<OnboardingState, OnboardingPayload<?>>> eventHandlers,
            OnboardingStateListener stateListener,
            OnboardingRequestRepository onboardingRequestRepository,
            OnboardingRequestMapper onboardingRequestMapper,
            OnboardingStateMapper onboardingStateMapper,
            JwtService jwtService) {
        this.eventHandlers = eventHandlers;
        this.stateListener = stateListener;
        this.onboardingRequestRepository = onboardingRequestRepository;
        this.onboardingRequestMapper = onboardingRequestMapper;
        this.onboardingStateMapper = onboardingStateMapper;
        this.jwtService = jwtService;
    }

    public <C extends Event<OnboardingState, OnboardingPayload<Void>>> OnboardingRequestDTO execute(
            UUID onboardingRequestId, Class<C> event, String initiatorService) {
        return execute(onboardingRequestId, event, null, initiatorService);
    }

    public <T, C extends Event<OnboardingState, OnboardingPayload<T>>> OnboardingRequestDTO execute(
            UUID onboardingRequestId, Class<C> event, T actionPayload, String initiatorService) {
        var onboardingRequest = onboardingRequestRepository.findByIdOrThrow(onboardingRequestId);

        var stateMachine = createStateMachine();
        stateMachine.listeners().add(stateListener);
        stateMachine.reset(onboardingStateMapper.toOnboardingState(
                onboardingRequest.getOnboardingStatus().getValue()));

        var payload = new OnboardingPayload<>(onboardingRequest, actionPayload);
        payload.setInitiatorUserId(jwtService.isAuthenticated() ? jwtService.getUserId() : null);
        payload.setInitiatorService(initiatorService);

        doSendEvent(event, stateMachine, payload);

        stateMachine.stop();
        stateMachine.listeners().remove(stateListener);
        return onboardingRequestMapper.toOnboardingRequestDto(onboardingRequest);
    }

    private <T, C extends Event<OnboardingState, OnboardingPayload<T>>> void doSendEvent(
            Class<C> event, GenericStateMachine<OnboardingState> stateMachine, OnboardingPayload<T> payload) {
        try {
            stateMachine.sendEvent(event, payload);
        } catch (IllegalTransitionException | GuardFailedException e) {
            throw new OperationNotPermittedException(e);
        }
    }

    private GenericStateMachine<OnboardingState> createStateMachine() {
        var stateMachine = new GenericStateMachine<OnboardingState>();
        eventHandlers.forEach(stateMachine::addTransition);
        stateMachine.init();
        return stateMachine;
    }
}
