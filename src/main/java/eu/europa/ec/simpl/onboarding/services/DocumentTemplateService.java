package eu.europa.ec.simpl.onboarding.services;

import eu.europa.ec.simpl.common.model.dto.onboarding.DocumentTemplateDTO;
import java.util.UUID;

public interface DocumentTemplateService {

    UUID createDocumentTemplate(DocumentTemplateDTO documentTemplate);

    DocumentTemplateDTO findDocumentTemplateById(UUID uuid);

    void checkExistsByIdOrThrow(UUID id);
}
