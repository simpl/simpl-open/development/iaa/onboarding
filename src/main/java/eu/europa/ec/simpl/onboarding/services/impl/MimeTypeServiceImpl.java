package eu.europa.ec.simpl.onboarding.services.impl;

import eu.europa.ec.simpl.common.model.dto.onboarding.MimeTypeDTO;
import eu.europa.ec.simpl.onboarding.entities.MimeType;
import eu.europa.ec.simpl.onboarding.exceptions.MimeTypeNotFoundException;
import eu.europa.ec.simpl.onboarding.exceptions.UnsupportedMimeTypeException;
import eu.europa.ec.simpl.onboarding.mappers.MimeTypeMapper;
import eu.europa.ec.simpl.onboarding.repositories.MimeTypeRepository;
import eu.europa.ec.simpl.onboarding.services.MimeTypeService;
import eu.europa.ec.simpl.onboarding.utils.MimeTypeUtil;
import jakarta.validation.Valid;
import java.util.UUID;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

@Log4j2
@Service
@Validated
public class MimeTypeServiceImpl implements MimeTypeService {

    private final MimeTypeRepository mimeTypeRepository;
    private final MimeTypeMapper mimeTypeMapper;

    public MimeTypeServiceImpl(MimeTypeRepository mimeTypeRepository, MimeTypeMapper mimeTypeMapper) {
        this.mimeTypeRepository = mimeTypeRepository;
        this.mimeTypeMapper = mimeTypeMapper;
    }

    @Override
    public MimeType findMimeTypeByValue(String value) {
        return mimeTypeRepository.findMimeTypeByValue(value).orElseThrow(() -> new MimeTypeNotFoundException(value));
    }

    @Override
    public UUID createMimeType(@Valid MimeTypeDTO mimeType) {
        if (!MimeTypeUtil.isKnown(mimeType.getValue())) {
            throw new UnsupportedMimeTypeException(mimeType.getValue());
        }

        var entity = mimeTypeMapper.toEntity(mimeType);
        return mimeTypeRepository.save(entity).getId();
    }

    @Override
    public Page<MimeTypeDTO> search(Pageable pageable) {
        return mimeTypeRepository.findAll(pageable).map(mimeTypeMapper::toDTO);
    }

    @Override
    public MimeTypeDTO findById(UUID id) {
        var mimeType =
                mimeTypeRepository.findById(id).orElseThrow(() -> new MimeTypeNotFoundException(String.valueOf(id)));
        return mimeTypeMapper.toDTO(mimeType);
    }

    @Transactional
    @Override
    public void updateById(UUID id, @Valid MimeTypeDTO mimeTypeDTO) {
        var mimeType =
                mimeTypeRepository.findById(id).orElseThrow(() -> new MimeTypeNotFoundException(String.valueOf(id)));
        mimeTypeMapper.updateEntity(mimeType, mimeTypeDTO);
    }

    @Override
    public void deleteById(UUID id) {
        mimeTypeRepository.deleteById(id);
    }
}
