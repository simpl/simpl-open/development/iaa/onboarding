package eu.europa.ec.simpl.onboarding.liquibase;

import java.util.List;

public class QueryExecutor<T> {

    private final QueryFactory factory;
    private final Class<T> clazz;

    public QueryExecutor(QueryFactory factory, Class<T> clazz) {
        this.factory = factory;
        this.clazz = clazz;
    }

    public QueryExecutor(QueryFactory factory) {
        this.factory = factory;
        this.clazz = null;
    }

    public T query(String sql, Object... params) {
        var query = factory.createQuery(clazz, sql, params);
        return query.getResult();
    }

    public List<T> queryList(String sql, Object... params) {
        var query = factory.createQuery(clazz, sql, params);
        return query.getResultList();
    }

    public void insert(String sql, Object... params) {
        factory.createQuery(clazz, sql, params).insert();
    }

    public void update(String sql, Object... params) {
        insert(sql, params);
    }

    public void execute(String sql, Object... params) {
        factory.createQuery(clazz, sql, params).execute();
    }
}
