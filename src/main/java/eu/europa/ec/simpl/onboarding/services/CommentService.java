package eu.europa.ec.simpl.onboarding.services;

import eu.europa.ec.simpl.common.model.dto.onboarding.CommentDTO;
import java.util.UUID;

public interface CommentService {
    CommentDTO createComment(UUID onboardingRequestId, CommentDTO commentDTO);
}
