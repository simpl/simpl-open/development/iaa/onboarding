package eu.europa.ec.simpl.onboarding.exceptions;

import eu.europa.ec.simpl.common.exceptions.StatusException;
import java.util.UUID;
import org.springframework.http.HttpStatus;

public class OutcomeAlreadyGivenException extends StatusException {

    public OutcomeAlreadyGivenException(UUID id, String onboardingStatus) {
        super(
                HttpStatus.CONFLICT,
                "Onboarding request %s has already a final status %s and cannot be modified"
                        .formatted(id, onboardingStatus));
    }
}
