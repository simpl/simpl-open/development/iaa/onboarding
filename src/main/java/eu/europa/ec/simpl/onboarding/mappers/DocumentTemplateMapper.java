package eu.europa.ec.simpl.onboarding.mappers;

import eu.europa.ec.simpl.common.model.dto.onboarding.DocumentTemplateDTO;
import eu.europa.ec.simpl.onboarding.entities.DocumentTemplate;
import lombok.Generated;
import org.mapstruct.AnnotateWith;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(uses = {MimeTypeMapper.class})
@AnnotateWith(Generated.class)
public interface DocumentTemplateMapper {

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "mimeType", ignore = true)
    DocumentTemplate toEntity(DocumentTemplateDTO documentTemplateDTO);

    DocumentTemplateDTO toDto(DocumentTemplate documentTemplate);
}
