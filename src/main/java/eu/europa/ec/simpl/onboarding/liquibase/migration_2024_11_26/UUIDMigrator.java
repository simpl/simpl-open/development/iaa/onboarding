package eu.europa.ec.simpl.onboarding.liquibase.migration_2024_11_26;

import eu.europa.ec.simpl.onboarding.liquibase.QueryExecutor;
import eu.europa.ec.simpl.onboarding.liquibase.QueryFactory;
import eu.europa.ec.simpl.onboarding.liquibase.migration_2024_11_26.pojo.Document;
import eu.europa.ec.simpl.onboarding.liquibase.migration_2024_11_26.pojo.DocumentTemplate;
import eu.europa.ec.simpl.onboarding.liquibase.migration_2024_11_26.pojo.MimeType;
import eu.europa.ec.simpl.onboarding.liquibase.migration_2024_11_26.pojo.MimeTypeNew;
import eu.europa.ec.simpl.onboarding.liquibase.migration_2024_11_26.pojo.OnboardingProcedureTemplate;
import eu.europa.ec.simpl.onboarding.liquibase.migration_2024_11_26.pojo.OnboardingRequest;
import eu.europa.ec.simpl.onboarding.liquibase.migration_2024_11_26.pojo.OnboardingStatus;
import eu.europa.ec.simpl.onboarding.liquibase.migration_2024_11_26.pojo.OnboardingStatusNew;
import eu.europa.ec.simpl.onboarding.liquibase.migration_2024_11_26.pojo.ParticipantType;
import eu.europa.ec.simpl.onboarding.liquibase.migration_2024_11_26.pojo.ParticipantTypeNew;
import java.sql.SQLException;
import java.util.UUID;
import java.util.stream.Collectors;
import liquibase.change.custom.CustomTaskChange;
import liquibase.database.Database;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.CustomChangeException;
import liquibase.exception.DatabaseException;
import liquibase.exception.SetupException;
import liquibase.exception.ValidationErrors;
import liquibase.resource.ResourceAccessor;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;

@Log4j2
public class UUIDMigrator implements CustomTaskChange {

    @SneakyThrows
    @Override
    public void execute(Database database) throws CustomChangeException {
        try {
            process(database);
        } catch (SQLException | DatabaseException e) {
            throw new RuntimeException(e);
        }
    }

    private void process(Database database) throws SQLException, DatabaseException {

        JdbcConnection connection = jdbcConnection(database);
        var factory = newQueryFactory(connection);
        mimeTypeUUIDMigration(factory);
        participantTypeUUIDMigration(factory);
        onboardingStatusUUIDMigration(factory);

        var dbExecutor = new QueryExecutor<>(factory);
        dbExecutor.execute("drop sequence if exists mime_type_id_seq");
        dbExecutor.execute("drop sequence if exists onboarding_status_id_seq");
        dbExecutor.execute("drop sequence if exists participant_type_id_seq");
    }

    private void onboardingStatusUUIDMigration(QueryFactory factory) {

        var onboardingStatusQueryExecutor = newQueryExecutor(factory, OnboardingStatus.class);
        var onbReqQueryExecutor = newQueryExecutor(factory, OnboardingRequest.class);

        var onboardingStates =
                onboardingStatusQueryExecutor.queryList("select id, value from onboarding_status").stream()
                        .collect(Collectors.toMap(
                                OnboardingStatus::getId,
                                os -> new OnboardingStatusNew(UUID.randomUUID(), os.getValue())));

        var onboardingRequests = onbReqQueryExecutor.queryList(
                "select id, onboarding_status_id as onboardingStatusId from onboarding_request");

        onboardingStatusQueryExecutor.execute(
                "alter table onboarding_status drop constraint onboarding_status_pkey cascade");
        onboardingStatusQueryExecutor.execute("alter table onboarding_status alter column id drop identity");
        onboardingStatusQueryExecutor.execute("alter table onboarding_status alter column id drop not null");
        onboardingStatusQueryExecutor.execute(
                "alter table onboarding_status alter column id type uuid using gen_random_uuid()");

        onboardingStates
                .values()
                .forEach(onboardingStatusNew -> onboardingStatusQueryExecutor.update(
                        "update onboarding_status set id=? where value=?",
                        onboardingStatusNew.id(),
                        onboardingStatusNew.value()));
        onboardingStatusQueryExecutor.execute("alter table onboarding_status add primary key (id)");

        onbReqQueryExecutor.execute(
                "alter table onboarding_request alter column onboarding_status_id type uuid using gen_random_uuid()");
        onboardingRequests.forEach(onboardingRequest -> onbReqQueryExecutor.update(
                "update onboarding_request set onboarding_status_id=? where id=?",
                onboardingStates.get(onboardingRequest.getOnboardingStatusId()).id(),
                onboardingRequest.getId()));
    }

    private void participantTypeUUIDMigration(QueryFactory factory) {

        var pTypeQueryExecutor = newQueryExecutor(factory, ParticipantType.class);
        var onbReqQueryExecutor = newQueryExecutor(factory, OnboardingRequest.class);
        var onbProcedureTemplateQueryExecutor = newQueryExecutor(factory, OnboardingProcedureTemplate.class);

        var pTypes = pTypeQueryExecutor.queryList("select id, value from participant_type").stream()
                .collect(Collectors.toMap(
                        ParticipantType::getId, p -> new ParticipantTypeNew(UUID.randomUUID(), p.getValue())));

        var onboardingRequests = onbReqQueryExecutor.queryList(
                "select id, participant_type_id as participantTypeId from onboarding_request");

        var onboardingProcedureTemplates = onbProcedureTemplateQueryExecutor.queryList(
                "select id, participant_type_id as participantTypeId from onboarding_procedure_template");

        pTypeQueryExecutor.execute("alter table participant_type drop constraint participant_type_pkey cascade");
        pTypeQueryExecutor.execute("alter table participant_type alter column id drop identity");
        pTypeQueryExecutor.execute("alter table participant_type alter column id drop not null");
        pTypeQueryExecutor.execute("alter table participant_type alter column id type uuid using gen_random_uuid()");

        pTypes.values()
                .forEach(pType -> pTypeQueryExecutor.update(
                        "update participant_type set id=? where value=?", pType.id(), pType.value()));
        pTypeQueryExecutor.execute("alter table participant_type add primary key (id)");

        onbReqQueryExecutor.execute(
                "alter table onboarding_request alter column participant_type_id type uuid using gen_random_uuid()");
        onboardingRequests.forEach(onboardingRequest -> onbReqQueryExecutor.update(
                "update onboarding_request set participant_type_id=? where id=?",
                pTypes.get(onboardingRequest.getParticipantTypeId()).id(),
                onboardingRequest.getId()));

        onbProcedureTemplateQueryExecutor.execute(
                "alter table onboarding_procedure_template alter column participant_type_id type uuid using gen_random_uuid()");
        onboardingProcedureTemplates.forEach(onboardingRequest -> onbReqQueryExecutor.update(
                "update onboarding_procedure_template set participant_type_id=? where id=?",
                pTypes.get(onboardingRequest.getParticipantTypeId()).id(),
                onboardingRequest.getId()));
    }

    private void mimeTypeUUIDMigration(QueryFactory factory) {

        var mimeTypeQueryExecutor = newQueryExecutor(factory, MimeType.class);
        var documentQueryExecutor = newQueryExecutor(factory, Document.class);
        var documentTemplateQueryExecutor = newQueryExecutor(factory, DocumentTemplate.class);

        var mimeTypes = mimeTypeQueryExecutor.queryList("select id, value from mime_type").stream()
                .collect(Collectors.toMap(MimeType::getId, m -> new MimeTypeNew(UUID.randomUUID(), m.getValue())));

        var documents = documentQueryExecutor.queryList(
                "select id, mime_type_id as mimeTypeId from document where mime_type_id is not null");

        var templates =
                documentTemplateQueryExecutor.queryList("select id, mime_type_id as mimeTypeId from document_template");

        mimeTypeQueryExecutor.execute("alter table mime_type drop constraint mime_type_pkey cascade");
        mimeTypeQueryExecutor.execute("alter table mime_type alter column id drop identity");
        mimeTypeQueryExecutor.execute("alter table mime_type alter column id drop not null");
        mimeTypeQueryExecutor.execute("alter table mime_type alter column id type uuid using gen_random_uuid()");
        mimeTypes
                .values()
                .forEach(mimeType -> mimeTypeQueryExecutor.update(
                        "update mime_type set id=? where value=?", mimeType.id(), mimeType.value()));
        mimeTypeQueryExecutor.execute("alter table mime_type add primary key (id)");

        documentQueryExecutor.execute("alter table document alter column mime_type_id type uuid using null");
        documents.forEach(document -> documentQueryExecutor.update(
                "update document set mime_type_id=? where id=?",
                mimeTypes.get(document.getMimeTypeId()).id(),
                document.getId()));

        documentTemplateQueryExecutor.execute(
                "alter table document_template alter column mime_type_id type uuid  using gen_random_uuid()");
        templates.forEach(template -> documentTemplateQueryExecutor.update(
                "update document_template set mime_type_id=? where id=?",
                mimeTypes.get(template.getMimeTypeId()).id(),
                template.getId()));
    }

    @Override
    public String getConfirmationMessage() {
        return "";
    }

    @Override
    public void setUp() throws SetupException {}

    @Override
    public void setFileOpener(ResourceAccessor resourceAccessor) {}

    @Override
    public ValidationErrors validate(Database database) {
        return null;
    }

    protected QueryFactory newQueryFactory(JdbcConnection connection) {
        return new QueryFactory(connection.getUnderlyingConnection());
    }

    protected JdbcConnection jdbcConnection(Database database) {
        return (JdbcConnection) database.getConnection();
    }

    protected <T> QueryExecutor<T> newQueryExecutor(QueryFactory factory, Class<T> clazz) {
        return new QueryExecutor<>(factory, clazz);
    }
}
