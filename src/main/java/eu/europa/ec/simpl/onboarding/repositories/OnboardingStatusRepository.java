package eu.europa.ec.simpl.onboarding.repositories;

import eu.europa.ec.simpl.common.model.dto.onboarding.OnboardingStatusValue;
import eu.europa.ec.simpl.onboarding.entities.OnboardingStatus;
import java.util.Optional;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OnboardingStatusRepository extends JpaRepository<OnboardingStatus, UUID> {

    Optional<OnboardingStatus> findByValue(OnboardingStatusValue status);
}
