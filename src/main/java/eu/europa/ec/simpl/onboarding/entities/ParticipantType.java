package eu.europa.ec.simpl.onboarding.entities;

import eu.europa.ec.simpl.common.entity.annotations.UUIDv7Generator;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import java.util.UUID;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Accessors(chain = true)
@Getter
@Setter
@Entity
@Table(name = "participant_type")
@ToString
@NoArgsConstructor
public class ParticipantType {
    @Id
    @UUIDv7Generator
    private UUID id;

    @Column(name = "value", nullable = false, unique = true, length = 100)
    private String value;

    @Column(name = "label")
    private String label;
}
