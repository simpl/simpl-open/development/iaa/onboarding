package eu.europa.ec.simpl.onboarding.statemachine;

import static eu.europa.ec.simpl.onboarding.statemachine.common.State.Type.FINAL;
import static eu.europa.ec.simpl.onboarding.statemachine.common.State.Type.INITIAL;

import eu.europa.ec.simpl.onboarding.statemachine.common.State;

public enum OnboardingState implements State {
    IN_PROGRESS(INITIAL),
    IN_REVIEW,
    APPROVED(FINAL),
    REJECTED(FINAL);

    private final State.Type type;

    OnboardingState(State.Type type) {
        this.type = type;
    }

    OnboardingState() {
        this(State.Type.STATE);
    }

    @Override
    public State.Type getType() {
        return type;
    }
}
