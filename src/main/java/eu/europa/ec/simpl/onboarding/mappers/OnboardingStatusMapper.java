package eu.europa.ec.simpl.onboarding.mappers;

import eu.europa.ec.simpl.common.model.dto.onboarding.OnboardingStatusDTO;
import eu.europa.ec.simpl.onboarding.entities.OnboardingStatus;
import lombok.Generated;
import org.mapstruct.AnnotateWith;
import org.mapstruct.Mapper;

@Mapper
@AnnotateWith(Generated.class)
public interface OnboardingStatusMapper {

    OnboardingStatusDTO toDto(OnboardingStatus onboardingStatus);
}
