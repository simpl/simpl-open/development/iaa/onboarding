package eu.europa.ec.simpl.onboarding.mappers;

import eu.europa.ec.simpl.common.model.dto.onboarding.CommentDTO;
import eu.europa.ec.simpl.onboarding.entities.Comment;
import java.util.UUID;
import lombok.Generated;
import org.mapstruct.AnnotateWith;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(uses = {ReferenceMapper.class})
@AnnotateWith(Generated.class)
public interface CommentMapper {

    @Mapping(target = "onboardingRequest", source = "onboardingRequestId")
    @Mapping(target = "creationTimestamp", ignore = true)
    @Mapping(target = "updateTimestamp", ignore = true)
    Comment toEntity(CommentDTO commentDTO, UUID onboardingRequestId);

    CommentDTO toCommentDto(Comment comment);
}
