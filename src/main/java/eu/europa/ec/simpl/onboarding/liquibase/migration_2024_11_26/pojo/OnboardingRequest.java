package eu.europa.ec.simpl.onboarding.liquibase.migration_2024_11_26.pojo;

import java.util.UUID;
import lombok.Data;

@Data
public class OnboardingRequest {

    private UUID id;
    private Long participantTypeId;
    private Long onboardingStatusId;
}
