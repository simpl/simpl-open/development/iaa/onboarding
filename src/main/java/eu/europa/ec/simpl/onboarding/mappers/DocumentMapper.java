package eu.europa.ec.simpl.onboarding.mappers;

import static org.mapstruct.NullValuePropertyMappingStrategy.IGNORE;
import static org.mapstruct.ReportingPolicy.ERROR;

import eu.europa.ec.simpl.common.model.dto.onboarding.DocumentDTO;
import eu.europa.ec.simpl.common.utils.Base64Util;
import eu.europa.ec.simpl.onboarding.entities.Document;
import java.util.UUID;
import lombok.Generated;
import org.mapstruct.AnnotateWith;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Named;

@Mapper(uses = {ReferenceMapper.class, BlobMapper.class})
@AnnotateWith(Generated.class)
public interface DocumentMapper {

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "updateTimestamp", ignore = true)
    @Mapping(target = "creationTimestamp", ignore = true)
    @Mapping(target = "documentTemplate", source = "dto.documentTemplate.id")
    @BeanMapping(unmappedTargetPolicy = ERROR)
    Document toEntity(DocumentDTO dto, UUID onboardingRequest);

    @Mapping(target = "content", ignore = true)
    @Mapping(target = "documentTemplate.mimeType", source = "entity.documentTemplate.mimeType")
    DocumentDTO toDTO(Document entity);

    @Named("toFullDTO")
    @Mapping(target = "documentTemplate.mimeType", source = "entity.documentTemplate.mimeType")
    DocumentDTO toFullDTO(Document entity);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "updateTimestamp", ignore = true)
    @Mapping(target = "creationTimestamp", ignore = true)
    @Mapping(target = "onboardingRequest", ignore = true)
    @Mapping(target = "documentTemplate", source = "dto.documentTemplate.id")
    @Mapping(target = "filesize", source = "content", qualifiedByName = "toFilesize")
    @BeanMapping(unmappedTargetPolicy = ERROR, nullValuePropertyMappingStrategy = IGNORE)
    void updateDocument(@MappingTarget Document document, DocumentDTO dto);

    @Named("toFilesize")
    default long toFilesize(String base64) {
        return Base64Util.getSizeInBytes(base64);
    }
}
