package eu.europa.ec.simpl.onboarding.statemachine;

import eu.europa.ec.simpl.common.model.dto.onboarding.OnboardingStatusValue;
import org.mapstruct.Mapper;

@Mapper
public interface OnboardingStateMapper {

    OnboardingStatusValue toOnboardingStatus(OnboardingState onboardingState);

    OnboardingState toOnboardingState(OnboardingStatusValue status);
}
