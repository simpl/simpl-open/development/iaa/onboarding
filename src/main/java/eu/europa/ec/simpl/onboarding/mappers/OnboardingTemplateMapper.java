package eu.europa.ec.simpl.onboarding.mappers;

import eu.europa.ec.simpl.common.model.dto.onboarding.DocumentTemplateDTO;
import eu.europa.ec.simpl.common.model.dto.onboarding.OnboardingTemplateDTO;
import eu.europa.ec.simpl.common.model.dto.onboarding.ParticipantTypeDTO;
import eu.europa.ec.simpl.onboarding.entities.DocumentTemplate;
import eu.europa.ec.simpl.onboarding.entities.OnboardingProcedureTemplate;
import java.util.List;
import java.util.Set;
import lombok.Generated;
import org.mapstruct.AnnotateWith;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper(uses = DocumentTemplateMapper.class)
@AnnotateWith(Generated.class)
public interface OnboardingTemplateMapper {

    OnboardingTemplateDTO toDTO(OnboardingProcedureTemplate onboardingProcedureTemplate);

    List<DocumentTemplateDTO> toDTOs(List<DocumentTemplate> documentTemplates);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "creationTimestamp", ignore = true)
    @Mapping(target = "updateTimestamp", ignore = true)
    @Mapping(target = "participantType", source = "participantType")
    @Mapping(target = "documentTemplates", source = "documentTemplates")
    void updateEntity(
            @MappingTarget OnboardingProcedureTemplate onboardingTemplate,
            OnboardingTemplateDTO onboardingTemplateDTO,
            Set<DocumentTemplate> documentTemplates,
            ParticipantTypeDTO participantType);
}
