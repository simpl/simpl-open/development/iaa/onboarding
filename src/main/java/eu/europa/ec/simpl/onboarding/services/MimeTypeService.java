package eu.europa.ec.simpl.onboarding.services;

import eu.europa.ec.simpl.common.model.dto.onboarding.MimeTypeDTO;
import eu.europa.ec.simpl.onboarding.entities.MimeType;
import jakarta.validation.Valid;
import java.util.UUID;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface MimeTypeService {
    MimeType findMimeTypeByValue(String fileName);

    UUID createMimeType(@Valid MimeTypeDTO mimeType);

    Page<MimeTypeDTO> search(Pageable pageable);

    MimeTypeDTO findById(UUID id);

    void updateById(UUID id, @Valid MimeTypeDTO mimeType);

    void deleteById(UUID id);
}
