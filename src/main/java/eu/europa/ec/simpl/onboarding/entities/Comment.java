package eu.europa.ec.simpl.onboarding.entities;

import eu.europa.ec.simpl.common.entity.annotations.UUIDv7Generator;
import jakarta.persistence.Basic;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import java.time.Instant;
import java.util.UUID;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@Accessors(chain = true)
@ToString
@Getter
@Setter
@Entity
@Table(name = "comment")
@NoArgsConstructor
public class Comment {
    @Id
    @UUIDv7Generator
    private UUID id;

    @ToString.Exclude
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "onboarding_request_id", nullable = false)
    private OnboardingRequest onboardingRequest;

    @Column(name = "author", nullable = false)
    private String author;

    @Basic(fetch = FetchType.LAZY)
    @ToString.Exclude
    @Column(name = "content", nullable = false, length = 5000)
    @Size(min = 1, max = 5000)
    @NotBlank
    private String content;

    @CreationTimestamp
    @Column(name = "creation_timestamp", nullable = false)
    private Instant creationTimestamp;

    @UpdateTimestamp
    @Column(name = "update_timestamp")
    private Instant updateTimestamp;
}
