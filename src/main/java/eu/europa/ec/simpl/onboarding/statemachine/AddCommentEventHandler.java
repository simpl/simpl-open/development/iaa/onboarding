package eu.europa.ec.simpl.onboarding.statemachine;

import eu.europa.ec.simpl.common.model.dto.onboarding.CommentDTO;
import eu.europa.ec.simpl.common.security.JwtService;
import eu.europa.ec.simpl.onboarding.entities.Comment;
import eu.europa.ec.simpl.onboarding.entities.OnboardingRequest;
import eu.europa.ec.simpl.onboarding.mappers.ReferenceMapper;
import eu.europa.ec.simpl.onboarding.services.CommentService;
import org.springframework.stereotype.Component;

@Component
public class AddCommentEventHandler implements AddCommentEvent, ApplicantEvent<CommentDTO>, NotaryEvent<CommentDTO> {

    private final JwtService jwtService;
    private final ReferenceMapper referenceMapper;
    private final CommentService commentService;

    public AddCommentEventHandler(
            JwtService jwtService, ReferenceMapper referenceMapper, CommentService commentService) {
        this.jwtService = jwtService;
        this.referenceMapper = referenceMapper;
        this.commentService = commentService;
    }

    @Override
    public void handle(OnboardingState source, OnboardingState target, OnboardingPayload<CommentDTO> payload) {
        var onboardingRequest = payload.getOnboardingRequest();
        var comment = createComment(onboardingRequest, payload.getActionPayload());
        onboardingRequest.addComment(comment);
        // TODO This is an implicit contract between this class and OnboardingStateListener, should be generalized
        payload.getActionPayload().setId(comment.getId());
    }

    private Comment createComment(OnboardingRequest onboardingRequest, CommentDTO comment) {
        return referenceMapper.toComment(
                commentService.createComment(onboardingRequest.getId(), comment).getId());
    }

    @Override
    public boolean guard(OnboardingState source, OnboardingState target, OnboardingPayload<CommentDTO> payload) {
        return switch (source) {
            case IN_REVIEW -> NotaryEvent.super.guard(source, target, payload);
            case IN_PROGRESS -> ApplicantEvent.super.guard(source, target, payload);
            default -> false;
        };
    }

    @Override
    public JwtService getJwtService() {
        return jwtService;
    }
}
