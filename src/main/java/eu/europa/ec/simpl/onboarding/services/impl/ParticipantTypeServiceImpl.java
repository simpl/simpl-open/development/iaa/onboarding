package eu.europa.ec.simpl.onboarding.services.impl;

import eu.europa.ec.simpl.common.model.dto.onboarding.ParticipantTypeDTO;
import eu.europa.ec.simpl.onboarding.exceptions.ParticipantTypeNotFoundException;
import eu.europa.ec.simpl.onboarding.mappers.ParticipantTypeMapper;
import eu.europa.ec.simpl.onboarding.repositories.ParticipantTypeRepository;
import eu.europa.ec.simpl.onboarding.services.ParticipantTypeService;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

@Log4j2
@Service
@Validated
public class ParticipantTypeServiceImpl implements ParticipantTypeService {

    private final ParticipantTypeRepository participantTypeRepository;
    private final ParticipantTypeMapper participantTypeMapper;

    public ParticipantTypeServiceImpl(
            ParticipantTypeRepository participantTypeRepository, ParticipantTypeMapper participantTypeMapper) {
        this.participantTypeRepository = participantTypeRepository;
        this.participantTypeMapper = participantTypeMapper;
    }

    @Override
    public ParticipantTypeDTO findParticipantTypeById(UUID participantTypeId) {
        return participantTypeRepository
                .findById(participantTypeId)
                .map(participantTypeMapper::toDto)
                .orElseThrow(() -> new ParticipantTypeNotFoundException(participantTypeId));
    }

    @Override
    public Set<ParticipantTypeDTO> findAllParticipantTypes() {
        return participantTypeRepository.findAll().stream()
                .map(participantTypeMapper::toDto)
                .collect(Collectors.toSet());
    }
}
