package eu.europa.ec.simpl.onboarding.validators;

import jakarta.validation.groups.Default;

public interface CreateOnboardingRequestOperation extends Default {}
