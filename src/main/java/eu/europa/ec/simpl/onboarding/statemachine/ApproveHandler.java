package eu.europa.ec.simpl.onboarding.statemachine;

import eu.europa.ec.simpl.common.exchanges.identityprovider.IdentityProviderParticipantExchange;
import eu.europa.ec.simpl.common.model.dto.identityprovider.ParticipantWithIdentityAttributesDTO;
import eu.europa.ec.simpl.common.model.dto.onboarding.ApproveDTO;
import eu.europa.ec.simpl.common.model.dto.securityattributesprovider.IdentityAttributeDTO;
import eu.europa.ec.simpl.common.security.JwtService;
import eu.europa.ec.simpl.onboarding.entities.OnboardingRequest;
import eu.europa.ec.simpl.onboarding.mappers.OnboardingRequestMapper;
import java.util.List;
import java.util.UUID;
import org.springframework.stereotype.Component;

@Component
public class ApproveHandler implements Approve, NotaryEvent<ApproveDTO> {

    private final JwtService jwtService;
    private final StatusChangeHandler statusChangeHandler;
    private final IdentityProviderParticipantExchange participantExchange;
    private final OnboardingRequestMapper onboardingRequestMapper;

    protected ApproveHandler(
            JwtService jwtService,
            StatusChangeHandler statusChangeHandler,
            IdentityProviderParticipantExchange participantExchange,
            OnboardingRequestMapper onboardingRequestMapper) {
        this.jwtService = jwtService;
        this.statusChangeHandler = statusChangeHandler;
        this.participantExchange = participantExchange;
        this.onboardingRequestMapper = onboardingRequestMapper;
    }

    @Override
    public void handle(OnboardingState source, OnboardingState target, OnboardingPayload<ApproveDTO> payload) {
        var onboardingRequest = payload.getOnboardingRequest();
        statusChangeHandler.changeStatus(onboardingRequest, target);
        var participantId =
                createParticipant(onboardingRequest, payload.getActionPayload().getIdentityAttributes());
        onboardingRequest.setParticipantId(participantId);
    }

    @Override
    public boolean guard(OnboardingState source, OnboardingState target, OnboardingPayload<ApproveDTO> payload) {
        return NotaryEvent.super.guard(source, target, payload);
    }

    private UUID createParticipant(OnboardingRequest onboardingRequest, List<UUID> identityAttributes) {
        return participantExchange.create(new ParticipantWithIdentityAttributesDTO()
                .setParticipant(onboardingRequestMapper.toParticipantDTO(onboardingRequest))
                .setIdentityAttributes(identityAttributes.stream()
                        .map(id -> new IdentityAttributeDTO().setId(id))
                        .toList()));
    }

    @Override
    public JwtService getJwtService() {
        return jwtService;
    }
}
