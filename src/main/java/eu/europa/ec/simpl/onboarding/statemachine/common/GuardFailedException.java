package eu.europa.ec.simpl.onboarding.statemachine.common;

import lombok.Getter;

@Getter
public class GuardFailedException extends RuntimeException {

    private final Object source;
    private final Object target;
    private final Object event;

    public GuardFailedException(Object source, Object target, Object event) {
        super("Guard failed while transitioning from state %s to state %s with event %s"
                .formatted(source, target, event));
        this.source = source;
        this.target = target;
        this.event = event;
    }
}
