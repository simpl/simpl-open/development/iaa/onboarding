package eu.europa.ec.simpl.onboarding.exceptions;

import eu.europa.ec.simpl.common.exceptions.StatusException;
import java.util.UUID;
import org.springframework.http.HttpStatus;

public class DocumentTemplateNotFoundException extends StatusException {
    public DocumentTemplateNotFoundException(UUID id) {
        this(HttpStatus.NOT_FOUND, id);
    }

    public DocumentTemplateNotFoundException(HttpStatus httpStatus, UUID id) {
        super(httpStatus, "Document template with id %s not found".formatted(id));
    }
}
