package eu.europa.ec.simpl.onboarding.mappers;

import eu.europa.ec.simpl.common.model.dto.identityprovider.ParticipantDTO;
import eu.europa.ec.simpl.common.model.dto.onboarding.OnboardingRequestDTO;
import eu.europa.ec.simpl.common.model.dto.onboarding.OnboardingStatusDTO;
import eu.europa.ec.simpl.common.model.dto.onboarding.OnboardingTemplateDTO;
import eu.europa.ec.simpl.onboarding.entities.OnboardingApplicant;
import eu.europa.ec.simpl.onboarding.entities.OnboardingRequest;
import lombok.Generated;
import org.mapstruct.AnnotateWith;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

@Mapper(
        uses = {
            ReferenceMapper.class,
            BlobMapper.class,
            OnboardingApplicantMapper.class,
            DocumentMapper.class,
            DocumentTemplateMapper.class
        })
@AnnotateWith(Generated.class)
public interface OnboardingRequestMapper {

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "onboardingProcedureTemplate", source = "onboardingTemplateDTO.id")
    @Mapping(target = "onboardingStatus.id", source = "initialStatus.id")
    @Mapping(target = "lastStatusUpdateTimestamp", expression = "java( java.time.Instant.now() )")
    @Mapping(target = "creationTimestamp", ignore = true)
    @Mapping(target = "updateTimestamp", ignore = true)
    @Mapping(target = "documents", ignore = true)
    @Mapping(target = "comments", ignore = true)
    @Mapping(target = "participantId", ignore = true)
    @Mapping(target = "rejectionCause", ignore = true)
    @BeanMapping(unmappedTargetPolicy = ReportingPolicy.ERROR)
    OnboardingRequest create(
            OnboardingApplicant onboardingApplicant,
            OnboardingTemplateDTO onboardingTemplateDTO,
            OnboardingStatusDTO initialStatus,
            String organization);

    @BeanMapping(unmappedTargetPolicy = ReportingPolicy.ERROR)
    @Mapping(target = "applicant", source = "onboardingApplicant")
    @Mapping(target = "status", source = "onboardingStatus")
    @Mapping(target = "documents", qualifiedByName = "toFullDTO")
    OnboardingRequestDTO toOnboardingRequestDto(OnboardingRequest entity);

    @BeanMapping(unmappedTargetPolicy = ReportingPolicy.ERROR)
    @Mapping(target = "participantType", source = "participantType.value")
    @Mapping(target = "credentialId", ignore = true)
    @Mapping(target = "expiryDate", ignore = true)
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "creationTimestamp", ignore = true)
    @Mapping(target = "updateTimestamp", ignore = true)
    ParticipantDTO toParticipantDTO(OnboardingRequest entity);
}
