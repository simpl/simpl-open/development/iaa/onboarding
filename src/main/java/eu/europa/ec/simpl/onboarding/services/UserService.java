package eu.europa.ec.simpl.onboarding.services;

import eu.europa.ec.simpl.common.model.dto.usersroles.KeycloakUserDTO;

public interface UserService {

    String createUser(KeycloakUserDTO keycloakUserDTO);
}
