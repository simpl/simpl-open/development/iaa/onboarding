package eu.europa.ec.simpl.onboarding.mappers;

import eu.europa.ec.simpl.common.model.dto.onboarding.OnboardingApplicantDTO;
import eu.europa.ec.simpl.common.model.dto.onboarding.ParticipantTypeDTO;
import eu.europa.ec.simpl.common.model.dto.usersroles.KeycloakUserDTO;
import eu.europa.ec.simpl.onboarding.entities.OnboardingApplicant;
import java.util.List;
import lombok.Generated;
import org.mapstruct.AnnotateWith;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

@Mapper
@AnnotateWith(Generated.class)
public interface OnboardingApplicantMapper {

    @Mapping(target = "id", ignore = true)
    OnboardingApplicant toEntity(
            OnboardingApplicantDTO applicantDTO, ParticipantTypeDTO participantType, String organization);

    OnboardingApplicantDTO toDto(OnboardingApplicant applicant);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "enabled", constant = "true")
    @BeanMapping(unmappedTargetPolicy = ReportingPolicy.ERROR)
    KeycloakUserDTO toKeycloakUserDto(OnboardingApplicantDTO applicantDTO, List<String> roles);
}
