package eu.europa.ec.simpl.onboarding.exceptions;

import eu.europa.ec.simpl.common.exceptions.StatusException;
import java.util.UUID;
import org.springframework.http.HttpStatus;

public class OnboardingRequestNotFoundException extends StatusException {
    public OnboardingRequestNotFoundException(UUID id) {
        this(HttpStatus.NOT_FOUND, id);
    }

    public OnboardingRequestNotFoundException(HttpStatus status, UUID id) {
        super(status, "Onboarding request %s not found".formatted(id));
    }
}
