package eu.europa.ec.simpl.onboarding.controllers;

import eu.europa.ec.simpl.api.onboarding.v1.exchanges.OnboardingTemplatesApi;
import eu.europa.ec.simpl.api.onboarding.v1.model.OnboardingTemplateDTO;
import eu.europa.ec.simpl.onboarding.mappers.OnboardingTemplateMapperV1;
import java.util.List;
import java.util.UUID;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("v1")
public class OnboardingTemplateControllerV1 implements OnboardingTemplatesApi {

    private final OnboardingTemplateController controller;
    private final OnboardingTemplateMapperV1 mapper;

    public OnboardingTemplateControllerV1(OnboardingTemplateController controller, OnboardingTemplateMapperV1 mapper) {
        this.controller = controller;
        this.mapper = mapper;
    }

    @Override
    public void deleteOnboardingTemplate(UUID participantTypeId) {
        controller.deleteOnboardingTemplate(participantTypeId);
    }

    @Override
    public OnboardingTemplateDTO getOnboardingTemplate(UUID participantTypeId) {
        return mapper.toV1(controller.getOnboardingTemplate(participantTypeId).getBody());
    }

    @Override
    public List<OnboardingTemplateDTO> getOnboardingTemplates() {
        return mapper.toV1(controller.getOnboardingTemplates());
    }

    @Override
    public OnboardingTemplateDTO updateOnboardingTemplate(
            UUID participantTypeId, OnboardingTemplateDTO onboardingTemplateDTO) {
        controller.updateOnboardingTemplate(participantTypeId, mapper.toV0(onboardingTemplateDTO));
        return mapper.toV1(controller.getOnboardingTemplate(participantTypeId).getBody());
    }

    @Override
    public void updateOnboardingTemplateDocuments(UUID participantTypeId, List<UUID> UUID) {
        controller.updateOnboardingTemplate(participantTypeId, UUID);
    }
}
