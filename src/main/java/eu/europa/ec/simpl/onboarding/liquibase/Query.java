package eu.europa.ec.simpl.onboarding.liquibase;

import eu.europa.ec.simpl.common.exceptions.RuntimeWrapperException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

public class Query<T> {

    private final Connection connection;
    private final QueryRunner runner;
    private final ResultSetHandler<T> beanHandler;
    private final ResultSetHandler<List<T>> beanListHandler;
    private final String query;
    private final Object[] params;

    public Query(Connection connection, QueryRunner runner, Class<T> clazz, String query, Object... params) {
        this.connection = connection;
        this.runner = runner;
        this.beanHandler = new BeanHandler<>(clazz);
        this.beanListHandler = new BeanListHandler<>(clazz);
        this.query = query;
        this.params = params;
    }

    public T getResult() {
        try {
            return runner.query(connection, query, beanHandler, params);
        } catch (SQLException e) {
            throw new RuntimeWrapperException(e);
        }
    }

    public List<T> getResultList() {
        try {
            return runner.query(connection, query, beanListHandler, params);
        } catch (SQLException e) {
            throw new RuntimeWrapperException(e);
        }
    }

    public void insert() {
        try {
            runner.update(connection, query, params);
        } catch (SQLException e) {
            throw new RuntimeWrapperException(e);
        }
    }

    public void execute() {
        try {
            runner.execute(connection, query, params);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
