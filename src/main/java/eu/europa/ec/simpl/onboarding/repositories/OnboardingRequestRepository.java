package eu.europa.ec.simpl.onboarding.repositories;

import eu.europa.ec.simpl.onboarding.entities.OnboardingRequest;
import eu.europa.ec.simpl.onboarding.entities.OnboardingRequest_;
import eu.europa.ec.simpl.onboarding.exceptions.OnboardingRequestNotFoundException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface OnboardingRequestRepository
        extends JpaRepository<OnboardingRequest, UUID>, JpaSpecificationExecutor<OnboardingRequest> {

    default OnboardingRequest findByIdOrThrow(UUID id) {
        return findById(id).orElseThrow(() -> new OnboardingRequestNotFoundException(id));
    }

    @EntityGraph(
            attributePaths = {
                OnboardingRequest_.ONBOARDING_STATUS,
                OnboardingRequest_.DOCUMENTS,
                OnboardingRequest_.PARTICIPANT_TYPE,
                OnboardingRequest_.ONBOARDING_APPLICANT
            })
    @Override
    Optional<OnboardingRequest> findById(UUID uuid);

    @Query(
            value =
                    """
    select request.*
    from onboarding_request request join onboarding_status status on request.onboarding_status_id = status.id
    where
      status.value = 'IN_PROGRESS'
      and request.last_status_update_timestamp + interval '1 second' * request.expiration_timeframe < CURRENT_TIMESTAMP
    """,
            nativeQuery = true)
    List<OnboardingRequest> findInProgressWithExpiredTimeframe();
}
