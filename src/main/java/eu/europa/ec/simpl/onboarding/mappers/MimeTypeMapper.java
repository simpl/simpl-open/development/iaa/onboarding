package eu.europa.ec.simpl.onboarding.mappers;

import eu.europa.ec.simpl.common.model.dto.onboarding.MimeTypeDTO;
import eu.europa.ec.simpl.onboarding.entities.MimeType;
import lombok.Generated;
import org.mapstruct.AnnotateWith;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper
@AnnotateWith(Generated.class)
public interface MimeTypeMapper {

    MimeTypeDTO toDTO(MimeType mimeType);

    @Mapping(target = "id", ignore = true)
    MimeType toEntity(MimeTypeDTO mimeTypeDTO);

    @Mapping(target = "id", ignore = true)
    void updateEntity(@MappingTarget MimeType mimeType, MimeTypeDTO mimeTypeDTO);
}
