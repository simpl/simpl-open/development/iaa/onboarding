package eu.europa.ec.simpl.onboarding.statemachine;

import eu.europa.ec.simpl.common.constants.Roles;
import eu.europa.ec.simpl.common.security.JwtService;
import eu.europa.ec.simpl.onboarding.exceptions.OperationNotPermittedException;

interface NotaryEvent<T> {

    default boolean guard(OnboardingState source, OnboardingState target, OnboardingPayload<T> payload) {
        if (getJwtService().hasRole(Roles.NOTARY)) {
            return true;
        } else {
            throw new OperationNotPermittedException();
        }
    }

    JwtService getJwtService();
}
