package eu.europa.ec.simpl.onboarding.statemachine.common;

import java.util.Arrays;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import lombok.Value;
import org.apache.commons.lang3.ClassUtils;

@Value
class EventType {
    Class<?> type;

    public static EventType fromInterface(Class<?> clazz) {
        return new EventType(clazz);
    }

    public static EventType fromClass(Class<?> clazz) {
        return new EventType(getEventSuperInterface(clazz));
    }

    private static Class<?> getEventSuperInterface(Class<?> type) {

        var eventInterfaces = ClassUtils.getAllInterfaces(type).stream()
                .filter(notEventInterface())
                .filter(hasEventSuperInterface())
                .toList();

        if (eventInterfaces.size() > 1) {
            throw new IllegalArgumentException("Type %s implements multiple %s interfaces: [%s]"
                    .formatted(
                            type.getName(),
                            Event.class.getSimpleName(),
                            eventInterfaces.stream().map(Class::getName).collect(Collectors.joining(", "))));
        }

        return eventInterfaces.getFirst();
    }

    private static Predicate<? super Class<?>> notEventInterface() {
        return i -> i != BasicEvent.class && i != Event.class;
    }

    private static Predicate<? super Class<?>> hasEventSuperInterface() {
        return i -> Arrays.stream(i.getInterfaces()).anyMatch(Event.class::isAssignableFrom);
    }
}
