package eu.europa.ec.simpl.onboarding.entities;

import eu.europa.ec.simpl.common.entity.annotations.UUIDv7Generator;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import java.time.Instant;
import java.util.Set;
import java.util.UUID;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@Accessors(chain = true)
@ToString
@Getter
@Setter
@Entity
@Table(name = "document_template")
@NoArgsConstructor
public class DocumentTemplate {
    @Id
    @UUIDv7Generator
    private UUID id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "description", nullable = false)
    private String description;

    @Getter(value = AccessLevel.NONE)
    @Column(name = "mandatory", nullable = false)
    private Boolean mandatory;

    @ToString.Exclude
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "mime_type_id", nullable = false)
    private MimeType mimeType;

    @CreationTimestamp
    @Column(name = "creation_timestamp", nullable = false)
    private Instant creationTimestamp;

    @UpdateTimestamp
    @Column(name = "update_timestamp")
    private Instant updateTimestamp;

    @ToString.Exclude
    @OneToMany(mappedBy = "documentTemplate")
    private Set<Document> documents;

    @ToString.Exclude
    @ManyToMany
    @JoinTable(
            name = "onboarding_procedure_template_document_template",
            joinColumns = @JoinColumn(name = "document_template_id"),
            inverseJoinColumns = @JoinColumn(name = "onboarding_procedure_template_id"))
    private Set<OnboardingProcedureTemplate> onboardingProcedureTemplates;

    public Boolean isMandatory() {
        return mandatory;
    }
}
