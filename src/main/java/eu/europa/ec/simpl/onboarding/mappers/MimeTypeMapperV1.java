package eu.europa.ec.simpl.onboarding.mappers;

import eu.europa.ec.simpl.api.onboarding.v1.model.PageMetadataDTO;
import eu.europa.ec.simpl.api.onboarding.v1.model.PagedModelMimeTypeDTO;
import eu.europa.ec.simpl.common.model.dto.onboarding.MimeTypeDTO;
import lombok.Generated;
import org.mapstruct.AnnotateWith;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import org.springframework.data.domain.Page;

@Mapper(unmappedSourcePolicy = ReportingPolicy.ERROR, unmappedTargetPolicy = ReportingPolicy.ERROR)
@AnnotateWith(Generated.class)
public interface MimeTypeMapperV1 {

    MimeTypeDTO toV0(eu.europa.ec.simpl.api.onboarding.v1.model.MimeTypeDTO mimeTypeDTO);

    @Mapping(target = "page", source = ".")
    @Mapping(target = "content", source = "content")
    PagedModelMimeTypeDTO toV1(Page<MimeTypeDTO> page);

    @BeanMapping(
            ignoreUnmappedSourceProperties = {
                "empty",
                "numberOfElements",
                "content",
                "sort",
                "first",
                "last",
                "pageable"
            })
    PageMetadataDTO toPageMetadataDTO(Page<MimeTypeDTO> page);

    eu.europa.ec.simpl.api.onboarding.v1.model.MimeTypeDTO toV1(MimeTypeDTO mimeType);
}
