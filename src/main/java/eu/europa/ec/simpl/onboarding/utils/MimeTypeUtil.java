package eu.europa.ec.simpl.onboarding.utils;

import eu.europa.ec.simpl.common.exceptions.RuntimeWrapperException;
import eu.europa.ec.simpl.onboarding.exceptions.UnsupportedMimeTypeException;
import java.util.Base64;
import java.util.List;
import java.util.Locale;
import lombok.experimental.UtilityClass;
import lombok.extern.log4j.Log4j2;
import org.apache.tika.mime.MediaType;
import org.apache.tika.mime.MimeTypeException;
import org.apache.tika.mime.MimeTypes;

@Log4j2
@UtilityClass
public class MimeTypeUtil {

    public static List<String> getAllAvailableMimeTypes(String search) {
        return MimeTypes.getDefaultMimeTypes().getMediaTypeRegistry().getTypes().stream()
                .map(MediaType::toString)
                .filter(mimeType -> mimeType.contains(search.toLowerCase(Locale.getDefault())))
                .toList();
    }

    public static boolean isKnown(String mimeType) {
        try {
            return MimeTypes.getDefaultMimeTypes().getRegisteredMimeType(mimeType) != null;
        } catch (MimeTypeException e) {
            log.error("Error getting mime type {}", mimeType, e);
            throw new UnsupportedMimeTypeException(mimeType);
        }
    }

    public static boolean hasMatchingMimeType(String contentType, String base64) {
        return hasMatchingMimeType(contentType, Base64.getDecoder().decode(base64));
    }

    public static boolean hasMatchingMimeType(String contentType, byte[] bytes) {
        try {
            return MimeTypes.getDefaultMimeTypes()
                    .getRegisteredMimeType(contentType)
                    .matches(bytes);
        } catch (MimeTypeException e) {
            throw new RuntimeWrapperException(e);
        }
    }
}
