package eu.europa.ec.simpl.onboarding.statemachine;

import eu.europa.ec.simpl.onboarding.statemachine.common.BasicEvent;

public interface TimeoutEvent extends BasicEvent<OnboardingState, OnboardingPayload<Void>> {

    @Override
    default OnboardingState getSource() {
        return OnboardingState.IN_PROGRESS;
    }

    @Override
    default OnboardingState getTarget() {
        return OnboardingState.REJECTED;
    }
}
