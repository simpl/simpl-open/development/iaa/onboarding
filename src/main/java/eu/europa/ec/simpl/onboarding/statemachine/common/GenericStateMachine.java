package eu.europa.ec.simpl.onboarding.statemachine.common;

import eu.europa.ec.simpl.common.exceptions.RuntimeWrapperException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import lombok.Data;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.StateMachineContext;
import org.springframework.statemachine.StateMachineEventResult;
import org.springframework.statemachine.StateMachinePersist;
import org.springframework.statemachine.access.StateMachineAccess;
import org.springframework.statemachine.action.Action;
import org.springframework.statemachine.config.StateMachineBuilder;
import org.springframework.statemachine.config.configurers.ExternalTransitionConfigurer;
import org.springframework.statemachine.config.configurers.StateConfigurer;
import org.springframework.statemachine.guard.Guard;
import org.springframework.statemachine.persist.DefaultStateMachinePersister;
import org.springframework.statemachine.persist.StateMachinePersister;
import org.springframework.statemachine.support.DefaultStateMachineContext;
import org.springframework.statemachine.support.StateMachineInterceptorAdapter;
import org.springframework.util.CollectionUtils;
import reactor.core.publisher.Mono;

public class GenericStateMachine<S extends State> {

    private static final String EXCEPTION_HOLDER_KEY = "EXCEPTION_HOLDER";
    private static final String PAYLOAD_HEADER = "PAYLOAD";

    private final StateMachineBuilder.Builder<S, EventType> builder;
    private final Interceptor interceptor;

    private S initialState;
    private StateMachine<S, EventType> sm;

    public GenericStateMachine() {
        this.builder = StateMachineBuilder.builder();
        this.interceptor = new Interceptor();
    }

    public <P> void addTransition(Event<S, P> event) {
        if (this.sm != null) {
            throw new IllegalStateException("State machine already initialized");
        }
        configureStates(event);
        configureTransitions(event);
    }

    public void init() {
        if (this.sm != null) {
            throw new IllegalStateException("State machine already initialized");
        }
        this.sm = builder.build();
        this.sm.getStateMachineAccessor().doWithAllRegions(addInterceptor());
    }

    private Consumer<StateMachineAccess<S, EventType>> addInterceptor() {
        return access -> access.addStateMachineInterceptor(interceptor);
    }

    public Listeners<S> listeners() {
        return interceptor;
    }

    public void start() {
        if (this.sm == null) {
            throw new IllegalStateException("State machine not initialized");
        }
        this.sm.startReactively().block();
    }

    public void reset(S state) {
        if (this.sm == null) {
            throw new IllegalStateException("State machine not initialized");
        }
        StateMachinePersister<S, EventType, Void> persister =
                new DefaultStateMachinePersister<>(new ResetPersist(state));
        try {
            persister.restore(this.sm, null);
        } catch (Exception e) {
            throw new RuntimeWrapperException(e);
        }
    }

    public <P, C extends Event<S, P>> void sendEvent(Class<C> event, P payload) {

        if (this.sm == null) {
            throw new IllegalStateException("State machine not initialized");
        }

        var message = MessageBuilder.withPayload(EventType.fromInterface(event))
                .setHeader(PAYLOAD_HEADER, payload)
                .setHeader(EXCEPTION_HOLDER_KEY, new ExceptionHolder())
                .build();

        var result = sm.sendEvent(Mono.just(message)).collectList().block();

        var exception = Optional.ofNullable(message.getHeaders().get(EXCEPTION_HOLDER_KEY, ExceptionHolder.class))
                .map(ExceptionHolder::getException)
                .orElse(null);

        if (exception != null) {
            throw runtimeExceptionOf(exception);
        }

        if (CollectionUtils.isEmpty(result)) {
            throw new IllegalStateException("StateMachine.sendEvent returned null or empty collection");
        }
        if (result.size() > 1) {
            throw new IllegalStateException("StateMachine.sendEvent returned collection with more than one entry");
        }
        if (result.getFirst().getResultType() == StateMachineEventResult.ResultType.DENIED) {
            throw new IllegalTransitionException(sm.getState().getId().toString(), event.toString());
        }
    }

    public void stop() {
        if (this.sm == null) {
            throw new IllegalStateException("State machine not initialized");
        }
        this.sm.stopReactively().block();
    }

    public S getState() {
        return sm.getState().getId();
    }

    public boolean isComplete() {
        return sm.isComplete();
    }

    private <P> void configureStates(Event<S, P> action) {
        action.getEdges().forEach(edge -> {
            configureState(edge.getSource());
            configureState(edge.getTarget());
        });
    }

    private void configureState(S state) {
        var stateConfigurer = getStateConfigurer();
        switch (state.getType()) {
            case INITIAL -> setInitialState(state);
            case FINAL -> stateConfigurer.end(state);
            case STATE -> stateConfigurer.state(state);
        }
    }

    private void setInitialState(S state) {
        if (this.initialState == null) {
            this.initialState = state;
            getStateConfigurer().initial(this.initialState);
        } else {
            if (!this.initialState.equals(state)) {
                throw new IllegalStateException(
                        "Multiple states marked as initial: %s and %s".formatted(this.initialState, state));
            }
        }
    }

    private StateConfigurer<S, EventType> getStateConfigurer() {
        try {
            return builder.configureStates().withStates();
        } catch (Exception e) {
            throw new RuntimeWrapperException(e);
        }
    }

    private <P> void configureTransitions(Event<S, P> event) {
        event.getEdges().forEach(edge -> {
            getTransitionConfigurer()
                    .source(edge.getSource())
                    .target(edge.getTarget())
                    .event(EventType.fromClass(event.getClass()))
                    .guard(invokeGuard(event))
                    .action(invokeHandler(event));
        });
    }

    private <P> Guard<S, EventType> invokeGuard(Event<S, P> event) {
        return context -> {
            var source = context.getSource().getId();
            var target = context.getTarget().getId();
            var eventType = context.getEvent().getType();

            Exception exception = tryInvokeGuard(event, context, source, target, eventType);

            if (exception != null) {
                setMessageHeader(context, exception);
            }

            return exception == null;
        };
    }

    private <P> Exception tryInvokeGuard(
            Event<S, P> event, StateContext<S, EventType> context, S source, S target, Class<?> eventType) {
        Exception exception = null;
        try {
            if (!event.guard(source, target, getPayload(context))) {
                exception = new GuardFailedException(source, target, eventType);
            }
        } catch (Exception e) {
            exception = e;
        }
        return exception;
    }

    private <P> Action<S, EventType> invokeHandler(Event<S, P> event) {
        return context -> {
            try {
                event.handle(context.getSource().getId(), context.getTarget().getId(), getPayload(context));
            } catch (Exception exception) {
                setMessageHeader(context, exception);
                throw runtimeExceptionOf(exception);
            }
        };
    }

    private static void setMessageHeader(StateContext<?, ?> context, Exception exception) {
        ((ExceptionHolder) context.getMessageHeader(EXCEPTION_HOLDER_KEY)).setException(exception);
    }

    @SuppressWarnings("unchecked")
    private <P> P getPayload(StateContext<S, EventType> context) {
        return (P) context.getMessage().getHeaders().get(PAYLOAD_HEADER);
    }

    private ExternalTransitionConfigurer<S, EventType> getTransitionConfigurer() {
        try {
            return builder.configureTransitions().withExternal();
        } catch (Exception e) {
            throw new RuntimeWrapperException(e);
        }
    }

    private RuntimeException runtimeExceptionOf(Exception exception) {
        if (exception instanceof RuntimeException runtimeException) {
            return runtimeException;
        } else {
            return new RuntimeWrapperException(exception);
        }
    }

    @Data
    private static class ExceptionHolder {
        Exception exception;
    }

    private class ResetPersist implements StateMachinePersist<S, EventType, Void> {

        private final S state;

        public ResetPersist(S state) {
            this.state = state;
        }

        @Override
        public void write(StateMachineContext<S, EventType> context, Void contextObj) throws Exception {
            throw new UnsupportedOperationException(
                    "This class should only be used to reset the state machine to a particular state");
        }

        @Override
        public StateMachineContext<S, EventType> read(Void contextObj) throws Exception {
            return new DefaultStateMachineContext<>(state, null, null, null, null);
        }
    }

    private class Interceptor extends StateMachineInterceptorAdapter<S, EventType> implements Listeners<S> {
        private final List<Listener<S>> listeners = new ArrayList<>();

        @Override
        public StateContext<S, EventType> postTransition(StateContext<S, EventType> context) {
            var oldState = context.getSource().getId();
            var newState = context.getTarget().getId();
            var event = context.getMessage().getPayload().getType();
            var payload = getPayload(context);
            listeners.forEach(listener -> listener.afterHandle(oldState, newState, event, payload));
            return context;
        }

        @Override
        public void add(Listener<S> listener) {
            listeners.add(listener);
        }

        @Override
        public void remove(Listener<S> listener) {
            listeners.remove(listener);
        }

        @Override
        public void clear(Listener<S> listener) {
            listeners.clear();
        }
    }

    public interface Listeners<S extends State> {
        void add(Listener<S> listener);

        void remove(Listener<S> listener);

        void clear(Listener<S> listener);
    }

    public interface Listener<S extends State> {

        void afterHandle(S oldState, S newState, Class<?> event, Object payload);
    }
}
