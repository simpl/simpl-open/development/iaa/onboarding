package eu.europa.ec.simpl.onboarding.exceptions;

import eu.europa.ec.simpl.common.exceptions.StatusException;
import org.springframework.http.HttpStatus;

public class UnsupportedMimeTypeException extends StatusException {
    public UnsupportedMimeTypeException(String mimeType) {
        super(HttpStatus.UNPROCESSABLE_ENTITY, "Mime type %s not supported".formatted(mimeType));
    }
}
