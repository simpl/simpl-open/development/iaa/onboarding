package eu.europa.ec.simpl.onboarding.controllers;

import eu.europa.ec.simpl.common.exchanges.onboarding.OnboardingRequestExchange;
import eu.europa.ec.simpl.common.filters.OnboardingRequestFilter;
import eu.europa.ec.simpl.common.model.dto.onboarding.ApproveDTO;
import eu.europa.ec.simpl.common.model.dto.onboarding.CommentDTO;
import eu.europa.ec.simpl.common.model.dto.onboarding.DocumentDTO;
import eu.europa.ec.simpl.common.model.dto.onboarding.OnboardingRequestDTO;
import eu.europa.ec.simpl.common.model.dto.onboarding.RejectDTO;
import eu.europa.ec.simpl.common.responses.PageResponse;
import eu.europa.ec.simpl.onboarding.services.OnboardingRequestService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import jakarta.validation.Valid;
import java.util.UUID;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("onboarding-request")
public class OnboardingRequestController implements OnboardingRequestExchange {

    private final OnboardingRequestService onboardingRequestService;

    public OnboardingRequestController(OnboardingRequestService onboardingRequestService) {
        this.onboardingRequestService = onboardingRequestService;
    }

    @Operation(
            summary = "Get Onboarding Request",
            description = "Retrieves an onboarding request by its ID",
            parameters = {
                @Parameter(
                        name = "onboardingRequestId",
                        description = "The ID of the onboarding request",
                        required = true,
                        schema = @Schema(type = "string", format = "uuid"))
            },
            responses = {
                @ApiResponse(
                        responseCode = "200",
                        description = "Onboarding request retrieved successfully",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = OnboardingRequestDTO.class))),
                @ApiResponse(responseCode = "400", description = "Invalid request data provided"),
                @ApiResponse(responseCode = "401", description = "Access denied"),
                @ApiResponse(responseCode = "403", description = "Forbidden: User does not have the required role"),
                @ApiResponse(responseCode = "404", description = "Onboarding request not found")
            })
    @Override
    public OnboardingRequestDTO getOnboardingRequest(@PathVariable("onboardingRequestId") UUID id) {
        return onboardingRequestService.getOnboardingRequest(id);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @Operation(
            summary = "Create Onboarding Request",
            description = "Creates a new onboarding request",
            requestBody =
                    @io.swagger.v3.oas.annotations.parameters.RequestBody(
                            description = "The onboarding request to create",
                            required = true,
                            content =
                                    @Content(
                                            mediaType = "application/json",
                                            schema = @Schema(implementation = OnboardingRequestDTO.class))),
            responses = {
                @ApiResponse(
                        responseCode = "201",
                        description = "Onboarding request created successfully",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = OnboardingRequestDTO.class))),
                @ApiResponse(responseCode = "400", description = "Invalid request data provided"),
                @ApiResponse(responseCode = "401", description = "Access denied"),
                @ApiResponse(responseCode = "403", description = "Forbidden: User does not have the required role")
            })
    @PostMapping
    public OnboardingRequestDTO create(@RequestBody @Valid OnboardingRequestDTO onboardingRequestDTO) {
        return onboardingRequestService.create(onboardingRequestDTO);
    }

    @PostMapping("{id}/submit")
    public OnboardingRequestDTO submit(@PathVariable UUID id) {
        return onboardingRequestService.submit(id, getInitiator());
    }

    @PostMapping("{id}/request-revision")
    public OnboardingRequestDTO requestRevision(@PathVariable UUID id) {
        return onboardingRequestService.requestRevision(id, getInitiator());
    }

    @PostMapping("{id}/approve")
    public OnboardingRequestDTO approve(@PathVariable UUID id, @RequestBody ApproveDTO approveDTO) {
        return onboardingRequestService.approve(id, approveDTO, getInitiator());
    }

    @PostMapping("{id}/reject")
    public OnboardingRequestDTO reject(@PathVariable UUID id, @RequestBody RejectDTO rejectDTO) {
        return onboardingRequestService.reject(id, rejectDTO, getInitiator());
    }

    @Operation(
            summary = "Add Comment to Onboarding Request",
            description = "Adds a comment to an existing onboarding request",
            parameters = {
                @Parameter(
                        name = "id",
                        description = "The ID of the onboarding request",
                        required = true,
                        schema = @Schema(type = "string", format = "uuid"))
            },
            requestBody =
                    @io.swagger.v3.oas.annotations.parameters.RequestBody(
                            description = "The comment to add",
                            required = true,
                            content =
                                    @Content(
                                            mediaType = "application/json",
                                            schema = @Schema(implementation = CommentDTO.class))),
            responses = {
                @ApiResponse(responseCode = "200", description = "Comment added successfully"),
                @ApiResponse(responseCode = "400", description = "Invalid request data provided"),
                @ApiResponse(responseCode = "401", description = "Access denied"),
                @ApiResponse(responseCode = "403", description = "Forbidden: User does not have the required role"),
                @ApiResponse(responseCode = "404", description = "Onboarding request not found")
            })
    @PatchMapping("{id}/comment")
    public OnboardingRequestDTO addComment(@PathVariable UUID id, @RequestBody CommentDTO comment) {
        return onboardingRequestService.addComment(id, comment, getInitiator());
    }

    @Operation(
            summary = "Search Onboarding Requests",
            description = "Searches for onboarding requests based on filters and pagination",
            responses = {
                @ApiResponse(responseCode = "200", description = "Search results"),
                @ApiResponse(responseCode = "400", description = "Invalid search criteria provided"),
                @ApiResponse(responseCode = "401", description = "Access denied"),
                @ApiResponse(responseCode = "403", description = "Forbidden: User does not have the required role")
            })
    @Override
    public PageResponse<OnboardingRequestDTO> search(
            @ParameterObject @ModelAttribute OnboardingRequestFilter filter,
            @PageableDefault(sort = "creationTimestamp") @ParameterObject Pageable pageable) {
        return onboardingRequestService.search(filter, pageable);
    }

    @Operation(
            summary = "Add Document to Onboarding Request",
            description = "Adds a document to an existing onboarding request",
            parameters = {
                @Parameter(
                        name = "id",
                        description = "The ID of the onboarding request",
                        required = true,
                        schema = @Schema(type = "string", format = "uuid"))
            },
            requestBody =
                    @io.swagger.v3.oas.annotations.parameters.RequestBody(
                            description = "The document to add",
                            required = true,
                            content =
                                    @Content(
                                            mediaType = "application/json",
                                            schema = @Schema(implementation = DocumentDTO.class))),
            responses = {
                @ApiResponse(
                        responseCode = "200",
                        description = "Document added successfully",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = OnboardingRequestDTO.class))),
                @ApiResponse(responseCode = "400", description = "Invalid request data provided"),
                @ApiResponse(responseCode = "401", description = "Access denied"),
                @ApiResponse(responseCode = "403", description = "Forbidden: User does not have the required role"),
                @ApiResponse(responseCode = "404", description = "Onboarding request not found")
            })
    @PostMapping("{id}/document")
    public OnboardingRequestDTO requestAdditionalDocument(@PathVariable UUID id, @RequestBody DocumentDTO document) {
        return onboardingRequestService.requestAdditionalDocument(id, document, getInitiator());
    }

    @Operation(
            summary = "Set Document for Onboarding Request",
            description = "Updates a document for an existing onboarding request",
            parameters = {
                @Parameter(
                        name = "id",
                        description = "The ID of the onboarding request",
                        required = true,
                        schema = @Schema(type = "string", format = "uuid"))
            },
            requestBody =
                    @io.swagger.v3.oas.annotations.parameters.RequestBody(
                            description = "The document to upload or update",
                            required = true,
                            content =
                                    @Content(
                                            mediaType = "application/json",
                                            schema = @Schema(implementation = DocumentDTO.class))),
            responses = {
                @ApiResponse(responseCode = "200", description = "Document updated successfully"),
                @ApiResponse(responseCode = "400", description = "Invalid request data provided"),
                @ApiResponse(responseCode = "401", description = "Access denied"),
                @ApiResponse(responseCode = "403", description = "Forbidden: User does not have the required role"),
                @ApiResponse(responseCode = "404", description = "Onboarding request not found")
            })
    @PatchMapping("{id}/document")
    public OnboardingRequestDTO uploadDocument(@PathVariable UUID id, @RequestBody DocumentDTO document) {
        return onboardingRequestService.uploadDocument(id, document, getInitiator());
    }

    @Operation(
            summary = "Set Expiration Timeframe for Onboarding Request",
            description = "Sets the expiration timeframe for an existing onboarding request",
            parameters = {
                @Parameter(
                        name = "id",
                        description = "The ID of the onboarding request",
                        required = true,
                        schema = @Schema(type = "string", format = "uuid")),
                @Parameter(
                        name = "expirationTimeframe",
                        description = "The expiration timeframe in milliseconds",
                        required = true,
                        schema = @Schema(type = "integer", format = "int64"))
            },
            responses = {
                @ApiResponse(responseCode = "200", description = "Expiration timeframe set successfully"),
                @ApiResponse(responseCode = "400", description = "Invalid request data provided"),
                @ApiResponse(responseCode = "401", description = "Access denied"),
                @ApiResponse(responseCode = "403", description = "Forbidden: User does not have the required role"),
                @ApiResponse(responseCode = "404", description = "Onboarding request not found")
            })
    @PatchMapping("{id}/expiration-timeframe")
    public void setExpirationTimeframe(@PathVariable UUID id, @RequestParam Long expirationTimeframe) {
        // TODO Return onboarding request
        onboardingRequestService.setExpirationTimeframe(id, expirationTimeframe);
    }

    @Operation(
            summary = "Get Document from Onboarding Request",
            description = "Retrieves a specific document from an existing onboarding request",
            parameters = {
                @Parameter(
                        name = "onboardingRequestId",
                        description = "The ID of the onboarding request",
                        required = true,
                        schema = @Schema(type = "string", format = "uuid")),
                @Parameter(
                        name = "documentId",
                        description = "The ID of the document to retrieve",
                        required = true,
                        schema = @Schema(type = "string", format = "uuid"))
            },
            responses = {
                @ApiResponse(
                        responseCode = "200",
                        description = "Document retrieved successfully",
                        content =
                                @Content(
                                        mediaType = "application/json",
                                        schema = @Schema(implementation = DocumentDTO.class))),
                @ApiResponse(responseCode = "400", description = "Invalid request data provided"),
                @ApiResponse(responseCode = "401", description = "Access denied"),
                @ApiResponse(responseCode = "403", description = "Forbidden: User does not have the required role"),
                @ApiResponse(responseCode = "404", description = "Onboarding request or document not found")
            })
    @GetMapping("{onboardingRequestId}/document/{documentId}")
    public DocumentDTO getDocument(@PathVariable UUID onboardingRequestId, @PathVariable UUID documentId) {
        return onboardingRequestService.getDocument(onboardingRequestId, documentId);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("{onboardingRequestId}/document/{documentId}")
    public void deleteDocument(@PathVariable UUID onboardingRequestId, @PathVariable UUID documentId) {
        onboardingRequestService.deleteDocument(onboardingRequestId, documentId);
    }

    private String getInitiator() {
        return getClass().getSimpleName(); // TODO MAX 50 CHARS
    }
}
